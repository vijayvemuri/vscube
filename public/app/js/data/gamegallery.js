var alphaordergallery = [agame1, agame2, agame3, agame4, agame5, agame6,
    agame7, agame8, agame9, agame10, agame11, agame12];

var chooseallgallery = [cagame1, cagame2 ];

var imgmatchinggamesgallery = [imgame1, imgame2, imgame3, imgame4, imgame5, imgame6,
    imgame7,imgame8];

var jumblewordgallery = [jwgame1, jwgame2, jwgame3, jwgame4, jwgame5,
    jwgame6, jwgame7, jwgame8, jwgame9, jwgame10,
    jwgame11, jwgame12, jwgame13, jwgame14, jwgame15,
    jwgame16, jwgame17, jwgame18, jwgame19, jwgame20,
    jwgame21, jwgame22, jwgame23, jwgame24, jwgame25,
    jwgame26, jwgame27, jwgame28, jwgame29, jwgame30 ];  

var matchinggamesgallery = [game1, game2, game3, game4, game5, game6,
    game7, game8, game9, game10, game11, game12,game13, game14, game15, game16, game17, game18,
    game19, game20, game21, game22, game23, game24, game25, game26, game27];

var missinglettergallery = [mlgame1, mlgame2, mlgame3, mlgame4, mlgame5,
    mlgame6, mlgame7, mlgame8, mlgame9, mlgame10,
    mlgame11, mlgame12, mlgame13, mlgame14, mlgame15,
    mlgame16, mlgame17, mlgame18, mlgame19, mlgame20,
    mlgame21, mlgame22, mlgame23, mlgame24, mlgame25,
    mlgame26, mlgame27, mlgame28, mlgame29, mlgame30 ];

var oddoneoutgallery = [ogame1, ogame2, ogame3, ogame4, ogame5];

var mcqquizgallery = [gkqgame1, gkqgame2, gkqgame3, gkqgame4, gkqgame5, gkqgame6,
    gkqgame7, gkqgame8, gkqgame9, gkqgame10];

var mbquizgallery = [mqgame1, mqgame2, mqgame3, mqgame4, mqgame5, mqgame6,
mqgame7, mqgame8, mqgame9, mqgame10];

var ramaquizgallery = [rqgame1, rqgame2,rqgame3,rqgame4, rqgame5,rqgame6 , rqgame7, rqgame8,rqgame9,rqgame10];

var allquizesgallery = [].concat(mcqquizgallery, mbquizgallery, ramaquizgallery);

var spellinggallery = [sgame1, sgame2, sgame3, sgame4, sgame5,
    sgame6, sgame7, sgame8, sgame9, sgame10,
    sgame11, sgame12, sgame13, sgame14, sgame15,
    sgame16, sgame17, sgame18,sgamenew1,sgamenew2,
    sgamenew3,sgamenew4,sgamenew5,sgamenew6,sgamenew7,
    sgamenew8,sgamenew9,sgamenew10,sgamenew11,sgamenew12,
    sgamenew13,sgamenew14,sgamenew15,sgamenew16,sgamenew17,
    sgamenew18,sgamenew19,sgamenew20,sgamenew21,sgamenew22,
    sgamenew23,sgamenew24,sgamenew25,sgamenew26,sgamenew27,
    sgamenew28,sgamenew29,sgamenew30,sgamenew31,sgamenew32,
    sgamenew33,sgamenew34,sgamenew35,sgamenew36,sgamenew37,
    sgamenew38,sgamenew39,sgamenew40,sgamenew41,sgamenew42,
    sgamenew43,sgamenew44,sgamenew45,sgamenew46,sgamenew47,
    sgamenew48,sgamenew49,sgamenew50,sgamenew51,sgamenew52,
    sgamenew53,sgamenew54,sgamenew55,sgamenew56,sgamenew57,
    sgamenew58,sgamenew59
 ];

 var wordfindergallery = [wfgame1, wfgame2, wfgame3, wfgame4, wfgame5,
    wfgame6, wfgame7, wfgame8, wfgame9, wfgame10,
    wfgame11, wfgame12, wfgame13, wfgame14, wfgame15,
    wfgame16, wfgame17, wfgame18,wfgame19,wfgame20
 ];

 var jiggamesgallery = [jiggame1,jiggame2];
