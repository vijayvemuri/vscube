var jiggame1 = { id : "jiggame1",gametype : 'VSCUBE.JIG-GAME', galleryGameLabel : 'Owl', galleryGroup : 'PICTURE MATCHING GAMES',
			  headerMsg : "Drag and drop the images from bottom tray to the yellow boxes to match the image on the left hand side.",
			  topTrayLabel : "Owl",
              topTrayOptionsArray : ["","","","","","","","",""],
              originalImageName:"Owl.jpg",
			  bottomTrayLabel : "Jumbled Images",
              bottomTrayOptionsArray : ["image_part_007.jpg","image_part_005.jpg","image_part_003.jpg","image_part_001.jpg",
              "image_part_009.jpg","image_part_002.jpg","image_part_004.jpg","image_part_006.jpg","image_part_008.jpg"],
              answerMappingArray : [3,5,2,6,1,7,0,8,4]
			}
var jiggame2 = { id : "jiggame2",gametype : 'VSCUBE.JIG-GAME', galleryGameLabel : 'Tiger', galleryGroup : 'PICTURE MATCHING GAMES',
			  headerMsg : "Drag and drop the images from bottom tray to the yellow boxes to match the image on the left hand side.",
			  topTrayLabel : "Tiger",
              topTrayOptionsArray : ["","","","","","","","",""],
              originalImageName:"Tiger.jpg",
			  bottomTrayLabel : "Jumbled Images",
              bottomTrayOptionsArray : ["image_part_007.jpg","image_part_005.jpg","image_part_003.jpg","image_part_001.jpg",
              "image_part_009.jpg","image_part_002.jpg","image_part_004.jpg","image_part_006.jpg","image_part_008.jpg"],
              answerMappingArray : [3,5,2,6,1,7,0,8,4]
			}			
	