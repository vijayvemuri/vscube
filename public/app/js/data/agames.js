var agame1 = { id : "agame1", gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Animals - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Animal Names",
              bottomTrayOptionsArray : ["Tiger","Lion","Elephant","Horse","Cow","Sheep","Monkey","Dog","Cat","Duck"],
              answerMappingArray : [8,4,7,9,2,3,1,6,5,0]
			}
var agame2 = { id : "agame2",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Flowers - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Country Names",
              bottomTrayOptionsArray : ["India","United States","China","Japan","France","Singapore","Philippines","Turkey","Netherlands","Mexico"],
              answerMappingArray : [2,4,0,3,9,8,6,5,7,1]
			}
var agame3 = { id : "agame3",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Water Animals - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Flower Names",
              bottomTrayOptionsArray : ["SunFlower","Rose","Lily","Rosemary","Daisy","Crocus","Tulip","BlueBell","Amaryllis","Orchid"],
              answerMappingArray : [8,7,5,4,2,9,1,3,0,6]
			}
var agame4 = { id : "agame4",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Vegetables - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Water Animals",
              bottomTrayOptionsArray : ["Whale","Dolphin","Fish","Shark","Crocodile","StarFish","Octopus","Nautilus","ElephantSeal","AngelFish"],
              answerMappingArray : [9,4,1,8,2,7,6,3,5,0]
			}
var agame5 = { id : "agame5",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Fruits - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Vegetable Names",
              bottomTrayOptionsArray : ["SweetCorn","Cabbages","Spinach","Onions","Lettuce","Mushrooms","Potatoes","Carrots","Tomotoes","Beans"],
              answerMappingArray : [9,1,7,4,5,3,6,2,0,8]
			}
var agame6 = { id : "agame6",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Food Items - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Fruits",
              bottomTrayOptionsArray : ["Figs","Blueberries","JackFruit","Grapes","Banana","Mango","Cherries","Apple","Watermelon","Kiwi"],
              answerMappingArray : [7,4,1,6,0,3,2,9,5,8]
			}
var agame7 = { id : "agame7",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Birds - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Food Item Names",
              bottomTrayOptionsArray : ["IceCream","Pizza","Burger","FriedRice","Cake","SpringRoll","FrenchFries","Pasta","Tacos","Quesadillas"],
              answerMappingArray : [2,4,6,3,0,7,1,9,5,8]
			}
var agame8 = { id : "agame8",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Games - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Games/Sports",
              bottomTrayOptionsArray : ["Swimming","Hockey","Soccer","Cricket","BasketBall","Archery","VollyBall","Polo","Karate","Surfing"],
              answerMappingArray : [5,4,3,1,8,7,2,9,0,6]
			}
var agame9 = { id : "agame9",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Home Appliances - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Home Appliances",
              bottomTrayOptionsArray : ["Stove","WashingMachine","Dryer","Mixer","Blender","DishWasher","Toaster","Oven","Grinder","Fridge"],
              answerMappingArray : [4,5,2,9,8,3,7,0,6,1]
			}
var agame10 = { id : "agame10",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Countries - 1', galleryGroup : 'ALPHABATICAL ORDER',
			  headerMsg : "Try to arrange the words from bottm tray in alphatical order",
			  topTrayLabel : "Alphabatical order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Birds",
              bottomTrayOptionsArray : ["Parrot","Owl","Crane","Gulls","KingFisher","BulBul","Sparrow","WoodPecker","Crow","Squirril"],
              answerMappingArray : [5,2,8,3,4,1,0,6,9,7]
			}

var agame11 = { id : "agame11",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Unit Of Time', galleryGroup : 'GENERAL KNOWLEDGE',
			  headerMsg : "Try to arrange the planet from bottm tray in Planet Postion from Sun order",
			  topTrayLabel : "Planet Postion order",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9"],
			  bottomTrayLabel : "Planet",
              bottomTrayOptionsArray : ["Venus","Mercury","Pluto","Neptune","Earth","Uranus","Jupiter","Mars","Saturn"],
              answerMappingArray : [1,0,4,7,6,8,5,3,2]
			}																									

var agame12 = { id : "agame11",gametype : 'VSCUBE.ALPHA-ORDER-GAME', galleryGameLabel : 'Planet Positions', galleryGroup : 'GENERAL KNOWLEDGE',
			  headerMsg : "Try to arrange the Unit of time from bottm tray starting from lowest.",
			  topTrayLabel : "Lowest to Highest",
              topTrayOptionsArray : ["1","2","3","4","5","6","7","8","9","10"],
			  bottomTrayLabel : "Unit of Time",
              bottomTrayOptionsArray : ["Decade","Hour","Century","Millennium","Minute","Week","Month","Day","Year","Second"],
              answerMappingArray : [9,4,1,7,5,6,8,0,2,3]
			}