var mlgame1 = { id : "mlgame1",level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Animals-1', galleryGroup : 'LEVEL - 1',
			  headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Animals",
              wordsArray : [
              { position : "1", correctWord :"TIGER", question : "T G R", missinglettersCount :"2",missingletters :"AIETV"},
              { position : "2", correctWord :"DONKEY",question : " O K Y",missinglettersCount :"3",missingletters :"ANDEP"},
              { position : "3", correctWord :"LION",question : " I N",missinglettersCount :"2",missingletters :"NLATO"},
              { position : "4", correctWord :"COW",question : " O ",missinglettersCount :"2",missingletters :"PCJWT"},
              { position : "5", correctWord :"CAMEL",question : " A  L",missinglettersCount :"3",missingletters :"AMCEO"}]
			}
var mlgame2 = { id : "mlgame2",level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Animals-2', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Animals",
              wordsArray : [
              { position : "1", correctWord :"ELEPHANT", question : "E E H NT", missinglettersCount :"3",missingletters :"AQLNP"},
              { position : "2", correctWord :"GIRAFFE",question : " IR F E",missinglettersCount :"3",missingletters :"BGKAF"},
              { position : "3", correctWord :"HORSE",question : "H R E",missinglettersCount :"2",missingletters :"TOKSZ"},
              { position : "4", correctWord :"MONKEY",question : "M  K Y",missinglettersCount :"3",missingletters :"SONET"},
              { position : "5", correctWord :"GORILLA",question : " O I LA",missinglettersCount :"3",missingletters :"AGSRL"}]
			}						
var mlgame3 = { id : "mlgame3",level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Animals-3', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Animals",
              wordsArray : [
              { position : "1", correctWord :"OSTRICH", question : "O T I H", missinglettersCount :"3",missingletters :"KSTRC"},
              { position : "2", correctWord :"TURKEY",question : " URK Y",missinglettersCount :"2",missingletters :"ATESL"},
              { position : "3", correctWord :"RABBIT",question : "R   IT",missinglettersCount :"3",missingletters :"ABCCB"},
              { position : "4", correctWord :"WOLF",question : "W  F",missinglettersCount :"2",missingletters :"ONLFG"},
              { position : "5", correctWord :"ZEBRA",question : "Z BR ",missinglettersCount :"2",missingletters :"AEIOU"}]
			}
var mlgame4 = { id : "mlgame4",level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Water Animals-1', galleryGroup : 'LEVEL - 1',
        headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Water Animals",
              wordsArray : [
              { position : "1", correctWord :"WHALE", question : "W   E", missinglettersCount :"3",missingletters :"AHLPQ"},
              { position : "2", correctWord :"DOLPHIN",question : "D L HI ",missinglettersCount :"3",missingletters :"ONEPS"},
              { position : "3", correctWord :"SHARK",question : " HA K",missinglettersCount :"2",missingletters :"SRKTU"},
              { position : "4", correctWord :"FISH",question : " IS ",missinglettersCount :"2",missingletters :"HORFT"},
              { position : "5", correctWord :"CROCODILE",question : "C  C DI E",missinglettersCount :"4",missingletters :"OORLS"}]
			}
var mlgame5 = { id : "mlgame5",level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Birds-1', galleryGroup : 'LEVEL - 1',
        headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Birds",
              wordsArray : [
              { position : "1", correctWord :"PARROT", question : "P R O ", missinglettersCount :"3",missingletters :"DARTO"},
              { position : "2", correctWord :"OWL",question : " W ",missinglettersCount :"2",missingletters :"LOVER"},
              { position : "3", correctWord :"PENGUIN",question : " E GU N",missinglettersCount :"3",missingletters :"PINTO"},
              { position : "4", correctWord :"CRANE",question : "  ANE",missinglettersCount :"2",missingletters :"RCDRK"},
              { position : "5", correctWord :"SPARROW",question : "S AR O ",missinglettersCount :"3",missingletters :"WPRKZ"}]
			}
var mlgame6 = { id : "mlgame6", level :"3",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Birds-2', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Birds",
              wordsArray : [
              { position : "1", correctWord :"BULBUL", question : "B LB L", missinglettersCount :"2",missingletters :"AEIUU"},
              { position : "2", correctWord :"GULLS",question : " ULL ",missinglettersCount :"2",missingletters :"RTGSA"},
              { position : "3", correctWord :"KINGFISHER",question : "K NGF SH R",missinglettersCount :"3",missingletters :"IEEIJ"},
              { position : "4", correctWord :"STORK",question : "ST  K",missinglettersCount :"2",missingletters :"DOORS"},
              { position : "5", correctWord :"WOODPECKER",question : "W  DP CK R",missinglettersCount :"4",missingletters :"OOEEA"}]
			}
var mlgame7 = { id : "mlgame7", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Country Names-1', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Countries",
              wordsArray : [
              { position : "1", correctWord :"CHINA", question : "C   A", missinglettersCount :"3",missingletters :"INDHA"},
              { position : "2", correctWord :"NEPAL",question : "N P L",missinglettersCount :"2",missingletters :"ANDEP"},
              { position : "3", correctWord :"INDIA",question : "I  I ",missinglettersCount :"3",missingletters :"ANDOR"},
              { position : "4", correctWord :"SRILANKA",question : "S IL  KA",missinglettersCount :"3",missingletters :"RANTO"},
              { position : "5", correctWord :"PAKISTAN",question : " AKI T N",missinglettersCount :"3",missingletters :"ASATP"}]
			}
var mlgame8 = { id : "mlgame8", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Country Names-2', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Countries",
              wordsArray : [
              { position : "1", correctWord :"AUSTRALIA", question : " USTR LI ", missinglettersCount :"3",missingletters :"ASASA"},
              { position : "2", correctWord :"BANGLADESH",question : "B NG A  SH",missinglettersCount :"4",missingletters :"BELAD"},
              { position : "3", correctWord :"CANADA",question : " A A A",missinglettersCount :"3",missingletters :"ACDNP"},
              { position : "4", correctWord :"GERMANY",question : "G R A Y",missinglettersCount :"3",missingletters :"NMETS"},
              { position : "5", correctWord :"JAPAN",question : "J   N",missinglettersCount :"3",missingletters :"PASTA"}]
			}
var mlgame9 = { id : "mlgame9", level :"3",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Country Names-3', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Countries",
              wordsArray : [
              { position : "1", correctWord :"MALAYSIA", question : "  LAY IA", missinglettersCount :"3",missingletters :"SAMTO"},
              { position : "2", correctWord :"BANGLADESH",question : "B NG A  SH",missinglettersCount :"4",missingletters :"BELAD"},
              { position : "3", correctWord :"SINGAPORE",question : " INGA OR ",missinglettersCount :"3",missingletters :"SPEAK"},
              { position : "4", correctWord :"SPAIN",question : "S   N",missinglettersCount :"3",missingletters :"PQRIA"},
              { position : "5", correctWord :"KENYA",question : " E Y ",missinglettersCount :"3",missingletters :"PKNAS"}]
			}
var mlgame10 = { id : "mlgame10", level :"3",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Country Names-4', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
			  topTrayLabel : "Countries",
              wordsArray : [
              { position : "1", correctWord :"MEXICO", question : " EXIC ", missinglettersCount :"2",missingletters :"MOODR"},
              { position : "2", correctWord :"BANGLADESH",question : "B NG A  SH",missinglettersCount :"4",missingletters :"BELAD"},
              { position : "3", correctWord :"BRAZIL",question : "B   IL",missinglettersCount :"3",missingletters :"ZARTO"},
              { position : "4", correctWord :"ITALY",question : "ITA  ",missinglettersCount :"2",missingletters :"COOLY"},
              { position : "5", correctWord :"AFGHANISTAN",question : "A G AN S AN",missinglettersCount :"4",missingletters :"FITHS"}]
			}

var mlgame11 = { id : "mlgame11", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Professions-1', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Professions",
              wordsArray : [
              { position : "1", correctWord :"DOCTOR", question : " OC O ", missinglettersCount :"3",missingletters :"DRTZQ"},
              { position : "2", correctWord :"LAWYER",question : "L W E ",missinglettersCount :"3",missingletters :"ZAYDR"},
              { position : "3", correctWord :"ENGINEER",question : " NG N  R",missinglettersCount :"4",missingletters :"EEEIP"},
              { position : "4", correctWord :"ACTOR",question : " CTO ",missinglettersCount :"2",missingletters :"BARPQ"},
              { position : "5", correctWord :"NURSE",question : "N R E",missinglettersCount :"2",missingletters :"USAUK"}]
                     }
var mlgame12 = { id : "mlgame12", level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Transport Vehicles', galleryGroup : 'LEVEL - 1',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Transport Vehicles",
              wordsArray : [
              { position : "1", correctWord :"TRAIN", question : "T   N", missinglettersCount :"3",missingletters :"IARPQ"},
              { position : "2", correctWord :"CAR",question : "C  ",missinglettersCount :"2",missingletters :"RACKO"},
              { position : "3", correctWord :"AEROPLANE",question : "A R PL NE",missinglettersCount :"3",missingletters :"EOAPR"},
              { position : "4", correctWord :"BOAT",question : "B  T",missinglettersCount :"2",missingletters :"AOTSK"},
              { position : "5", correctWord :"SHIP",question : " HI ",missinglettersCount :"2",missingletters :"PSTKZ"}]
                     }   
var mlgame13 = { id : "mlgame13", level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : '>Flowers-1', galleryGroup : 'LEVEL - 1',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Flowers",
              wordsArray : [
              { position : "1", correctWord :"ROSE", question : " OS ", missinglettersCount :"2",missingletters :"REDS"},
              { position : "2", correctWord :"LILY",question : "L  Y",missinglettersCount :"2",missingletters :"LISPA"},
              { position : "3", correctWord :"JASMINE",question : "J  MI E",missinglettersCount :"3",missingletters :"ASNQS"},
              { position : "4", correctWord :"MARIGOLD",question : "M R G LD",missinglettersCount :"3",missingletters :"AEIOU"},
              { position : "5", correctWord :"SUNFLOWER",question : "SU  LOWE ",missinglettersCount :"3",missingletters :"FNRIN"}]
                     } 
var mlgame14 = { id : "mlgame14", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Flowers-2', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Flowers",
              wordsArray : [
              { position : "1", correctWord :"HIBISCUS", question : "H B SC S", missinglettersCount :"2",missingletters :"UIISK"},
              { position : "2", correctWord :"LAVENDER",question : " A EN ER",missinglettersCount :"3",missingletters :"DLAOP"},
              { position : "3", correctWord :"ORCHID",question : "O CH D",missinglettersCount :"2",missingletters :"RIVER"},
              { position : "4", correctWord :"ROSEMARY",question : "R SEM R ",missinglettersCount :"3",missingletters :"OYATZ"},
              { position : "5", correctWord :"TULIP",question : "T LI ",missinglettersCount :"2",missingletters :"PUSTK"}]
                     } 
var mlgame15 = { id : "mlgame15", level :"3",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Flowers-3', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Flowers",
              wordsArray : [
              { position : "1", correctWord :"DAISY", question : " AIS ", missinglettersCount :"2",missingletters :"YDEAR"},
              { position : "2", correctWord :"CROCUS",question : "C OC S",missinglettersCount :"2",missingletters :"REDUI"},
              { position : "3", correctWord :"CAMELLIAS",question : "C ME LI S",missinglettersCount :"3",missingletters :"SALAS"},
              { position : "4", correctWord :"BLUEBELL",question : "B  EB LL",missinglettersCount :"3",missingletters :"LUEAR"},
              { position : "5", correctWord :"AMARYLLIS",question : "A A YL IS",missinglettersCount :"3",missingletters :"IMRLK"}]
                     }   
var mlgame16 = { id : "mlgame16", level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'English Months', galleryGroup : 'LEVEL - 1',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Months",
              wordsArray : [
              { position : "1", correctWord :"AUGUST", question : "A G ST", missinglettersCount :"2",missingletters :"UIUKJ"},
              { position : "2", correctWord :"JANUARY",question : "J  U RY",missinglettersCount :"3",missingletters :"ZANAP"},
              { position : "3", correctWord :"MARCH",question : " AR H",missinglettersCount :"2",missingletters :"MCARD"},
              { position : "4", correctWord :"DECEMBER",question : "  CEMB R",missinglettersCount :"3",missingletters :"DEERA"},
              { position : "5", correctWord :"JUNE",question : " U E",missinglettersCount :"2",missingletters :"NJITZ"}]
                     }
var mlgame17 = { id : "mlgame17", level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Food Items', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Food items",
              wordsArray : [
              { position : "1", correctWord :"PASTA", question : " AST ", missinglettersCount :"2",missingletters :"AIATV"},
              { position : "2", correctWord :"CAKE",question : "C  E",missinglettersCount :"2",missingletters :"KITEA"},
              { position : "3", correctWord :"BREAD",question : "B E D",missinglettersCount :"2",missingletters :"RANTO"},
              { position : "4", correctWord :"PIZZA",question : "PI  A",missinglettersCount :"2",missingletters :"ZAZAA"},
              { position : "5", correctWord :"BURGER",question : "BU GE ",missinglettersCount :"2",missingletters :"ROARS"}]
                     }                                                                                                                                                                                                                                                                                                                          

var mlgame18 = { id : "mlgame18", level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Sports-1', galleryGroup : 'LEVEL - 1',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Sports/Games",
              wordsArray : [
              { position : "1", correctWord :"HOCKEY", question : " OCKE ", missinglettersCount :"2",missingletters :"YHDRA"},
              { position : "2", correctWord :"CRICKET",question : " RI KE ",missinglettersCount :"3",missingletters :"CCTVS"},
              { position : "3", correctWord :"FOOTBALL",question : "  OT ALL",missinglettersCount :"3",missingletters :"FOBER"},
              { position : "4", correctWord :"BADMINTON",question : "BA MI TO ",missinglettersCount :"3",missingletters :"DNNPZ"},
              { position : "5", correctWord :"BASKETBALL",question : " AS ETBA  ",missinglettersCount :"4",missingletters :"LLBKA"}]
                     } 

var mlgame19 = { id : "mlgame19", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Sports-2', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Sports/Games",
              wordsArray : [
              { position : "1", correctWord :"VOLLYBALL", question : " OLL BA  ", missinglettersCount :"4",missingletters :"LLVYX"},
              { position : "2", correctWord :"BOXING",question : "BO IN ",missinglettersCount :"2",missingletters :"GXYUI"},
              { position : "3", correctWord :"ARCHERY",question : " R H RY",missinglettersCount :"3",missingletters :"KACED"},
              { position : "4", correctWord :"RUNNING",question : " U NI G",missinglettersCount :"3",missingletters :"RNNUX"},
              { position : "5", correctWord :"TENNIS",question : "TE  IS",missinglettersCount :"2",missingletters :"NUNTI"}]
                     } 

var mlgame20 = { id : "mlgame20", level :"3",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Sports-3', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Sports/Games",
              wordsArray : [
              { position : "1", correctWord :"SURFING", question : "SU   NG", missinglettersCount :"3",missingletters :"FIRST"},
              { position : "2", correctWord :"GOLF",question : "G L ",missinglettersCount :"2",missingletters :"FONDS"},
              { position : "3", correctWord :"GYMNASTICS",question : " YM AS ICS",missinglettersCount :"3",missingletters :"GTNPO"},
              { position : "4", correctWord :"KARATE",question : " ARAT  ",missinglettersCount :"2",missingletters :"KITES"},
              { position : "5", correctWord :"POLO",question : " O O",missinglettersCount :"2",missingletters :"LOPSE"}]
                     } 
var mlgame21 = { id : "mlgame21", level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Solar System-1', galleryGroup : 'LEVEL - 1',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Solar System-1",
              wordsArray : [
              { position : "1", correctWord :"SUN", question : "S N", missinglettersCount :"1",missingletters :"UDIRS"},
              { position : "2", correctWord :"VENUS",question : " ENU ",missinglettersCount :"2",missingletters :"SOARS"},
              { position : "3", correctWord :"EARTH",question : "E R H",missinglettersCount :"2",missingletters :"TEATO"},
              { position : "4", correctWord :"JUPITER",question : " UP TE ",missinglettersCount :"3",missingletters :"RIJKO"},
              { position : "5", correctWord :"NEPTUNE",question : " EPTUN ",missinglettersCount :"2",missingletters :"TONER"}]
                     }  
var mlgame22 = { id : "mlgame22", level :"1",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Solar System-2', galleryGroup : 'LEVEL - 1',
        headerMsg : "Fill in the missing letters from the right hand side options",
                       topTrayLabel : "Solar System-2",
              wordsArray : [
              { position : "1", correctWord :"SATURN", question : "S T RN", missinglettersCount :"2",missingletters :"AIEUD"},
              { position : "2", correctWord :"MARS",question : "M  S",missinglettersCount :"2",missingletters :"ANDRP"},
              { position : "3", correctWord :"PLUTO",question : " LUT ",missinglettersCount :"2",missingletters :"PONDS"},
              { position : "4", correctWord :"URANUS",question : " RAN S",missinglettersCount :"2",missingletters :"UIUYY"},
              { position : "5", correctWord :"MERCURY",question : " ER U Y",missinglettersCount :"3",missingletters :"CRMCO"}]
                     } 
var mlgame23 = { id : "mlgame23", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Bodies of Water-1', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
                      topTrayLabel : "Bodies of water",
             wordsArray : [
              { position : "1", correctWord :"OCEAN", question : " CE N", missinglettersCount :"2",missingletters :"AOSTR"},
              { position : "2", correctWord :"CHANNEL",question : "C A NE ",missinglettersCount :"3",missingletters :"HLNHD"},
              { position : "3", correctWord :"RIVER",question : "R VE ",missinglettersCount :"2",missingletters :"IMRLK"},
              { position : "4", correctWord :"SEA",question : "S A",missinglettersCount :"1",missingletters :"AEIOU"},
              { position : "5", correctWord :"POND",question : "P D",missinglettersCount :"2",missingletters :"ONOFF"}]
             }                                                                                                                                                                       

var mlgame24 = { id : "mlgame24", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Unit of Time-1', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
                      topTrayLabel : "Unit of Time",
             wordsArray : [
              { position : "1", correctWord :"DECADE", question : "D C D ", missinglettersCount :"3",missingletters :"EAERT"},
              { position : "2", correctWord :"HOUR",question : " OU ",missinglettersCount :"2",missingletters :"THEOR"},
              { position : "3", correctWord :"CENTURY",question : "CE  UR ",missinglettersCount :"3",missingletters :"NYTUI"},
              { position : "4", correctWord :"MONTH",question : " ON H",missinglettersCount :"2",missingletters :"MTSJY"},
              { position : "5", correctWord :"MILLENNIUM",question : " IL EN IUM",missinglettersCount :"3",missingletters :"LMNIU"}]
                    }                                                                                                                                                                       

var mlgame25 = { id : "mlgame25", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Unit of Time-2', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
                      topTrayLabel : "Unit of Time",
             wordsArray : [
              { position : "1", correctWord :"WEEK", question : "W  K", missinglettersCount :"2",missingletters :"AEIOE"},
              { position : "2", correctWord :"DAY",question : "D Y",missinglettersCount :"1",missingletters :"ANDEP"},
              { position : "3", correctWord :"YEAR",question : "Y  R",missinglettersCount :"2",missingletters :"EARTH"},
              { position : "4", correctWord :"SECOND",question : "S C  D",missinglettersCount :"3",missingletters :"EONDR"},
              { position : "5", correctWord :"MINUTE",question : " I UT ",missinglettersCount :"3",missingletters :"AMCEN"}]
                    }                                                                                                                                                                       

var mlgame26 = { id : "mlgame26", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Seasons', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
                      topTrayLabel : "Seasons",
             wordsArray : [
              { position : "1", correctWord :"SPRING", question : " P I G", missinglettersCount :"3",missingletters :"NSRTE"},
              { position : "2", correctWord :"SUMMER",question : " U  ER",missinglettersCount :"3",missingletters :"MSMTR"},
              { position : "3", correctWord :"AUTUMN",question : "A TU N",missinglettersCount :"2",missingletters :"MUTEX"},
              { position : "4", correctWord :"MONSOON",question : "M NS  N",missinglettersCount :"3",missingletters :"OOOPS"},
              { position : "5", correctWord :"WINTER",question : "WI  E ",missinglettersCount :"3",missingletters :"SRNTR"}]
                    }                                                                                                                                                                       

var mlgame27 = { id : "mlgame27", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Meals-Course', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
                      topTrayLabel : "Food Timings",
             wordsArray : [
              { position : "1", correctWord :"DINNER", question : " INNE ", missinglettersCount :"2",missingletters :"RDSXT"},
              { position : "2", correctWord :"BREAKFAST",question : "B E KFAS ",missinglettersCount :"3",missingletters :"TARST"},
              { position : "3", correctWord :"LUNCH",question : " UNC ",missinglettersCount :"2",missingletters :"HRLST"},
              { position : "4", correctWord :"SNACKS",question : " NACK ",missinglettersCount :"2",missingletters :"STSTS"},
              { position : "5", correctWord :"SUPPER",question : "S   ER",missinglettersCount :"3",missingletters :"IUPPQ"}]
              }                                                                                                                                                                       

var mlgame28 = { id : "mlgame28", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Human Organs-1', galleryGroup : 'LEVEL - 1',
        headerMsg : "Fill in the missing letters from the right hand side options",
                      topTrayLabel : "Sense Organs",
             wordsArray : [
              { position : "1", correctWord :"EYES", question : " YE ", missinglettersCount :"2",missingletters :"ASETV"},
              { position : "2", correctWord :"NOSE",question : "N  E",missinglettersCount :"2",missingletters :"ONDES"},
              { position : "3", correctWord :"TONGUE",question : " O G E",missinglettersCount :"3",missingletters :"NLUTO"},
              { position : "4", correctWord :"EARS",question : "E  S",missinglettersCount :"2",missingletters :"ARJWT"},
              { position : "5", correctWord :"SKIN",question : " K N",missinglettersCount :"2",missingletters :"ISTER"}]

                    } 
var mlgame29 = { id : "mlgame29", level :"2",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Human Organs-2', galleryGroup : 'LEVEL - 2',
        headerMsg : "Fill in the missing letters from the right hand side options",
                      topTrayLabel : "Human Organs",
             wordsArray : [
              { position : "1", correctWord :"STOMACH", question : " T MAC ", missinglettersCount :"3",missingletters :"HORSE"},
              { position : "2", correctWord :"LIVER",question : "L VE ",missinglettersCount :"2",missingletters :"IMRLK"},
              { position : "3", correctWord :"HEART",question : " EA T",missinglettersCount :"2",missingletters :"HRTSJ"},
              { position : "4", correctWord :"KIDNEYS",question : "K D EY ",missinglettersCount :"3",missingletters :"SINDR"},
              { position : "5", correctWord :"LUNGS",question : " UNG ",missinglettersCount :"2",missingletters :"RNTLS"}]
                    } 
var mlgame30 = { id : "mlgame30", level :"3",gametype : 'VSCUBE.MISSING-LETTERS-GAME', galleryGameLabel : 'Human Organs-3', galleryGroup : 'LEVEL - 3',
        headerMsg : "Fill in the missing letters from the right hand side options",
                      topTrayLabel : "Human Organs",
             wordsArray : [
              { position : "1", correctWord :"BRAIN", question : " RAI ", missinglettersCount :"2",missingletters :"ABNAJ"},
              { position : "2", correctWord :"SPINE",question : " P N ",missinglettersCount :"3",missingletters :"EISTN"},
              { position : "3", correctWord :"HEART",question : " EA T",missinglettersCount :"2",missingletters :"HRTSJ"},
              { position : "4", correctWord :"APPENDIX",question : "A  ENDI ",missinglettersCount :"3",missingletters :"XNMPP"},
              { position : "5", correctWord :"PANCREAS",question : "PAN  EA ",missinglettersCount :"3",missingletters :"CREST"}]

                    } 
