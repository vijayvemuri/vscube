var mqgame1 = { id : "mqgame11",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 1', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. Who is Shantanu's first wife ?",
                    "2. Who is the son of Shantanu and Ganga ?",
                    "3. What is the other name of Devavrat ?",
                    "4. Who is Shantanu's Second wife ?",
                    "5. How many sons Shantanu and Satyavati has ?"
              ],
              questionOptionsArray : [
                     ["Satyavati","Madri","Ganga","Amba"],
                     ["Devavrat","Pandu","Vidura","Vichitravirya"],
                     ["Parashar","Shantanu","Pandu","Bhishma"],
                     ["Amba","Madri","Ganga","Satyavati"],
                     ["1","2","3","4"]
              ],
              answerMappingArray : [2,0,3,3,1]
                     }
var mqgame2 = { id : "mqgame12",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 2', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. Who accepted to remain celibate through out his life ?",
                    "2. Who gave boon to Devavrat to choose the time of his death ?",
                    "3. What is the name of elder son of Shantanu and Satyavati ?",
                    "4. What is the name of younger son of Shantanu and Satyavati ?",
                    "5. Who abducted the three princesses ( Amba, Ambica and Ambalika) of a neighbouring kingdom ?"
              ],
              questionOptionsArray : [
                     ["Pandu","Devavrat","Vichitravirya","Vidura"],
                     ["Parashar","Shantanu","Vyasa","Vashishtha"],
                     ["Vidura","Devavrat","Vichitravirya","Chitrangadha"],
                     ["Chitrangadha","Devavrat","Vichitravirya","Vidura"],
                     ["Pandu","Devavrat","Vichitravirya","Chitrangadha"],
              ],
              answerMappingArray : [1,1,3,2,1]
                     }
var mqgame3 = { id : "mqgame13",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 3', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                      "1. Who refused to marry Vichitravirya ?",
                      "2. Who is the mother of Sage Vyasa ? ",
                      "3. Who is the father of Sage Vyasa ? ",
                      "4. Who is the blind son of Ambica blessed by Sage Vyasa ?",
                      "5. Who is the Pale son of Ambalika blessed by Sage Vyasa ?"
              ],
              questionOptionsArray : [
                     ["Ambalika","Amba","Ambika","Drupadi"],
                     ["Satyavati","Madri","Ganga","Amba"],
                     ["Sage Vashishtha","Shantanu","Bhishma","Sage Parashar"],
                     ["Dhritarashtra","Pandu","Vidura","Karna"],
                     ["Dhritarashtra","Pandu","Vidura","Karna"],
              ],
              answerMappingArray : [1,0,3,0,1]
                     }
var mqgame4 = { id : "mqgame14",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 4', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. Who is the intelligent son of the maid blessed by Sage Vyasa ?",
                    "2. Who is Dhritarashtra's wife ?",
                    "3. Who is Gandhari's brother ?",
                    "4. How many wives that Pandu had ?",
                    "5. Who is Pandu's fist wife ?"
              ],
              questionOptionsArray : [
                     ["Dhritarashtra","Pandu","Vidura","Karna"],
                     ["Kunti","Gandhari","Madri","Drupadi"],
                     ["Drona","Bhishma","Vidura","Shakuni"],
                     ["4","3","2","1"],
                     ["Kunti","Gandhari","Madri","Satyavati"],
              ],
              answerMappingArray : [2,1,3,2,0]
                     }
var mqgame5 = { id : "mqgame15",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 5', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. Who is Pandu's second wife ?",
                    "2. How many are Pandava princes?",
                    "3. How many are Kaurava princes?",
                    "4. Who is the daughter of Dhritarashtra and Gandhari ?",
                    "5. Who is blessed by God Dharma to Kunti ?"
              ],
              questionOptionsArray : [
                     ["Kunti","Gandhari","Madri","Drupadi"],
                     ["5","100","2","3"],
                     ["5","100","2","3"],
                     ["Dussala","Drupadi","Madri","Subhadra"],
                     ["Nakul","Bheem","Arjuna","Yudhishthir"]
              ],
              answerMappingArray : [2,0,1,0,3]
                     }
var mqgame6 = { id : "mqgame16",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 6', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. Who is blessed by God Vayu to Kunti ?",
                    "2. Who is blessed by God Indra to Kunti ?",
                    "3. Who is blessed by God Ashvins to Madri ?",
                    "4. Who is the eldest of Kaurava ?",
                    "5. Who is the second eldest of Kaurava ?"
              ],
              questionOptionsArray : [
                     ["Nakul","Bheem","Arjuna","Yudhishthir"],
                     ["Nakul","Bheem","Arjuna","Yudhishthir"],
                     ["Nakul and Sahadev","Bheem","Arjuna","Yudhishthir"],
                     ["Duryodhana","Dushasana","Dushyanta","Drupada"],
                     ["Duryodhana","Dushasana","Dushyanta","Drupada"]
              ],
              answerMappingArray : [1,2,0,0,1]
                     }
var mqgame7 = { id : "mqgame17",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 7', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. Who is the eldest of Pandavas ?",
                    "2. Who is the second eldest of Pandavas ?",
                    "3. Who is the middle one of Pandavas ?",
                    "4. Who is the second youngest of Pandavas ?",
                    "5. Who is the youngest of Pandavas ?"
              ],
              questionOptionsArray : [
                     ["Nakul","Bheem","Arjuna","Yudhishthir"],
                     ["Nakul","Bheem","Arjuna","Yudhishthir"],
                     ["Nakul","Bheem","Arjuna","Yudhishthir"],
                     ["Nakul","Bheem","Arjuna","Yudhishthir"],
                     ["Nakul","Bheem","Arjuna","Sahadev"],
              ],
              answerMappingArray : [3,1,2,0,3]
                     }
var mqgame8 = { id : "mqgame18",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 8', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. Who is the first Guru of Kauravas and Pandavas ? ",
                    "2. Who taught warfare and archery to Kauravas and Pandavas ?",
                    "3. Who is the son of Kunti blessed by Lord Surya before Kunti's marriage ?",
                    "4. Who offered Kingdom of vassal to Karna ?",
                    "5. Who tried and failed to poison Bheem in childhood ?"
              ],
              questionOptionsArray : [
                     ["Krupa","Drona","Vashishtha","Viswamitra"],
                     ["Krupa","Drona","Vashishtha","Viswamitra"],
                     ["Karna","Bheem","Arjuna","Yudhishthir"],
                     ["Yudhishthir","Dushasana","Dushyanta","Duryodhana"],
                     ["Dushasana","Duryodhana","Dushyanta","Yudhishthir"],
              ],
              answerMappingArray : [0,1,0,3,1]
                     }

var mqgame9 = { id : "mqgame19",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 9', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. Who planned to kill Kunti Pandavas by setting up fire to the palace ?",
                    "2. Who releaved the setting up fire to palace plan to Pandavas?",
                    "3. Who killed Bakasura ?",
                    "4. What is the name of the daemon that Bheem married to ? ",
                    "5. Who is the son of Bheem and Hidimbi ? "
              ],
              questionOptionsArray : [
                     ["Dushasana","Duryodhana","Dushyanta","Dhritarashtra"],
                     ["Karna","Dhritarashtra","Vidura","Drona"],
                     ["Krishna","Bheem","Arjuna","Duryodhana"],
                     ["Hidimbi","Putana","Manasa","Drupadi"],
                     ["Kaliya","Bakasura","Hidimba","Ghatotkacha"]
              ],
              answerMappingArray : [1,2,1,0,3]
                     }
var mqgame10 = { id : "mqgame20",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Mahabhart Quiz - 10', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. Who shot the fish at Draupadi Swayamvar ?",
                    "2. Who is Draupadi's twin ? ",
                    "3. What is the capital of Kaurava's kingdom?",
                    "4. What is the capital of Pandava's new kingdom?",
                    "5. Who lost everything (wealth,kingdom, family ) in Dice game ?"
              ],
              questionOptionsArray : [
                     ["Nakul","Bheem","Arjuna","Yudhishthir"],
                     ["Dhrishtadyumna","Arjuna","Bheem","Nakul"],
                     ["Hastinapur","Ayodhya","Indraprastha","Kosala"],
                     ["Hastinapur","Ayodhya","Indraprastha","Kosala"],
                     ["Shakuni","Duryodhana","Arjuna","Yudhishthir"],
              ],
              answerMappingArray : [2,0,0,2,3]
              }
            
