var mixmatchLabelsgame1 = { id : "mixmatchLabelsgame1",gametype : 'VSCUBE.MIX-LABEL-MATCHING-GAME', galleryGameLabel : 'Continents-Oceans-Countries', galleryGroup : 'MIXED MATCHING GAMES',
			  headerMsg : "Drag and drop the Indian Award names from bottom tray to the yellow boxes.",
			  topTray1Label : "Civilian Awards",
			  topTray2Label : "Military Awards",
			  topTray3Label : "Sports Awards",
              bottomTrayLabel : "Award Names",
              bottomTrayOptionsArray : ["Bharat Ratna","Rajiv Gandhi Khel Ratna","Vir Chakra","Padma Shri",
              "Arjuna","Ashok Chakra","Kirti Chakra","Padma Vibhushan","Dronacharya","Param Vir Chakra",
              "Dhyan Chand","Shaurya Chakra","Maha Vir Chakra","Padma Bhushan","C.K. Nayudu"],
              answerMappingArray : [[0,3,7,13],[2,5,6,9,11,12],[1,4,8,10,14]]
			}
var mixmatchLabelsgame2 = { id : "mixmatchLabelsgame2",gametype : 'VSCUBE.MIX-LABEL-MATCHING-GAME', galleryGameLabel : 'Indian Awards - Category', galleryGroup : 'MIXED MATCHING GAMES',
			  headerMsg : "Drag and drop the items from bottom tray to the yellow boxes.",
			  topTray1Label : "Continents",
			  topTray2Label : "Oceans",
			  topTray3Label : "Countries",
              bottomTrayLabel : "Names",
              bottomTrayOptionsArray : ["Atlantic","USA","North America","United Kingdom","Arctic","Europe",
              "Indian","Japan","Pacific","India","Australia","Canada","Southern","China","France",
              "South America","Asia","Africa","Antarctica","Germany"],
              answerMappingArray : [[2,5,10,15,16,17,18],[0,4,6,8,12,],[1,3,7,9,11,13,14,19]]
			}			
var mixmatchLabelsgame3 = { id : "mixmatchLabelsgame3",gametype : 'VSCUBE.MIX-LABEL-MATCHING-GAME', galleryGameLabel : 'Fruits-Vegetables-Fast Food-1', galleryGroup : 'MIXED MATCHING GAMES',
			  headerMsg : "Drag and drop the items from bottom tray to the yellow boxes.",
			  topTray1Label : "Fruits",
			  topTray2Label : "Vegetables",
			  topTray3Label : "Fast Food",
              bottomTrayLabel : "Names",
              bottomTrayOptionsArray : ["Mushrooms","Cherries","Potatoes","SpringRoll","Carrots","Tomotoes","Beans","Kiwi","Pasta","Watermelon","Quesadillas","Apple","FrenchFries","Tacos","Mango"],
              answerMappingArray : [[1,7,11,14,9],[0,2,4,5,6],[3,8,10,12,13]]
			}			
var mixmatchLabelsgame4 = { id : "mixmatchLabelsgame4",gametype : 'VSCUBE.MIX-LABEL-MATCHING-GAME', galleryGameLabel : 'Fruits-Vegetables-Fast Food-2', galleryGroup : 'MIXED MATCHING GAMES',
			  headerMsg : "Drag and drop the items from bottom tray to the yellow boxes.",
			  topTray1Label : "Fruits",
			  topTray2Label : "Vegetables",
			  topTray3Label : "Fast Food",
              bottomTrayLabel : "Names",
              bottomTrayOptionsArray : ["Cake","Figs","Cabbages","Burger","Blueberries","FriedRice","Banana","Pizza","SweetCorn","JackFruit","Spinach","Grapes","Onions","IceCream","Lettuce"],
              answerMappingArray : [[1,4,6,9,11],[2,8,10,12,14],[0,3,5,7,13]]
			}		
var mixmatchLabelsgame5 = { id : "mixmatchLabelsgame5",gametype : 'VSCUBE.MIX-LABEL-MATCHING-GAME', galleryGameLabel : 'Birds-Animals-Land-Water-1', galleryGroup : 'MIXED MATCHING GAMES',
			  headerMsg : "Drag and drop the items from bottom tray to the yellow boxes.",
			  topTray1Label : "Land Animals",
			  topTray2Label : "Water Animals",
			  topTray3Label : "Birds",
              bottomTrayLabel : "Names",
              bottomTrayOptionsArray : [],
              answerMappingArray : [[],[],[]]
			}
var mixmatchLabelsgame6 = { id : "mixmatchLabelsgame6",gametype : 'VSCUBE.MIX-LABEL-MATCHING-GAME', galleryGameLabel : 'Birds-Animals-Land-Water-2', galleryGroup : 'MIXED MATCHING GAMES',
			  headerMsg : "Drag and drop the items from bottom tray to the yellow boxes.",
			  topTray1Label : "Land Animals",
			  topTray2Label : "Water Animals",
			  topTray3Label : "Birds",
              bottomTrayLabel : "Names",
              bottomTrayOptionsArray : [],
              answerMappingArray : [[],[],[]]
			}				