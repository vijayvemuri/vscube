var qgame1 = { id : "qgame1",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Harry Potter -1', galleryGroup : 'Harry Potter',
			  headerMsg : "Harry Potter Quiz by : Aditya.T, Chirec School, Hyd.",
              questionsArray: [
              "1. Who are Harry Potter's best friends ?",
              "2. Who is Harry Potter's father ?",
              "3. Who killed Harry Potter's parents ?",
              "4. What is the sixth book  name in the Harry Potter series ?",
              "5. Who is the head master of hogwarts ?",
              ],
              questionOptionsArray : [
	              ["A) Draco Malfoy","B) Ron Weasley","C) Hermione Granger","D) B & C "],
	              ["John Potter","James Potter","Joseph Potter","Lary Potter"],
	              ["Lord Voldemort","Bill Weasley","Bellatrix Lestrange","Igor Karkaroff"],
	              ["Harry Potter and the Order of the Phoenix","Harry Potter and the Sorcerer's Stone"," Harry Potter and the Half-Blood Prince","Harry Potter and the Deathly Hallows"],
	              ["Lockhart ","McGonagall","Percy Weasley","Dumbledore"]
              ],
              answerMappingArray : [3,1,0,2,3]
			}
var qgame2 = { id : "qgame2",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Harry Potter -2', galleryGroup : 'Harry Potter',
                       headerMsg : "Harry Potter Quiz by : Srinidhi P, New Horizon Gurukul School. Bangolore",
              questionsArray: [
              "1. Alohamora is used to ?",
              "2. Leviosa is used for ?",
              "3. Expecto Patronum is used for ?",
              "4. What was luna loved good patron ?",
              "5. What spell was used by Voldemort to kill Harry's parents?",
              ],
              questionOptionsArray : [
                     ["Kill","Fly things","Open locks","None of these "],
                     ["Kill","Fly things","Open locks","None of these "],
                     ["Kill","Fly things","Patrons Charm","Open locks"],
                     ["Horse","Rabbit","Cat","Dog"],
                     ["Wingardium leviosa ","Avada kedavra","Expecto patronum","Alohomora"]
              ],
              answerMappingArray : [2,1,2,1,1]
                     }
