var thgame1 = { id : "thgame1",
			  hintSeqArray : ["Cow","Duck","Tiger","Monkey","Elephant"],
              questionsArray: [
              "What is the name of Rama's wife ?",
              "What is the name of Rama's father ?",
              "What is the name of Pandava's wife ?",
              "Who is the son of Dronaacharya ?",
              "What is the name of Bhishma's father ?",
              ],
              questionOptionsArray : [
	              ["Surpanaka","Sita","Kunti","Drupadi"],
	              ["Dasaradha","Lakshmana","Krishna","Hanuman"],
	              ["Kunti","Drupadi","Sita","Sasirekha"],
	              ["Arjuna","Aswaddhama","Rama","Karna"],
	              ["Pandu","Shakuni","Vidura","Shantanu"]
              ],
              answerMappingArray : [1,0,1,1,3]
			}
