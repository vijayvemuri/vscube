           
var rqgame1 = { id : "rqgame21",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 1', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. What is the name of Rama's Father ?",
              "2. What is the name of Rama's Wife ?",
              "3. What is the name of Rama's Mother ?",
              "4. How many brothers Rama has ?",
              "5. In the below Options, Who is the NOT the brother of Rama ?",
              ],
              questionOptionsArray : [
                     ["Ravana","Dasaradha","Bharata","Viswamitra"],
                     ["Kunti","Drupadi","Sita","Sasirekha"],
                     ["Sumitra","Kaikeyi","Janaki","Kausalya"],
                     ["4","3","2","1"],
                     ["Hanuman","Lakshmana","Shatrugna","Bharata"]
              ],
              answerMappingArray : [1,2,3,1,0]
                     }
var rqgame2 = { id : "rqgame22",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 2', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. How many Sons Rama has ?",
              "2. What is the name of Lakshmna's Wife ?",
              "3. What is the name of Bharata's Wife ?",
              "4. What it the name of Shatrughna's Wife ?",
              "5. What is the name of Hanuma's Mother ?",
              ],
              questionOptionsArray : [
                     ["3","2","1","4"],
                     ["Sita","Lakshmi","Urmila","Kaikeyi"],
                     ["Mandavi","Urmila","Suneyana","Shrutakeerti"],
                     ["Suneyana","Shrutakeerti","Mandavi","Sita"],
                     ["Sita","Mandara","Anjana","Kousalya"]
              ],
              answerMappingArray : [1,2,0,1,2]
                     }
var rqgame3 = { id : "rqgame23",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 3', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. What is the other name of Sita ?",
              "2. What is name of Lakshmna's Mother ?",
              "3. What is name of Bharata's Mother ?",
              "4. What is name of Shatrughna's Mother ?",
              "5. Who is the twin brother of Lakshmna ?",
              ],
              questionOptionsArray : [
                     ["Urmila","Janaki","Mandavi","Kousalya"],
                     ["Sumitra","Kousalya","Kaikeyi","Urmila"],
                     ["Sumitra","Kousalya","Kaikeyi","Urmila"],
                     ["Sumitra","Kousalya","Kaikeyi","Urmila"],
                     ["Hanuman","Shatrughna","Rama","Bharata"]
              ],
              answerMappingArray : [1,0,2,0,1]
                     }
var rqgame4 = { id : "rqgame24",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 4', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. Who suggesed Kaikeyi to ask Rama to go Vanawas ?",
              "2. Who asked Rama to go to Vanawas ?",
              "3. How many years of anawas that Rama was asked to go ?",
              "4. Who followed Rama when he is on his way to Vanawas ?",
              "5. Who abducted Sita from forest ?",
              ],
              questionOptionsArray : [
                     ["Dasaradha","Mandhara","Sumitra","Bharata"],
                     ["Dasaradha","Lakshmana","Kaikeyi","Bharata"],
                     ["12","7","14","10"],
                     ["Hanuman","Shatrughna","Lakshmana","Bharata"],
                     ["Hanuman","Mareech","Shurpanakha","Ravana"]
              ],
              answerMappingArray : [1,0,2,2,3]
                     }
var rqgame5 = { id : "rqgame25",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 5', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. What is the name of Ravana's wife ?",
              "2. What is the name of Ravana's brother ?",
              "3. What is the name of Ravana's son ?",
              "4. What is the name of Ravana's sister ?",
              "5. To whom Ravana offer prayers Daily ?",
              ],
              questionOptionsArray : [
                     ["Surpanaka","Sita","Mandodari","Sarama"],
                     ["Indrajit","Vibhishana","Mareech","sugreeva"],
                     ["Kumbhakarna","Vibhishana","Mareech","Indrajit"],
                     ["Indrajit","Mandodari","Shurpanakha","Sarama"],
                     ["Siva","Rama","Brahma","Vishnu"]
              ],
              answerMappingArray : [2,1,3,2,0]
                     }
var rqgame6 = { id : "rqgame26",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 6', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. Who is Rama's Guru ?",
              "2. Who offered fruits to Rama in forest ?",
              "3. Who helped Rama to cross the River in Forest ?",
              "4. Who faught with Ravana when he is abducting Sita ?",
              "5. What is the name of Vali's Brother ?",
              ],
              questionOptionsArray : [
                     ["Viswamitra","Vashishtha","Valmiki","Dasaradha"],
                     ["Sabari","Guha","Jatayu","Hanuman"],
                     ["Sabari","Hanuman","Jatayu","Guha"],
                     ["Hanuman","Guha","Jatayu","Sabari"],
                     ["Angada","Sugriva","Hanuman","Rama"]
              ],
              answerMappingArray : [1,0,3,2,1]
                     }
var rqgame7 = { id : "rqgame27",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 7', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. Which place is under Vali's rule ?",
              "2. What is the name of Vali's son ?",
              "3. Who is sugreeva's brother ?",
              "4. Where was Rama born ?",
              "5. Where was Ravana living ?",
              ],
              questionOptionsArray : [
                     ["Lanka","Kosala","Kishkindha","Midhila"],
                     ["Angada","Sugriva","Hanuman","Rama"],
                     ["Hanuman","Lakshmana","Vali","Angada"],
                     ["Kishkindha","Panchavati","Ayodhya","Midhila"],
                     ["Kosala","Lanka","Kishkindha","Midhila"]
              ],
              answerMappingArray : [2,0,2,2,1]
                     }
var rqgame8 = { id : "rqgame28",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 8', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. What is the name of Sita's Father ?",
              "2. Who wrote Ramayana ?",
              "3. What is the name of Vibhishana's wife ?",
              "4. Which dynasty that Rama belongs to ?",
              "5. Who crossed the ocean in search of Sita ?",
              ],
              questionOptionsArray : [
                     ["Dasaradha","Valmiki","Janak","Hanuman"],
                     ["Viswamitra","Vashishtha","Valmiki","Dasaradha"],
                     ["Surpanaka","Tara","Mandodari","Sarama"],
                     ["Ikshvaku","Yadava","Nanda","Mauryan"],
                     ["Angada","Vali","Sugreeva","Hanuman"],
              ],
              answerMappingArray : [2,2,3,0,3]
                     }

var rqgame9 = { id : "rqgame29",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 9', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. What is the name of Ravana's brother that sleeps most of the time ?",
              "2. Who set Lanka on fire ?",
              "3. What is the name of the mountain that Hanuma brought for Lakshmana ?",
              "4. Which idntity of Rama shown to Sita in Lanka ?",
              "5. Which Sage asked Rama to protect his Yaga ?",
              ],
              questionOptionsArray : [
                     ["Indrajit","Vibhishana","Mareech","Kumbhakarna"],
                     ["Hanuman","Vali","Sugreeva","Angada"],
                     ["Everest","Sanjeevini","Himalayas","Trisul"],
                     ["Ring","Bow","Dress","Paduka"],
                     ["Agastya","Vashishtha","Valmiki","Viswamitra"]
              ],
              answerMappingArray : [3,0,1,0,3]
                     }
var rqgame10 = { id : "rqgame30",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'Ramayan Quiz - 10', galleryGroup : 'INDIAN MYTHOLOGY',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
              "1. How many heads Ravana has ?",
              "2. What is the weapon that Rama uses?",
              "3. Who broke the devine bow of Lord Siva ?",
              "4. Who killed Vali ?",
              "5. What is the name of Rama's Kindom ?",
              ],
              questionOptionsArray : [
                     ["1","10","14","100"],
                     ["Sword","Bow and Arrow","Trisul","Sudarsan Chakra"],
                     ["Ravana","Vibhishana","Rama","Lakshmana"],
                     ["Rama","Sugreeva","Angada","Ravana"],
                     ["Lanka","Kosala","Kishkindha","Midhila"]
              ],
              answerMappingArray : [1,1,2,0,1]
              }
              
