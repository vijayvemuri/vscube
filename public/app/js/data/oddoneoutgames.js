var ogame1 = { id : "ogame1",level:"2",gametype : 'VSCUBE.ODD-ONE-OUT-GAME', galleryGameLabel : 'Game-1', galleryGroup : 'BASIC',
			  headerMsg : "Try to identify the Odd One from the list",
              questionOptionsArray : [
	              ["paper","pen","pencil","crayon"],["bungalow","cottage","farm","hut"],
	              ["Earth","Moon","Pluto","Mars"],["flute","guitar","bugle","trumpet"],
	              ["jacket ","shirt ","cloth","trousers"]
              ],
              reasonsArray : ["Paper is the odd one out because it is the medium on which we write or draw. Crayon, pen and pencil are devices with which we write or draw.",
              				  "Farm is the odd one out because it usually refers to piece of cultivated land. Bungalow, cottage and hut are types of houses",
              				  "Moon is the odd one out because it is a satellite. Earth, Mars and Pluto are planets.",
              				  "Guitar is the odd one out because it is the only stringed musical instrument. Bugle, flute and trumpet are wind instruments played by blowing",
              				  "Cloth is the odd one out because shirt, trousers and jacket are garments made from cloth."
              ],
              answerMappingArray : [0,2,1,1,2]
			}

var ogame2 = {
  "id" : "ogame2",gametype : 'VSCUBE.ODD-ONE-OUT-GAME', galleryGameLabel : 'Game-2', galleryGroup : 'BASIC',
  "headerMsg" : "Try to identify the Odd One from the list",
  "questionOptionsArray" : [ [ "Cat", "Dog", "Fox", "Rabbit" ], [ "Sword", "Fork", "Knife", "Spoon" ], [ "Whale", "Snake", "Fish", "Crocodile" ], [ "Rectangle", "Square", "Triangle", "Circle" ], [ "Tiger", "Cow", "Wolf ", "Lion" ] ],
  "reasonsArray" : [ "Except fox rest are pets.", "All except sword are used in kitchen", "All except Whale lay eggs.", "All except Circle has sides", "Tiger, Lion and Wolf all are carnivores whereas Cow is an herbivore." ],
  "answerMappingArray" : [ 2, 0, 0, 3, 1 ]
}

var ogame3 = {
  "id" : "ogame3",gametype : 'VSCUBE.ODD-ONE-OUT-GAME', galleryGameLabel : 'Game-3', galleryGroup : 'BASIC',
  "headerMsg" : "Try to identify the Odd One from the list",
  "questionOptionsArray" : [ [ "Eyes", "Nose", "Ear", "Brain" ], [ "Mobile", "Spoon", "T.V", "iPad" ], [ "Eyes", "Lily", "Lotus", "Rose" ], [ "Chair", "Sofa", "Door", "Bed" ], [ "Fan", "Light", "Bed", "Fridge" ] ],
  "reasonsArray" : [ "Brain is an internal Organ", "Spoon is used in kitchen", "Except Eyes all the remaining are flowers", "Except Door all the remaining are furniture", "Except Bed all the remaining needs eletricity to work" ],
  "answerMappingArray" : [ 3, 1, 0, 2, 2 ]
}

var ogame4 = {
  "id" : "ogame4",gametype : 'VSCUBE.ODD-ONE-OUT-GAME', galleryGameLabel : 'Game-4', galleryGroup : 'BASIC',
  "headerMsg" : "Try to identify the Odd One from the list",
  "questionOptionsArray" : [ [ "Soccer", "Badminton", "Boxing", "Tennis" ], [ "Football", "VolleyBall", "Handball", "Dance" ], [ "Swimming", "Water Polo", "Running", "Boating" ], [ "LongJump", "Shot Put", "discos", "javelin" ], [ "Billiard", "Wrestling", "Pool", "Table Tennis" ] ],
  "reasonsArray" : [ "Except Soccer all other sports played indoor", "Dance is not a sport", "Execpt Running all other are water sports", "Except Long Jump all other involves throwing an object", "Except Wrestling all other played on Table" ],
  "answerMappingArray" : [ 0, 3, 2, 0, 1 ]
}
var ogame5 = {
  "id" : "ogame5",gametype : 'VSCUBE.ODD-ONE-OUT-GAME', galleryGameLabel : 'Game-5', galleryGroup : 'BASIC',
  "headerMsg" : "Try to identify the Odd One from the list",
  "questionOptionsArray" : [ [ "Lake", "River", "Pool", "Pond" ], [ "Cottage", "Hut", "Palace", "School" ], [ "Eagle", "Ostrich", "Penguin", "Kiwi" ], [ "Lamb", "Turtle", "Colt", "Bitch" ], [ "Fox", "Tiger", "Deer", "Lion" ] ],
  "reasonsArray" : [ "All except River contain stagnant water", "All except School are dwelling places.", "All Except Eagle can not fly", "Except Bitch all the others are young ones of animals", "Except Deer all the other are flesh eating animals" ],
  "answerMappingArray" : [ 1, 3, 0, 3, 2 ]
}