var imgame1 = { id : "imgame1",gametype : 'VSCUBE.IMG-MATCHING-GAME', galleryGameLabel : 'Animals', galleryGroup : 'PICTURE MATCHING GAMES',
			  headerMsg : "Try matching the correct Animal and the image.",
			  topTrayLabel : "Animal Names",
              topTrayOptionsArray : ["Tiger","Lion","Elephant","Horse","Cow","Sheep","Monkey","Dog","Cat","Duck"],
			  bottomTrayLabel : "Images",
              bottomTrayOptionsArray : ["Cat.jpg","Cow.jpg","Tiger.jpg","Horse.jpg","Dog.jpg","Monkey.jpg","Lion.jpg","Duck.jpg","Elephant.jpg","Sheep.jpg"],
              answerMappingArray : [2,6,8,3,1,9,5,4,0,7]
			}

var imgame2 = { id : "imgame2",gametype : 'VSCUBE.IMG-MATCHING-GAME', galleryGameLabel : 'Clocks - Time -2', galleryGroup : 'PICTURE MATCHING GAMES',
			  headerMsg : "Try matching the time what clock shows HH:MM:SS",
			  topTrayLabel : "Timing in Hours : Minutes : Seconds",
              topTrayOptionsArray : ["04:40:20","00:00:00","02:15:38","06:00:05","03:45:00","00:09:00","03:00:00","05:27:16","10:10:00","07:45:30"],
			  bottomTrayLabel : "Clock Images",
              bottomTrayOptionsArray : ["clock000000.jpg","clock030000.jpg","clock034500.jpg","clock021538.jpg","clock044020.jpg","clock052716.jpg","clock074530.jpg","clock090000.jpg","clock101000.jpg","clock060005.jpg"],
              answerMappingArray : [4,0,3,9,2,7,1,5,8,6]
			}
var imgame3 = { id : "imgame3",gametype : 'VSCUBE.IMG-MATCHING-GAME', galleryGameLabel : 'Home Appliances', galleryGroup : 'PICTURE MATCHING GAMES',
			  headerMsg : "Try matching the corrrect home appliance and the image.",
			  topTrayLabel : "Home appliance Names",
              topTrayOptionsArray : ["Computer","Fridge","Sofa","Washing Machine","Oven","Clock","Phone","Stove","Table","Cot"],
			  bottomTrayLabel : "Home appliance Images",
              bottomTrayOptionsArray : ["clock.jpg","stove.jpg","oven.jpg","phone.jpg","sofa.jpg","computer.jpg","washingmachine.jpg","fridge.jpg","cot.jpg","table.jpg"],
              answerMappingArray : [5,7,4,6,2,0,3,1,9,8]
			}
var imgame4 = { id : "imgame4",gametype : 'VSCUBE.IMG-MATCHING-GAME', galleryGameLabel : 'Clocks - Time -1', galleryGroup : 'PICTURE MATCHING GAMES',
			  headerMsg : "Try matching the time what clock shows..",
			  topTrayLabel : "Timing in Hours",
              topTrayOptionsArray : ["3'o Clock","8'o Clock","2'o Clock","5'o Clock","10'o Clock","4'o Clock","12'o Clock","7'o Clock","11'o Clock","1'o Clock"],
			  bottomTrayLabel : "Clock Images",
              bottomTrayOptionsArray : ["clock1.jpg","clock4.jpg","clock5.jpg","clock2.jpg","clock8.jpg","clock12.jpg","clock10.jpg","clock3.jpg","clock11.jpg","clock7.jpg"],
              answerMappingArray : [7,4,3,2,6,1,5,9,8,0]
			}						
var imgame5 = { id : "imgame5",gametype : 'VSCUBE.IMG-MATCHING-GAME', galleryGameLabel : 'Basic Shapes', galleryGroup : 'PICTURE MATCHING GAMES',
			  headerMsg : "Try matching the Shape ",
			  topTrayLabel : "Shape Names",
              topTrayOptionsArray : ["Triangle","Square","Rectangle","Circle","Oval","Daimond","Pentagon","Star","Hexagon","Heart"],
			  bottomTrayLabel : "Shape Images",
              bottomTrayOptionsArray : ["circle.JPG","triangle.jpg","pentagon.JPG","hexagon.JPG","rectangle.JPG","heart.JPG","star.JPG","square.JPG","oval.JPG","daimond.JPG"],
              answerMappingArray : [1,7,4,0,8,9,2,6,3,5]
			}				
var imgame6 = { id : "imgame6",gametype : 'VSCUBE.IMG-MATCHING-GAME', galleryGameLabel : 'Birds -1', galleryGroup : 'PICTURE MATCHING GAMES',
			  headerMsg : "Try matching the Birds ",
			  topTrayLabel : "Birds Names",
              topTrayOptionsArray : ["cuckoos","eagle","chicken","gulls","penguin","crow","woodpecker","stork","sparrow","parrot"],
			  bottomTrayLabel : "Birds Images",
              bottomTrayOptionsArray : ["chicken.jpg","crow.jpg","cuckoos.jpg","eagle.jpg","gulls.jpg","parrot.jpg","penguin.jpg","sparrow.jpg","stork.jpg","woodpecker.jpg"],
              answerMappingArray : [2,3,0,4,6,1,9,8,7,5]
			}				
var imgame7 = { id : "imgame7",gametype : 'VSCUBE.IMG-MATCHING-GAME', galleryGameLabel : 'Birds -2', galleryGroup : 'PICTURE MATCHING GAMES',
			  headerMsg : "Try matching the Birds ",
			  topTrayLabel : "Birds Names",
              topTrayOptionsArray : ["falcon","owl","bulbul","hummingbird","kiwi","sandpiper","swan","duck","crane","goose"],
			  bottomTrayLabel : "Birds Images",
              bottomTrayOptionsArray : ["bulbul.jpg","crane.jpg","duck.jpg","falcon.jpg","goose.jpg","hummingbird.jpg","kiwi.jpg","owl.jpg","sandpiper.jpg","swan.jpg"],
              answerMappingArray : [3,7,0,5,6,8,9,2,1,4]
			}				
var imgame8 = { id : "imgame8",gametype : 'VSCUBE.IMG-MATCHING-GAME', galleryGameLabel : 'Vehicles', galleryGroup : 'PICTURE MATCHING GAMES',
				headerMsg : "Try matching the Vehicles ",
				topTrayLabel : "Vehicle Names",
				topTrayOptionsArray : ["Parachute","Train","aeroplane","Bicycle","Helicopter","Glider",
				"Ship","Boat","Bus","Auto"],
				bottomTrayLabel : "Vehicle Images",
				bottomTrayOptionsArray : ["aeroplane.jpg","Auto.jpg","Bicycle.jpg","Boat.jpg",
				"Bus.jpg","Glider.jpg","Helicopter.jpg","Train.jpg","Ship.jpg","Parachute.jpg"],
				answerMappingArray : [9,7,0,2,6,5,8,3,4,1]
}				

									