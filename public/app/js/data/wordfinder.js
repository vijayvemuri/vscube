var wfgame1 = { id : "wfgame1",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Colors', galleryGroup : '',
			  headerMsg : "Please identify the color names from the letters grid",
			  topTrayLabel : "Colors",
        wordsArray : ["RED","BLUE","WHITE","ORANGE","PURPLE","GREEN","VIOLET","YELLOW","BLACK","PINK"],
        gridRowsArray : ["WZJYELLOWJ","HYZPURPLEM","IPUKLREDGH","TBLACKYNBS","EGREENBTWM","JBLUEXJZBT","GHANLSFLYG","FWVIOLETXS","QNGORANGEU","YEPINKBTJX"],
        positionsArray : [["25","26","27"],["51","52","53","54"],["00","10","20","30","40"],["83","84","85","86","87","88"],["13","14","15","16","17","18"],["41","42","43","44","45"],["72","73","74","75","76","77"],["03","04","05","06","07","08"],["31","32","33","34","35"],["92","93","94","95"]]
			}
var wfgame2 = { id : "wfgame2",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Animals', galleryGroup : '',
			  headerMsg : "Please identify the Animal names from the letters grid",
			  topTrayLabel : "Animals",
        wordsArray : ["ELEPHANT","HORSE","GIRAFFE","COW","DONKEY","CAMEL","TIGER","LION","GORILLA","MONKEY"],
        gridRowsArray : ["MMJHORSEFX","ELEPHANTKP","LIONJFHMOU","DZYTIGERMN","DONKEYJATK","GIRAFFEFZI","GORILLAINK","FTKNYCOWTG","VCRCAMELNX","KMONKEYIJX"],
        positionsArray : [["10","11","12","13","14","15","16","17"],["03","04","05","06","07"],["50","51","52","53","54","55","56"],["75","76","77"],["40","41","42","43","44","45"],["83","84","85","86","87"],["33","34","35","36","37"],["20","21","22","23"],["60","61","62","63","64","65","66"],["91","92","93","94","95","96"]]
			}			

var wfgame3 = { id : "wfgame3",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Birds', galleryGroup : '',
			  headerMsg : "Please identify the Birds names from the letters grid",
			  topTrayLabel : "Birds",
        wordsArray : ["SQUIRREL","TURKEY","PARROT","OWL","BULBUL","OSTRICH","STORK","CROW","CRANE","SPARROW"],
        gridRowsArray : ["SPARROTBHC","QCROWHTTWG","UXSTORKOZC","IVEJNKOGLI","ROSTRICHGB","RSPARROWOJ","ERBULBULXQ","LYBTURKEYK","CRANEPVXLZ","TAHLOOWLBH"],
        positionsArray : [["00","10","20","30","40","50","60","70"],["73","74","75","76","77","78"],["01","02","03","04","05","06"],["95","96","97"],["62","63","64","65","66","67"],["41","42","43","44","45","46","47"],["22","23","24","25","26"],["11","12","13","14"],["80","81","82","83","84"],["51","52","53","54","55","56","57"]]
			}			

var wfgame4 = { id : "wfgame4",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Countries', galleryGroup : '',
			  headerMsg : "Please identify the Countries names from the letters grid",
			  topTrayLabel : "Countries",
        wordsArray : ["SINGAPORE","NEPAL","INDIA","SRILANKA","PAKISTAN","AUSTRALIA","CANADA","GERMANY","JAPAN","CHINA"],
        gridRowsArray : ["BPAKISTANQ","COJCHINATY","GERMANYJVY","SRILANKASX","IEBZINDIAW","SINGAPORET","AUSTRALIAG","KJAPANCUVM","NEPALVDDWS","FVYCANADAE"],
        positionsArray : [["50","51","52","53","54","55","56","57","58"],["80","81","82","83","84"],["44","45","46","47","48"],["30","31","32","33","34","35","36","37"],["01","02","03","04","05","06","07","08"],["60","61","62","63","64","65","66","67","68"],["93","94","95","96","97","98"],["20","21","22","23","24","25","26"],["71","72","73","74","75"],["13","14","15","16","17"]]
			}			

var wfgame5 = { id : "wfgame5",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Vechicles', galleryGroup : '',
			  headerMsg : "Please identify the Vechicle names from the letters grid",
			  topTrayLabel : "Vechicles",
        wordsArray : ["AEROPLANE","TRUCK","CAR","BOAT","SHIP","BUS","BICYCLE","GLIDER","TRAIN","CHARIOT"],
        gridRowsArray : ["AEROPLANEG","XLSLBOATYX","GLIDERQQVP","TCARYAKFUI","RTGJSHIPZX","UKYXDMPLHO","CWNPTRAINV","KACHARIOTV","RZDICBUSKF","TMBICYCLEV"],
        positionsArray : [["00","01","02","03","04","05","06","07","08"],["30","40","50","60","70"],["31","32","33"],["14","15","16","17"],["44","45","46","47"],["85","86","87"],["92","93","94","95","96","97","98"],["20","21","22","23","24","25"],["64","65","66","67","68"],["72","73","74","75","76","77","78"]]
			}			
var wfgame6 = { id : "wfgame6",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Human Emotions', galleryGroup : '',
			  headerMsg : "Please identify the Human Emotions from the letters grid",
			  topTrayLabel : "Human Emotions",
              wordsArray : ["Excite","joy","Surprise","Sadness","Anger","Disgust","Contempt","Fear","Shame","Guilt"],
              gridRowsArray : ["WDISGUSTXE","JWFEARLFCX","OOSADNESSC","YWUGUILTWI","FSVBMJGTTT","WNHKOPGZME","SURPRISEGG","BSHAMEESEG","CCONTEMPTH","ANGERERFKX"],
              positionsArray :[["09","19","29","39","49","59"],["10","20","30"],["60","61","62","63","64","65","66","67"],["22","23","24","25","26","27","28"],["90","91","92","93","94"],["01","02","03","04","05","06","07"],["81","82","83","84","85","86","87","88"],["12","13","14","15"],["71","72","73","74","75"],["33","34","35","36","37"]]
            }			
var wfgame7 = { id : "wfgame7",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Sports', galleryGroup : '',
headerMsg : "Please identify the Sports names from the letters grid",
topTrayLabel : "Sports",
wordsArray : ["KARATE","HOCKEY","FOOTBALL","CRICKET","RUNNING","TENNIS","VOLLYBALL","GOLF","POLO","BADMINTON"],
gridRowsArray : ["FOOTBALLTP","CRICKETOHM","BADMINTONK","KARATEEQBT","MHLGGOLFVN","ERUNNINGXU","PPOLOBVLCY","WFITENNISV","RXZHOCKEYG","VOLLYBALLL"],
positionsArray :[["30","31","32","33","34","35"],["83","84","85","86","87","88"],["00","01","02","03","04","05","06","07"],["10","11","12","13","14","15","16"],["51","52","53","54","55","56","57"],["73","74","75","76","77","78"],["90","91","92","93","94","95","96","97","98"],["44","45","46","47"],["61","62","63","64"],["20","21","22","23","24","25","26","27","28"]]
}			

var wfgame8 = { id : "wfgame8",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Indian Rivers', galleryGroup : '',
headerMsg : "Please identify the Indian River names from the letters grid",
topTrayLabel : "Indian Rivers",
wordsArray : ["Yamuna","BRAHMPUTRA","Mahanadi","Narmada","Godavari","Krishna","Kaveri","Tapti","INDUS","Ganges"],
gridRowsArray : ["YAMUNAQJTL","KAVERIYGAN","TNARMADARM","GODAVARIHZ","LQKRISHNAP","VMINDUSDQB","VMAHANADIF","BRAHMPUTRA","ZGANGESQAQ","UKTAPTINVU"],
positionsArray :[["00","01","02","03","04","05"],["70","71","72","73","74","75","76","77","78","79"],["61","62","63","64","65","66","67","68"],["21","22","23","24","25","26","27"],["30","31","32","33","34","35","36","37"],["42","43","44","45","46","47","48"],["10","11","12","13","14","15"],["92","93","94","95","96"],["52","53","54","55","56"],["81","82","83","84","85","86"]]
}			
var wfgame9 = { id : "wfgame9",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Vegetables - 1', galleryGroup : '',
headerMsg : "Please identify the Vegetable names from the letters grid",
topTrayLabel : "Vegetables -2",
wordsArray : ["peas","spinach","beans","radish","potato","pumpkin","mushroom","chilli","tomato","sprouts"],
gridRowsArray : ["QPEASHRBGI","UCHILLISES","JBTOMATOOP","BEANSAYWER","MUSHROOMWO","SPINACHVVU","EENVPIFQOT","PUMPKINTZS","RADISHAOYV","ZJJPOTATOJ"],
positionsArray :[["01","02","03","04"],["50","51","52","53","54","55","56"],["30","31","32","33","34"],["80","81","82","83","84","85"],["93","94","95","96","97","98"],["70","71","72","73","74","75","76"],["40","41","42","43","44","45","46","47"],["11","12","13","14","15","16"],["22","23","24","25","26","27"],["19","29","39","49","59","69","79"]]
}			
var wfgame10 = { id : "wfgame10",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Vegetables - 2', galleryGroup : '',
headerMsg : "Please identify the Vegetable names from the letters grid",
topTrayLabel : "Vegetables -1",
wordsArray : ["avocado","beetroot","broccoli","cabbage","carrot","cucumber","ginger","lettuce","garlic","onion"],
gridRowsArray : ["YAVOCADOBB","BWGINGERCD","XDLETTUCEA","CABBAGEWAI","IBROCCOLIK","CARROTRIXA","JBEETROOTH","ACUCUMBERK","VYFGARLICY","ONIONFCLYF"],
positionsArray :[["01","02","03","04","05","06","07"],["61","62","63","64","65","66","67","68"],["41","42","43","44","45","46","47","48"],["30","31","32","33","34","35","36"],["50","51","52","53","54","55"],["71","72","73","74","75","76","77","78"],["12","13","14","15","16","17"],["22","23","24","25","26","27","28"],["83","84","85","86","87","88"],["90","91","92","93","94"]]
}			


var wfgame11 = { id : "wfgame11",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Fruits', galleryGroup : '',
headerMsg : "Please identify the fruit names from the letters grid",
topTrayLabel : "Fruits",
wordsArray : ["Watermelon","Banana","Apple","Grapes","Jackfruit","Kiwi","Mango","Guava","Cherries","Fig"],
gridRowsArray : ["GWAKIWIATQ","RCHERRIESQ","AXIGUAVAVO","PEBPMANGOJ","EFIGNWJZAV","SMDLAPPLEX","WXZSEWQXJH","WATERMELON","JACKFRUITY","TBANANAYOP"],
positionsArray :[["70","71","72","73","74","75","76","77","78","79"],["91","92","93","94","95","96"],["54","55","56","57","58"],["00","10","20","30","40","50"],["80","81","82","83","84","85","86","87","88"],["03","04","05","06"],["34","35","36","37","38"],["23","24","25","26","27"],["11","12","13","14","15","16","17","18"],["41","42","43"]]
}			

var wfgame12 = { id : "wfgame12",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Food Items', galleryGroup : '',
headerMsg : "Please identify the food names from the letters grid",
topTrayLabel : "Food Items",
wordsArray : ["Dumplings","Sandwich","Salad","Sauce","Noodles","Pie","Soup","Dips","Puddings","Stews"],
gridRowsArray : ["SRSALADFKY","CXMLPIEQYH","SAUCEMWYUM","PUDDINGSLY","OSTEWSNOAK","FIZYDIPSVU","DUMPLINGSK","USANDWICHJ","KFHSOUPATM","BINOODLESR"],
positionsArray :[["60","61","62","63","64","65","66","67","68"],["71","72","73","74","75","76","77","78"],["02","03","04","05","06"],["20","21","22","23","24"],["92","93","94","95","96","97","98"],["14","15","16"],["83","84","85","86"],["54","55","56","57"],["30","31","32","33","34","35","36","37"],["41","42","43","44","45"]]
}			

var wfgame13 = { id : "wfgame13",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'World Cities', galleryGroup : '',
headerMsg : "Please identify the world cities from the letters grid",
topTrayLabel : "World Cities",
wordsArray : ["Singapore","Dubai","London","NewYork","Tokyo","Delhi","Beijing","Paris","Dhaka","Karachi"],
gridRowsArray : ["KARACHIDIP","VGPARISHJL","NEWYORKNHU","SINGAPOREZ","QMDUBAIEIM","DHAKAMSCRR","DDELHIUIZM","TOKYOSBZJU","ZBEIJINGUP","LONDONAQXE"],
positionsArray :[["30","31","32","33","34","35","36","37","38"],["42","43","44","45","46"],["90","91","92","93","94","95"],["20","21","22","23","24","25","26"],["70","71","72","73","74"],["61","62","63","64","65"],["81","82","83","84","85","86","87"],["12","13","14","15","16"],["50","51","52","53","54"],["00","01","02","03","04","05","06"]]
}			

var wfgame14 = { id : "wfgame14",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Europian Cities', galleryGroup : '',
headerMsg : "Please identify the Europian cities from the letters grid",
topTrayLabel : "Europian Cities",
wordsArray : ["Amsterdam","Madrid","Berlin","Venice","Vienna","Munich","Budapest","Istanbul","Athens","Rome"],
gridRowsArray : ["BUDAPESTOB","AMSTERDAMB","BERLINMVJM","KZATHENSQR","ROVENICEOP","CIQMADRIDH","MUNICHODGZ","FKVIENNADL","WISTANBULR","BABROROMEX"],
positionsArray :[["10","11","12","13","14","15","16","17","18"],["53","54","55","56","57","58"],["20","21","22","23","24","25"],["42","43","44","45","46","47"],["72","73","74","75","76","77"],["60","61","62","63","64","65"],["00","01","02","03","04","05","06","07"],["81","82","83","84","85","86","87","88"],["32","33","34","35","36","37"],["95","96","97","98"]]
}			

var wfgame15 = { id : "wfgame15",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'World currencies', galleryGroup : '',
headerMsg : "Please identify the World currencies from the letters grid",
topTrayLabel : "World currencies",
wordsArray : ["dollor","euro","pound","dinar","franc","rial","peso","rupee","yen","ringgit"],
gridRowsArray : ["PESOAQLEJK","DCAICRIALT","ISLFUVFKVH","NYHUFRANCR","AADOLLORKE","RAZJVYENNN","GRINGGITOW","EUROBPBWWZ","YDVAPOUNDP","JRUPEEDJYM"],
positionsArray :[["42","43","44","45","46","47"],["70","71","72","73"],["84","85","86","87","88"],["10","20","30","40","50"],["34","35","36","37","38"],["15","16","17","18"],["00","01","02","03"],["91","92","93","94","95"],["55","56","57"],["61","62","63","64","65","66","67"]]
}			

var wfgame16 = { id : "wfgame16",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'World Languages', galleryGroup : '',
headerMsg : "Please identify the World Languages from the letters grid",
topTrayLabel : "World Languages",
wordsArray : ["Chinese","Spanish","English","Hindi","Arabic","Portuguese","Russian","Japanese","German","French"],
gridRowsArray : ["YJAPANESEA","PRUSSIANHE","PORTUGUESE","HSPANISHHZ","INMWMQSRFQ","NGERMANKWE","DCHINESEVA","IQPFRENCHZ","XYVARABICM","XEENGLISHP"],
positionsArray :[["61","62","63","64","65","66","67"],["31","32","33","34","35","36","37"],["92","93","94","95","96","97","98"],["30","40","50","60","70"],["83","84","85","86","87","88"],["20","21","22","23","24","25","26","27","28","29"],["11","12","13","14","15","16","17"],["01","02","03","04","05","06","07","08"],["51","52","53","54","55","56"],["73","74","75","76","77","78"]]
}			

var wfgame17 = { id : "wfgame17",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Music Instruments', galleryGroup : '',
headerMsg : "Please identify the Music Instruments from the letters grid",
topTrayLabel : "Music Instruments",
wordsArray : ["Saxophone","Trumpet","Clarinet","Violin","Piano","Guitar","Drums","Bell","Keyboard","Harp"],
gridRowsArray : ["GHBELLRZKW","GETRUMPETN","SAXOPHONER","CLARINETPF","FVVDRUMSLV","ZBSGUITARB","PIANORZRRY","MZWHARPESL","OKEYBOARDZ","DPVIOLINFA"],
positionsArray :[["20","21","22","23","24","25","26","27","28"],["12","13","14","15","16","17","18"],["30","31","32","33","34","35","36","37"],["92","93","94","95","96","97"],["60","61","62","63","64"],["53","54","55","56","57","58"],["43","44","45","46","47"],["02","03","04","05"],["81","82","83","84","85","86","87","88"],["73","74","75","76"]]
}			

var wfgame18 = { id : "wfgame18",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Metals', galleryGroup : '',
headerMsg : "Please identify the Metals from the letters grid",
topTrayLabel : "Metals",
wordsArray : ["Aluminium","Platinum","Gold","Silver","Bronze","Copper","Mercury","Zinc","Iridium","Titaniun"],
gridRowsArray : ["SILVEREEGA","JXIRIDIUMW","UZQCOPPERL","YEBRONZEAJ","ALUMINIUMG","PMERCURYMG","FGOLDLZBVB","TPLATINUMM","ZINCFMHZIJ","ZTITANIUNI"],
positionsArray :[["40","41","42","43","44","45","46","47","48"],["71","72","73","74","75","76","77","78"],["61","62","63","64"],["00","01","02","03","04","05"],["32","33","34","35","36","37"],["23","24","25","26","27","28"],["51","52","53","54","55","56","57"],["80","81","82","83"],["12","13","14","15","16","17","18"],["91","92","93","94","95","96","97","98"]]
}			

var wfgame19 = { id : "wfgame19",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Human Organs', galleryGroup : '',
headerMsg : "Please identify the Human organ names from the letters grid",
topTrayLabel : "Human organs",
wordsArray : ["intestines","liver","bladder","kidneys","heart","stomach","lungs","brain","Pancreas","spine"],
gridRowsArray : ["MSTOMACHZQ","INTESTINES","BBLADDERHF","BEZHSPINEF","RPANCREASJ","AUAELIVERX","IWGLUNGSEJ","NRPBXQZWNQ","KIDNEYSAEU","SZIHEARTWV"],
positionsArray :[["10","11","12","13","14","15","16","17","18","19"],["54","55","56","57","58"],["21","22","23","24","25","26","27"],["80","81","82","83","84","85","86"],["93","94","95","96","97"],["01","02","03","04","05","06","07"],["63","64","65","66","67"],["30","40","50","60","70"],["41","42","43","44","45","46","47","48"],["34","35","36","37","38"]]
}			


var wfgame20 = { id : "wfgame20",level :"1",gametype : 'VSCUBE.WORD-FINDER-GAME', galleryGameLabel : 'Unit of Time', galleryGroup : '',
headerMsg : "Please identify the Unit of Time from the letters grid",
topTrayLabel : "Unit of Time",
wordsArray : ["Millennium","Decade","Hour","Century","Minute","Week","Month","Day","Year","Second"],
gridRowsArray : ["FMONTHYWXT","YQHTOYKDFN","VWYEARYAYQ","CENTURYYTP","MILLENNIUM","JRVYKHOURO","AWEEKLTASA","SECONDKRAB","IBMINUTEND","YQPDECADEH"],
positionsArray :[["40","41","42","43","44","45","46","47","48","49"],["93","94","95","96","97","98"],["55","56","57","58"],["30","31","32","33","34","35","36"],["82","83","84","85","86","87"],["61","62","63","64"],["01","02","03","04","05"],["17","27","37"],["22","23","24","25"],["70","71","72","73","74","75"]]
}			
