var jwgame1 = { id : "jwgame1",level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Animals-1', galleryGroup : 'LEVEL - 1',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Animals",
              wordsArray : [
              { position : "1", correctWord :"TIGER",jumbledWord :"EGITR"},
              { position : "2", correctWord :"DONKEY",jumbledWord :"KNEODY"},
              { position : "3", correctWord :"LION",jumbledWord :"NIOL"},
              { position : "4", correctWord :"COW",jumbledWord :"OWC"},
              { position : "5", correctWord :"CAMEL",jumbledWord :"EACML"}]
			}
var jwgame2 = { id : "jwgame2",level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Animals-2', galleryGroup : 'LEVEL - 2',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Animals",
              wordsArray : [
              { position : "1", correctWord :"ELEPHANT",jumbledWord :"HPLEETNA"},
              { position : "2", correctWord :"GIRAFFE",jumbledWord :"FEFIAGR"},
              { position : "3", correctWord :"HORSE",jumbledWord :"SRHOE"},
              { position : "4", correctWord :"MONKEY",jumbledWord :"NMOEYK"},
              { position : "5", correctWord :"GORILLA",jumbledWord :"GALILOR"}]
			}						
var jwgame3 = { id : "jwgame3",level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Animals-3', galleryGroup : 'LEVEL - 3',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Animals",
              wordsArray : [
              { position : "1", correctWord :"OSTRICH",jumbledWord :"ROTSHIC"},
              { position : "2", correctWord :"TURKEY",jumbledWord :"KREUTY"},
              { position : "3", correctWord :"RABBIT",jumbledWord :"TIBBAR"},
              { position : "4", correctWord :"WOLF",jumbledWord :"LOWF"},
              { position : "5", correctWord :"ZEBRA",jumbledWord :"RZEBA"}]
			}
var jwgame4 = { id : "jwgame4",level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Water Animals-1', galleryGroup : 'LEVEL - 1',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Water Animals",
              wordsArray : [
              { position : "1", correctWord :"WHALE",jumbledWord :"AWELH"},
              { position : "2", correctWord :"DOLPHIN",jumbledWord :"OPIDLHN"},
              { position : "3", correctWord :"SHARK",jumbledWord :"RAHKS"},
              { position : "4", correctWord :"FISH",jumbledWord :"HFSI"},
              { position : "5", correctWord :"CROCODILE",jumbledWord :"CCOORIELD"}]
			}
var jwgame5 = { id : "jwgame5",level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Birds-1', galleryGroup : 'LEVEL - 1',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Birds",
              wordsArray : [
              { position : "1", correctWord :"PARROT",jumbledWord :"RAPTOR"},
              { position : "2", correctWord :"OWL",jumbledWord :"WLO"},
              { position : "3", correctWord :"PENGUIN",jumbledWord :"NUNPIGE"},
              { position : "4", correctWord :"CRANE",jumbledWord :"NAREC"},
              { position : "5", correctWord :"SPARROW",jumbledWord :"RPASORW"}]
			}
var jwgame6 = { id : "jwgame6", level :"3",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Birds-2', galleryGroup : 'LEVEL - 3',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Birds",
              wordsArray : [
              { position : "1", correctWord :"BULBUL",jumbledWord :"BBUULL"},
              { position : "2", correctWord :"GULLS",jumbledWord :"ULGLS"},
              { position : "3", correctWord :"KINGFISHER",jumbledWord :"IGIHRKNFSE"},      
              { position : "4", correctWord :"STORK",jumbledWord :"ROTSK"},
              { position : "5", correctWord :"WOODPECKER",jumbledWord :"DOOWOREKECP"}]
			}
var jwgame7 = { id : "jwgame7", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Country Names-1', galleryGroup : 'LEVEL - 3',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Countries",
              wordsArray : [
              { position : "1", correctWord :"CHINA",jumbledWord :"INHCA"},
              { position : "2", correctWord :"NEPAL",jumbledWord :"APENL"},
              { position : "3", correctWord :"INDIA",jumbledWord :"DAIIN"},
              { position : "4", correctWord :"SRILANKA",jumbledWord :"IALRSAKN"},
              { position : "5", correctWord :"PAKISTAN",jumbledWord :"ATKISPNA"}]
			}
var jwgame8 = { id : "jwgame8", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Country Names-2', galleryGroup : 'LEVEL - 3',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Countries",
              wordsArray : [
              { position : "1", correctWord :"AUSTRALIA",jumbledWord :"USAATRAIL"},
              { position : "2", correctWord :"BANGLADESH",jumbledWord :"ESDALGNBH"},
              { position : "3", correctWord :"CANADA",jumbledWord :"NDCAAA"},
              { position : "4", correctWord :"GERMANY",jumbledWord :"EMARGYN"},
              { position : "5", correctWord :"JAPAN",jumbledWord :"PNAAJ"}]
			}
var jwgame9 = { id : "jwgame9", level :"3",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Country Names-3', galleryGroup : 'LEVEL - 3',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Countries",
              wordsArray : [
              { position : "1", correctWord :"MALAYSIA",jumbledWord :"YALAMAIS"},
              { position : "2", correctWord :"BANGLADESH",jumbledWord :"GADLNABHES"},
              { position : "3", correctWord :"SINGAPORE",jumbledWord :"OPAGNISER"},
              { position : "4", correctWord :"SPAIN",jumbledWord :"INAPS"},
              { position : "5", correctWord :"KENYA",jumbledWord :"NEKAY"}]
			}
var jwgame10 = { id : "jwgame10", level :"3",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Country Names-4', galleryGroup : 'LEVEL - 3',
			  headerMsg : "Identify the correct word using the jumbled letters",
			  topTrayLabel : "Countries",
              wordsArray : [
              { position : "1", correctWord :"MEXICO",jumbledWord :"IXOCEM"},
              { position : "2", correctWord :"BANGLADESH",jumbledWord :"ABNALGHSED"},
              { position : "3", correctWord :"BRAZIL",jumbledWord :"IBALZR"},
              { position : "4", correctWord :"ITALY",jumbledWord :"AIYLT"},
              { position : "5", correctWord :"AFGHANISTAN",jumbledWord :"AAAHGFSINNT"}]
			}

var jwgame11 = { id : "jwgame11", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Professions-1', galleryGroup : 'LEVEL - 2',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Professions",
              wordsArray : [
              { position : "1", correctWord :"DOCTOR",jumbledWord :"COTROD"},
              { position : "2", correctWord :"LAWYER",jumbledWord :"EWLARY"},
              { position : "3", correctWord :"ENGINEER",jumbledWord :"EEENNIGR"},
              { position : "4", correctWord :"ACTOR",jumbledWord :"TOCAR"},
              { position : "5", correctWord :"NURSE",jumbledWord :"RSUEN"}]
                     }
var jwgame12 = { id : "jwgame12", level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Transport Vehicles', galleryGroup : 'LEVEL - 1',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Transport Vehicles",
              wordsArray : [
              { position : "1", correctWord :"TRAIN",jumbledWord :"IARTN"},
              { position : "2", correctWord :"CAR",jumbledWord :"RAC"},
              { position : "3", correctWord :"AEROPLANE",jumbledWord :"ROEPAALEN"},
              { position : "4", correctWord :"BOAT",jumbledWord :"OTAB"},
              { position : "5", correctWord :"SHIP",jumbledWord :"IHPS"}]
                     }   
var jwgame13 = { id : "jwgame13", level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : '>Flowers-1', galleryGroup : 'LEVEL - 1',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Flowers",
              wordsArray : [
              { position : "1", correctWord :"ROSE",jumbledWord :"SOER"},
              { position : "2", correctWord :"LILY",jumbledWord :"YILL"},
              { position : "3", correctWord :"JASMINE",jumbledWord :"MSIENAJ"},
              { position : "4", correctWord :"MARIGOLD",jumbledWord :"IARMDOLG"},
              { position : "5", correctWord :"SUNFLOWER",jumbledWord :"FUNSWOLRE"}]
                     } 
var jwgame14 = { id : "jwgame14", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Flowers-2', galleryGroup : 'LEVEL - 2',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Flowers",
              wordsArray : [
              { position : "1", correctWord :"HIBISCUS",jumbledWord :"IBSIHSUC"},
              { position : "2", correctWord :"LAVENDER",jumbledWord :"ENVALRED"},
              { position : "3", correctWord :"ORCHID",jumbledWord :"HCRODI"},
              { position : "4", correctWord :"ROSEMARY",jumbledWord :"YAMRESOR"},
              { position : "5", correctWord :"TULIP",jumbledWord :"ILTPU"}]
                     } 
var jwgame15 = { id : "jwgame15", level :"3",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Flowers-3', galleryGroup : 'LEVEL - 3',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Flowers",
              wordsArray : [
              { position : "1", correctWord :"DAISY",jumbledWord :"IADYS"},
              { position : "2", correctWord :"CROCUS",jumbledWord :"CCORSU"},
              { position : "3", correctWord :"CAMELLIAS",jumbledWord :"LLSIAEMAC"},
              { position : "4", correctWord :"BLUEBELL",jumbledWord :"BBLLLEEU"},
              { position : "5", correctWord :"AMARYLLIS",jumbledWord :"SIYRLLAAM"}]
                     }   
var jwgame16 = { id : "jwgame16", level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'English Months', galleryGroup : 'LEVEL - 1',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Months",
              wordsArray : [
              { position : "1", correctWord :"AUGUST",jumbledWord :"GUUSTA"},
              { position : "2", correctWord :"JANUARY",jumbledWord :"UNARAJY"},
              { position : "3", correctWord :"MARCH",jumbledWord :"RCAMH"},
              { position : "4", correctWord :"DECEMBER",jumbledWord :"MECEDREB"},
              { position : "5", correctWord :"JUNE",jumbledWord :"NUJE"}]
                     }
var jwgame17 = { id : "jwgame17", level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Food Items', galleryGroup : 'LEVEL - 3',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Food items",
              wordsArray : [
              { position : "1", correctWord :"PASTA",jumbledWord :"SAPAT"},
              { position : "2", correctWord :"CAKE",jumbledWord :"KACE"},
              { position : "3", correctWord :"BREAD",jumbledWord :"AERDB"},
              { position : "4", correctWord :"PIZZA",jumbledWord :"ZAIPZ"},
              { position : "5", correctWord :"BURGER",jumbledWord :"GRUERB"}]
                     }                                                                                                                                                                                                                                                                                                                          

var jwgame18 = { id : "jwgame18", level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Sports-1', galleryGroup : 'LEVEL - 1',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Sports/Games",
              wordsArray : [
              { position : "1", correctWord :"HOCKEY",jumbledWord :"KCEOYH"},
              { position : "2", correctWord :"CRICKET",jumbledWord :"TKIRCEC"},
              { position : "3", correctWord :"FOOTBALL",jumbledWord :"LALBOTOF"},
              { position : "4", correctWord :"BADMINTON",jumbledWord :"MDABNNIOT"},
              { position : "5", correctWord :"BASKETBALL",jumbledWord :"SEKABBTLLA"}]
                     } 

var jwgame19 = { id : "jwgame19", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Sports-2', galleryGroup : 'LEVEL - 2',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Sports/Games",
              wordsArray : [
              { position : "1", correctWord :"VOLLYBALL",jumbledWord :"ALBLYOVLL"},
              { position : "2", correctWord :"BOXING",jumbledWord :"IXNOGB"},
              { position : "3", correctWord :"ARCHERY",jumbledWord :"EHCRRAY"},
              { position : "4", correctWord :"RUNNING",jumbledWord :"GRNUNNI"},
              { position : "5", correctWord :"TENNIS",jumbledWord :"STNENI"}]
                     } 

var jwgame20 = { id : "jwgame20", level :"3",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Sports-3', galleryGroup : 'LEVEL - 3',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Sports/Games",
              wordsArray : [
              { position : "1", correctWord :"SURFING",jumbledWord :"NIGRFUS"},
              { position : "2", correctWord :"GOLF",jumbledWord :"OFGL"},
              { position : "3", correctWord :"GYMNASTICS",jumbledWord :"ITSMNAYGS"},
              { position : "4", correctWord :"KARATE",jumbledWord :"TRKAAE"},
              { position : "5", correctWord :"POLO",jumbledWord :"POOL"}]
                     } 
var jwgame21 = { id : "jwgame21", level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Solar System-1', galleryGroup : 'LEVEL - 1',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Solar System-1",
              wordsArray : [
              { position : "1", correctWord :"SUN",jumbledWord :"UNS"},
              { position : "2", correctWord :"VENUS",jumbledWord :"NESUV"},
              { position : "3", correctWord :"EARTH",jumbledWord :"HEART"},
              { position : "4", correctWord :"JUPITER",jumbledWord :"IPUJRET"},
              { position : "5", correctWord :"NEPTUNE",jumbledWord :"PTENNUE"}]
                     }  
var jwgame22 = { id : "jwgame22", level :"1",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Solar System-2', galleryGroup : 'LEVEL - 1',
                       headerMsg : "Identify the correct word using the jumbled letters",
                       topTrayLabel : "Solar System-2",
              wordsArray : [
              { position : "1", correctWord :"SATURN",jumbledWord :"AUTNRS"},
              { position : "2", correctWord :"MARS",jumbledWord :"ASRM"},
              { position : "3", correctWord :"PLUTO",jumbledWord :"OPUTL"},
              { position : "4", correctWord :"URANUS",jumbledWord :"UARNSU"},
              { position : "5", correctWord :"MERCURY",jumbledWord :"RYUCEMR"}]
                     } 
var jwgame23 = { id : "jwgame23", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Bodies of Water-1', galleryGroup : 'LEVEL - 2',
                      headerMsg : "Identify the correct word using the jumbled letters",
                      topTrayLabel : "Bodies of water",
             wordsArray : [
             { position : "1", correctWord :"OCEAN",jumbledWord :"EANCO"},
             { position : "2", correctWord :"CHANNEL",jumbledWord :"NACHLEN"},
             { position : "3", correctWord :"RIVER",jumbledWord :"VERIR"},
             { position : "4", correctWord :"SEA",jumbledWord :"EAS"},
             { position : "5", correctWord :"POND",jumbledWord :"NODP"}]
             }                                                                                                                                                                       

var jwgame24 = { id : "jwgame24", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Unit of Time-1', galleryGroup : 'LEVEL - 2',
                      headerMsg : "Identify the correct word using the jumbled letters",
                      topTrayLabel : "Unit of Time",
             wordsArray : [
             { position : "1", correctWord :"DECADE",jumbledWord :"ACEDED"},
             { position : "2", correctWord :"HOUR",jumbledWord :"ROUH"},
             { position : "3", correctWord :"CENTURY",jumbledWord :"UENCTYR"},
             { position : "4", correctWord :"MONTH",jumbledWord :"TNOHM"},
             { position : "5", correctWord :"MILLENNIUM",jumbledWord :"MMNNIILLEU"}]
                    }                                                                                                                                                                       

var jwgame25 = { id : "jwgame25", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Unit of Time-2', galleryGroup : 'LEVEL - 2',
                      headerMsg : "Identify the correct word using the jumbled letters",
                      topTrayLabel : "Unit of Time",
             wordsArray : [
             { position : "1", correctWord :"WEEK",jumbledWord :"EKWE"},
             { position : "2", correctWord :"DAY",jumbledWord :"ADY"},
             { position : "3", correctWord :"YEAR",jumbledWord :"AERY"},
             { position : "4", correctWord :"SECOND",jumbledWord :"OCESDN"},
             { position : "5", correctWord :"MINUTE",jumbledWord :"UINMTE"}]
                    }                                                                                                                                                                       

var jwgame26 = { id : "jwgame26", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Seasons', galleryGroup : 'LEVEL - 2',
                      headerMsg : "Identify the correct word using the jumbled letters",
                      topTrayLabel : "Seasons",
             wordsArray : [
             { position : "1", correctWord :"SPRING",jumbledWord :"IRPNGS"},
             { position : "2", correctWord :"SUMMER",jumbledWord :"MMREUS"},
             { position : "3", correctWord :"AUTUMN",jumbledWord :"TAUUNM"},
             { position : "4", correctWord :"MONSOON",jumbledWord :"MNNSOOO"},
             { position : "5", correctWord :"WINTER",jumbledWord :"TINERW"}]
                    }                                                                                                                                                                       

var jwgame27 = { id : "jwgame27", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Meals-Course', galleryGroup : 'LEVEL - 2',
                      headerMsg : "Identify the correct word using the jumbled letters",
                      topTrayLabel : "Food Timings",
             wordsArray : [
             { position : "1", correctWord :"DINNER",jumbledWord :"NINERD"},
             { position : "2", correctWord :"BREAKFAST",jumbledWord :"EARBFKSAT"},
             { position : "3", correctWord :"LUNCH",jumbledWord :"UCNLH"},
             { position : "4", correctWord :"SNACKS",jumbledWord :"CKANSS"},
             { position : "5", correctWord :"SUPPER",jumbledWord :"REUSPP"}]
                    }                                                                                                                                                                       

var jwgame28 = { id : "jwgame28", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Human Organs-1', galleryGroup : 'LEVEL - 1',
                      headerMsg : "Identify the correct word using the jumbled letters",
                      topTrayLabel : "Sense Organs",
             wordsArray : [
             { position : "1", correctWord :"EYES",jumbledWord :"SYEE"},
             { position : "2", correctWord :"NOSE",jumbledWord :"ENSO"},
             { position : "3", correctWord :"TONGUE",jumbledWord :"UGNTEO"},
             { position : "4", correctWord :"EARS",jumbledWord :"SARE"},
             { position : "5", correctWord :"SKIN",jumbledWord :"NIKS"}]
                    } 
var jwgame29 = { id : "jwgame29", level :"2",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Human Organs-2', galleryGroup : 'LEVEL - 2',
                      headerMsg : "Identify the correct word using the jumbled letters",
                      topTrayLabel : "Human Organs",
             wordsArray : [
             { position : "1", correctWord :"STOMACH",jumbledWord :"AMOTSHC"},
             { position : "2", correctWord :"LIVER",jumbledWord :"IERLV"},
             { position : "3", correctWord :"HEART",jumbledWord :"RAETH"},
             { position : "4", correctWord :"KIDNEYS",jumbledWord :"ENDSIYK"},
             { position : "5", correctWord :"LUNGS",jumbledWord :"NULGS"}]
                    } 
var jwgame30 = { id : "jwgame30", level :"3",gametype : 'VSCUBE.JUMBLE-WORDS-GAME', galleryGameLabel : 'Human Organs-3', galleryGroup : 'LEVEL - 3',
                      headerMsg : "Identify the correct word using the jumbled letters",
                      topTrayLabel : "Human Organs",
             wordsArray : [
             { position : "1", correctWord :"BRAIN",jumbledWord :"ARINB"},
             { position : "2", correctWord :"SPINE",jumbledWord :"INEPS"},
             { position : "3", correctWord :"HEART",jumbledWord :"AERHT"},
             { position : "4", correctWord :"APPENDIX",jumbledWord :"XIDNEPAP"},
             { position : "5", correctWord :"PANCREAS",jumbledWord :"SAERCNAP"}]
                    } 
