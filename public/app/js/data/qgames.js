var gkqgame1 = { id : "gkqgame1",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 1', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. How many continents in this world ?",
                    "2. How many oceans in this world ?", 
                    "3. How many countries in this world ?",
                    "4. How many planets in this universe ?", 
                    "5. How many colors in rainbow ?" 
              ],
              questionOptionsArray : [
                     ["1","12","7","5"],
                     ["5","4","7","2"],
                     ["100","202","194","196"],
                     ["5","10","9","7"],
                     ["7","2","6","8"]
              ],
              answerMappingArray : [2,0,3,2,0]
                     }
var gkqgame2 = { id : "gkqgame2",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 2', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                      "1. What is the largest continent by Area ?",
                      "2. What is the smallest continent by Area ?",
                      "3. What is the largest planet in the universe ?",
                      "4. What is the smallest planet in the universe ?",
                      "5. What is the biggest ocean on the Earth ?"
              ],
              questionOptionsArray : [
                     ["Africa","Asia","North America","Australia"],
                     ["Australia","Asia","Africa","South America"],
                     ["Uranus","Pluto","Earth","Jupiter"],
                     ["Mercury","Neptune","Mars","Earth"],
                     ["Arctic Ocean","Atlantic Ocean","Indian Ocean","Pacific Ocean"]
              ],
              answerMappingArray : [1,0,3,0,3]
                     }
var gkqgame3 = { id : "gkqgame3",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 3', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. What is the smallest ocean on the Earth ?",
                    "2. What is the largest country in the world by land area ?",
                    "3. What is the smallest country in the world by land area ?",
                    "4. Which country has more population in the world ?",
                    "5. Which country has least population in the world ?"
              ],
              questionOptionsArray : [
                     ["Arctic Ocean","Atlantic Ocean","Indian Ocean","Pacific Ocean"],
                     ["India","Russia","America","China"],
                     ["SriLanka","Denmark","Vatican City","Japan"],
                     ["China","India","France","Pakistan"],
                     ["Singapore","Taiwan","Swedan","Vatican City"]
              ],
              answerMappingArray : [0,1,2,0,3]
                     }
var gkqgame4 = { id : "gkqgame4",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 4', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. What is the largest waterfall in the world ?",
                    "2. What is the longest river in the world ?",
                    "3. What is the smallest river in the world ?",
                    "4. What is the highest mountain peak in the world ?",
                    "5. What is the lowest mountain peak in the world ?"
              ],
              questionOptionsArray : [
                     ["Nayagara Falls","Victoria Falls","Angel Falls","Jog Falls"],
                     ["Amazon River","Ganges River","Nile river","Congo River"],
                     ["Roe River","Ganges River","Mississippi River","Volga River"],
                     ["Mount Kinabalu","Mount Kilimanjaro","Mount Logan","Mount Everest"],
                     ["Mount Kilimanjaro","Mount Wycheproof","Mount Makalu","Mount Everest"]
              ],
              answerMappingArray : [1,2,0,3,1]
                     }
var gkqgame5 = { id : "gkqgame5",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 5', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                      "1. What is the largest island by land area ?",
                      "2. What is the largest Sea in the world ?",
                      "3. What is the largest Desert in the world ?",
                      "4. Which continent has more countries in the world ?",
                      "5. Which continent has less countries in the world ?"
              ],
              questionOptionsArray : [
                     ["Bali","SriLanka","Ireland","GreenLand"],
                     ["Red Sea","Mediterranean Sea","Arabian Sea","Black Sea"],
                     ["Sahara Desert","Black Rock Desert","Thar Desert","Namib Desert"],
                     ["North America","Asia","South America","Africa"],
                     ["Asia","Africa","Antarctica","North America"]
              ],
              answerMappingArray : [3,1,0,3,2]
                     }
var gkqgame6 = { id : "gkqgame6",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 6', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                      "1. What is the tallest Animal ?",
                      "2. What is the smallest Bird ?",
                      "3. What ist the Heaviest Land Animal in the World ?",
                      "4. What is the Largest Animal Ever ?",
                      "5. Which is the largest bird in the world?"
              ],
              questionOptionsArray : [
                     ["Giraffe","Lion","Tiger","Camel"],
                     ["Crow","king Fisher","Humming Bird","Parrot"],
                     ["Lion","Elephant","Pig","Rhinoceros"],
                     ["Lion","Rhinoceros","Elephant","Blue Whale"],
                     ["Goose","Ostrich","Sparrow","Kiwi"]
              ],
              answerMappingArray : [0,2,1,3,1]
                     }
var gkqgame7 = { id : "gkqgame7",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 7', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                      "1. Who is the first person designed Printing Press ?",
                      "2. Who invented Radio ?",
                      "3. Who invented Telephone ?",
                      "4. Who invented Telescope ?",
                      "5. Who invented first mechanical computer ?",
              ],
              questionOptionsArray : [
                     ["Galileo","Thomas Edison","Newton","Gutenberg"],
                     ["Alexander Fleming","Marconi","Graham Bell","Marie Curie"],
                     ["Newton","Thomas Edison","Einstein","Graham Bell"],
                     ["Galileo","Marie Curie","C.V.Raman","Gutenberg"],
                     ["Pandu","Shakuni","Charles Babbage","Shantanu"]
              ],
              answerMappingArray : [3,1,3,0,2]
                     }
var gkqgame8 = { id : "gkqgame8",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 8', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                      "1. Who invented light bulb ?",
                      "2. What is the hottest continent on Earth? ",
                      "3. Which of below animal does not sleep ? ",
                      "4. Who discovered Penicillin ?   ",
                      "5. Who discovered Electricity ?  "
              ],
              questionOptionsArray : [
                     ["Einstein","Newton","Thomas Edison","Galileo"],
                     ["Africa","Asia","North America","Australia"],
                     ["Camel","Ant","Lion","Rat"],
                     ["Graham Bell","Alexander Fleming","Darwin","Marconi"],
                     ["Thomas Edison","Galileo","Gutenberg","Michael Faraday"]
              ],
              answerMappingArray : [2,0,1,1,3]
                     }

var gkqgame9 = { id : "gkqgame9",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 9', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. How many number of players need to play Baseball ?",
                    "2. How many number of players need to play Basketball ?", 
                    "3. How many number of players need to play Cricket ? ",
                    "4. How many number of players need to play Rugby Football ? ",
                    "5. How many number of players need to play Polo ? "
              ],
              questionOptionsArray : [
                     ["10","11","6","9"],
                     ["5","7","9","11"],
                     ["12","10","11","9"],
                     ["15","16","14","11"],
                     ["3","5","6","4"]
              ],
              answerMappingArray : [3,0,2,0,3]
                     }
var gkqgame10 = { id : "gkqgame10",gametype : 'VSCUBE.MCQ-GAME', galleryGameLabel : 'GK Quiz - 10', galleryGroup : 'GENERAL KNOWLEDGE',
                       headerMsg : "Please click on the correct answer for each question.",
              questionsArray: [
                    "1. How many wonders in this world ?", 
                    "2. How many bones in human body ? ",
                    "3. How many world wars have happened ? ",
                    "4. How many states in India ? ",
                    "5. How many colors in Indian Flag ? "
              ],
              questionOptionsArray : [
                     ["14","7","5","8"],
                     ["205","103","206","456"],
                     ["2","0","1","3"],
                     ["25","21","30","29"],
                     ["2","3","7","5"]
              ],
              answerMappingArray : [1,2,0,3,1]
              }                                                                                                                                                                                                                                                            