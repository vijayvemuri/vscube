function openGame(gameConfig, url) {
  window.localStorage.setItem("vscubeglobals.current_game", JSON.stringify(gameConfig));
  window.location= url;
  }
  function openGameSeries(gameSeriesConfig) {
    window.localStorage.setItem("vscubeglobals.current_game_series", JSON.stringify(gameSeriesConfig));
    window.localStorage.setItem("vscubeglobals.current_game_gallery", JSON.stringify(gameSeriesConfig));
    window.location= gameSeriesConfig.url;
  }

  function openGameGallery(gameGalleryConfig) {
    console.log(gameGalleryConfig);
    window.localStorage.setItem("vscubeglobals.current_game_gallery", JSON.stringify(gameGalleryConfig));
    window.location= gameGalleryConfig.url;
  }

var chooseall_games_gallery_url = '../games/chooseallgames_g.html';
var alphaorder_games_gallery_url = '../games/alphaordergames_g.html';
var jwgames_games_gallery_url = '../games/jwgames_g.html';
var missingletter_games_gallery_url = '../games/missinglettergames_g.html';
var oddoneout_games_gallery_url = '../games/oddoneoutgames_g.html';
var spelling_games_gallery_url = '../games/spellinggames_g.html';
var quiz_games_gallery_url = '../games/mcqquizagames_g.html';
var quiz_games_from_you_gallery_url = '../games/mcqquizgamesfromyou_g.html';
var matching_games_gallery_url = '../games/matchingames_g.html';
var img_matching_games_gallery_url = '../games/imgmatchinggames_g.html';
var mix_matching_games_gallery_url = '../games/mixmatchinggames_g.html';
var word_finder_games_gallery_url = '../games/wordfindergames_g.html';
var word_chain_games_gallery_url = '../games/wordchaingames_g.html';
var jiggames_games_gallery_url ='../games/jigsawgames_g.html';