var cagame1 = {
  "id" : "cagame1",gametype : 'VSCUBE.CA-QUIZ-GAME', galleryGameLabel : 'Choose All Quiz - 1', galleryGroup : 'GENERAL KNOWLEDGE',
  "questionsArray" : [ "Identify the Planets from the below list" ],
  "questionOptionsArray" : [ "Pluto", "Moon", "Venus", "Earth", "Astorid", "Sea", "Milkyway", "neptune", "Asia", "Mars" ],
  "answerMappingArray" : [ 0, 2, 3, 7, 9 ]
}
var cagame2 = {
  "id" : "cagame2",gametype : 'VSCUBE.CA-QUIZ-GAME', galleryGameLabel : 'Choose All Quiz - 2', galleryGroup : 'GENERAL KNOWLEDGE',                      
  "questionsArray" : [ "Identify the sense organs from the below list ?" ],
  "questionOptionsArray" : [ "nose", "ear", "eye", "liver", "kidney", "legs", "skin", "hand", "fingers", "knee", "elbow", "foot", "palm", "teeth", "tongue" ],
  "answerMappingArray" : [ 0, 1, 2, 6, 14 ]
}