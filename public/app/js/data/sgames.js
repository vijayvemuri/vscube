var sgame1 = { id : "sgame1",level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'Mixed - 1', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
	              ["Weding","Wedding","Weddingg"],["Tomorow","Tomorrow","Toomorrow"],
	              ["Tiger","Tigre","Tigar"],["Flowar","Flower","Flawar"],
	              ["Mermeid","Mermaid","Marmaid"]
              ],
              answerMappingArray : [1,1,0,1,1]
			}
var sgame2 = { id : "sgame2",level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'Mixed - 2', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				  ["Dactor","Docter","Doctor"],
	              ["Kangaroo","Kangaru","Kongaroo"],["Computar","Computer","Camputer"],
	              ["Hospital","Haspital","Hospitol"],["Pilat","Pilot","Pelot"]
              ],
              answerMappingArray : [2,0,1,0,1]
			}

var sgame3 = { id : "sgame3",level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 11', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				  ["rember","remeber","remember"],
	              ["suprize","surprise","suprise"],["tongue","tounge","tonge"],
	              ["wherevar","whereever","wherever"],["separate","separete","seperate"]
              ],
              answerMappingArray : [2,1,0,2,0]
			}			

var sgame4 = { id : "sgame4",level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 12', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
	              ["belive","beleive","believe"],["finalli","finally","finaly"],
	              ["foreign","foriegn","forien"],["humaurous","humorous","humourous"],
	              ["lollipop","lollypop","lolypop"]
              ],
              answerMappingArray : [2,1,0,1,0]
			}

var sgame5 = { id : "sgame5",level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 13', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
	              ["sense","sence","sanse"],["succesful","succassful","successful"],
	              ["piese","piece","peice"],["occurance","occurence","occurrence"],
	              ["occasion","ocassion","occassion"]
              ],
              answerMappingArray : [0,2,1,2,0]
			}
var sgame6 = { id : "sgame6",level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 14', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				  ["neccessary","necessary","necessery"],
	              ["knowlege","nowledge","knowledge"],["immediately","immediatly","imediately"],
	              ["goverment","government","govarnment"],["friend","freind","frend"]
              ],
              answerMappingArray : [1,2,0,1,0]
			}			
var sgame7 = { id : "sgame7", level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'Animals - 1', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
	              ["Camel","Camal","Kamel"],["Katerpillar","Caterpilar","Caterpillar"],
	              ["Dolpin","Dolphin","Dollphin"],["Grasshoper","Grosshopper","Grasshopper"],
	              ["Hummingbird","Humingbird","Huminggbird"]
              ],
              answerMappingArray : [0,2,1,2,0]
			}						
var sgame8 = { id : "sgame8", level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'Animals - 2', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Hina","Hyina","Hyena"],
	              ["Jagwar","Jaguar","Jaguer"],["Leopared","Lepard","Leopard"],
	              ["Mongoose","Mongose","Mangoose"],["Ostrick","Ostrich","Ostritch"]
              ],
              answerMappingArray : [2,1,2,0,1]
			}

var sgame9 = { id : "sgame9", level:"3",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 1', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Accelarate","Accelerate","Acelerate"],
	              ["Acolade","Accolde","Accolade"],["Accomplish","Accomplesh","Acomplish"],
	              ["Acheive","Achieve","Acheeve"],["Admier","Admyre","Admire"]
              ],
              answerMappingArray : [1,2,0,1,2]
			}
var sgame10 = { id : "sgame10", level:"3",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 2', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Afirm","Affarm","Affirm"],
	              ["Agenda","Ajenda","Aganda"],["Ameze","Amaze","Amase"],
	              ["Announce","Anounce","Announse"],["Apreciete","Appreciate","Appresiate"]
              ],
              answerMappingArray : [2,0,1,0,1]
			}
var sgame11 = { id : "sgame11", level:"3",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 3', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Dailogue","Dailoge","Dailague"],
	              ["Devalop","Develap","Develop"],["Discuss","Discus","Descuss"],
	              ["Deligent","Diligent","Dilegent"],["Diplamatic","Diplomatic","Diplometic"]
              ],
              answerMappingArray : [0,2,0,1,1]
			}
var sgame12 = { id : "sgame12", level:"3",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 4', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Iniciative","Initietive","Initiative"],
	              ["Inovation","Innovation","Inovation"],["Intension","Intention","Intentien"],
	              ["Incentive","Insentive","Insenteve"],["Independant","Independent","Indipendent"]
              ],
              answerMappingArray : [2,1,1,0,1]
			}

var sgame13 = { id : "sgame13", level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 5', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Mission","Mision","Mession"],
	              ["Modarn","Modern","Modernn"],["Momentum","Momentam","Momemtum"],
	              ["Motivete","Motevate","Motivate"],["Magnificient","Magnificent","Magnificant"]
              ],
              answerMappingArray : [0,1,0,2,1]
			}

var sgame14 = { id : "sgame14", level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 6', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Centiment","Sentiment","Sentement"],
	              ["Sensitive","Sensetive","Censitive"],["Significent","Significant","Signeficant"],
	              ["Suronding","Surronding","Surrounding"],["Suitable","Sutable","Suiteble"]
              ],
              answerMappingArray : [1,0,1,2,0]
			}
var sgame15 = { id : "sgame15", level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 7', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Recognize","Recognise","Recugnize"],
	              ["Recomend","Recommend","Recommand"],["Rationel","Ratienal","Rational"],
	              ["Relevant","Relevent","Relavant"],["Reputasion","Reputation","Reputetion"]
              ],
              answerMappingArray : [0,1,2,0,1]
			}

var sgame16 = { id : "sgame16", level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 8', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Versatile","Versetile","Varsatile"],
	              ["Vigilent","Vigelant","Vigilant"],["Vibrent","Vibrant","Vibrrant"],
	              ["Vivacious","Vivacios","Vevacious"],["Victery","Viktory","Victory"]
              ],
              answerMappingArray : [0,2,1,0,2]
			}

var sgame17 = { id : "sgame17", level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 9', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Understand","Undarstand","Underrstand"],
	              ["Universel","Univarsal","Universal"],["Unanimos","Unanimous","Unanimoss"],
	              ["Urgent","Urgant","Urrgent"],["Ultimete","Ultimate","Ultemate"]
              ],
              answerMappingArray : [0,2,1,0,1]
			}

var sgame18 = { id : "sgame18", level:"2",gametype : 'VSCUBE.SPELLING-GAME', galleryGameLabel : 'List - 10', galleryGroup : '',
			  headerMsg : "Try to identify the correct spelling.",
              questionOptionsArray : [
				["Obsarve","Observe","Obserrve"],
	              ["Opportunity","Oportunity","Opportunety"],["Originel","Orijinal","Original"],
	              ["Objactive","Objective","Objecteve"],["Optemism","Optimesm","Optimism"]
              ],
              answerMappingArray : [1,0,2,1,2]
			}																											