var game1 = { id : "game1", gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Animals - Sounds-1', galleryGroup : 'MATCHING GAMES	',
			  headerMsg : "Try matching the correct Animal and the sound that it produces.. Drag from Bottom Tray",
			  topTrayLabel : "Animals",
              topTrayOptionsArray : ["Tiger","Lion","Elephant","Horse","Cow","Sheep","Monkey","Dog","Cat","Duck"],
			  bottomTrayLabel : "Sounds",
              bottomTrayOptionsArray : ["Meow","Moo","Growl","Neigh","Bark","Chatter","Roar","Quack","Trumpet","Baa"],
              answerMappingArray : [2,6,8,3,1,9,5,4,0,7]
			}
var game2 = { id : "game2",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Animals-Birds-Babies-1', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the correct Animal and the baby animal..Drag from Bottom Tray",
			  topTrayLabel : "Animals",
              topTrayOptionsArray : ["Hen","Cat","Dog","Frog","Donkey","Buffalo","Kangaroo","Rabbit","Deer","Sheep"],
			  bottomTrayLabel : "Babies",
              bottomTrayOptionsArray : ["calf","joey","fawn","foal","kit","kitten","lamb","chick","puppy","tadpole"],
              answerMappingArray : [7,5,8,9,3,0,1,4,2,6]
			}			
var game3 = { id : "game3",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Animals-Birds-Babies-2', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the correct Animal and the baby animal..Drag from Bottom Tray",
			  topTrayLabel : "Animals",
              topTrayOptionsArray : ["Fish","Giraffe","Tiger","Spider","Horse","Pig","Monkey","Owl","Butterfly","Duck"],
			  bottomTrayLabel : "Babies",
              bottomTrayOptionsArray : ["duckling","owlet","colt","piglet","cub","fry","spiderling","caterpillar","calf","infant"],
              answerMappingArray : [5,8,4,6,2,3,9,1,7,0]
			}
var game4 = { id : "game4",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Countries-Captials-1', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the correct Captital and the Country..Drag from Bottom Tray",
			  topTrayLabel : "Countries",
              topTrayOptionsArray : ["India","Afghanistan","SriLnaka","Bangladesh","Pakistan","Nepal","China","Malaysia","Singapore","Japan"],
			  bottomTrayLabel : "Capital Cities",
              bottomTrayOptionsArray : ["Kathmandu","Tokyo","Islamabad","New Delhi","Singapore","Kuala Lumpur","Dhaka","Colombo","Beijing","Kabul"],
              answerMappingArray : [3,9,7,6,2,0,8,5,4,1]
			}
var game5 = { id : "game5",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Countries-Captials-2', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the correct Captital and the Country..Drag from Bottom Tray",
			  topTrayLabel : "Countries",
              topTrayOptionsArray : ["United States","United Kingdom","France","Germany","Canada","Australia","Italy","South Africa","Switzerland","Russia"],
			  bottomTrayLabel : "Capital Cities",
              bottomTrayOptionsArray : ["Berlin","Canberra","Rome","Pretoria","Moscow","Bern","Washington","Paris","London","Ottawa"],
              answerMappingArray : [6,8,7,0,9,1,2,3,5,4]
			}
var game6 = { id : "game6",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'IndianRivers-Birthplaces', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the correct Indian River and the birth place",
			  topTrayLabel : "Rivers",
              topTrayOptionsArray : ["Ganges","Godavari","Kaveri","Krishna","Sutlej","Brahmaputra","Mahanadi","Indus"],
			  bottomTrayLabel : "Birth place",
              bottomTrayOptionsArray : ["Tibet-Manasarovar","Southwestern Tibet","Southern Himalayas","Talakaveri","Mahabaleswar","Trimbak","Punjab","Chhattisgarh"],
              answerMappingArray : [2,5,3,4,6,1,7,0]
			}
var game7 = { id : "game7",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Indian-Lankmarks-cities', galleryGroup : 'MATCHING GAMES',
			  containerType :"big",
			  headerMsg : "Try matching the land mark and the indian city where it is located",
			  topTrayLabel : "Cities",
              topTrayOptionsArray : ["Agra","Delhi","Hyderabad","Jaipur","Amritsar","Mumbai","Bengaluru","Aurangabad","Kolkata","konark"],
			  bottomTrayLabel : "Land marks",
              bottomTrayOptionsArray : ["Hawa Mahal","Golden Temple","Victoria Memorial","Charminar","Taj Mahal","Red Fort","Gateway of India","Sun Temple","Lal Bagh","Ajanta & Ellora Caves"],
              answerMappingArray : [4,5,3,0,1,6,8,9,2,7]
			}
var game8 = { id : "game8",
			  headerMsg : "Try matching the word and the antonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["young","wise","rich","happy","hot","wet","old"],
			  bottomTrayLabel : "Antonyms",
              bottomTrayOptionsArray : ["poor","sad","cold","new","old","foolish","dry"],
              answerMappingArray : [4,5,0,1,2,6,3]
			}	
var game9 = { id : "game9",
			  headerMsg : "Try matching the word and the antonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["failure","first","deep","strong","dirty","near","false"],
			  bottomTrayLabel : "Antonyms",
              bottomTrayOptionsArray : ["true","week","success","shallow","far","clean","last"],
              answerMappingArray : [2,6,3,1,5,4,0]
			}	
var game10 = { id : "game10",
			  headerMsg : "Try matching the word and the antonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["yes","valley","parent","peace","late","light","long"],
			  bottomTrayLabel : "Antonyms",
              bottomTrayOptionsArray : ["child","war","no","short","dark","hill","early"],
              answerMappingArray : [2,5,0,1,6,4,3]
			}
var game11 = { id : "game11",
			  headerMsg : "Try matching the word and the antonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["bad","beautiful","before","disease","kind","victory","question"],
			  bottomTrayLabel : "Antonyms",
              bottomTrayOptionsArray : ["after","health","cruel","defeat","good","answer","ugly"],
              answerMappingArray : [4,6,0,1,2,3,5]
			}	
var game12 = { id : "game12",
			  headerMsg : "Try matching the word and the antonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["cheap","fast","come","remember","right","off","open"],
			  bottomTrayLabel : "Antonyms",
              bottomTrayOptionsArray : ["go","forget","wrong","on","close","expensive","slow"],
              answerMappingArray : [5,6,0,1,2,3,4]
			}	
var game13 = { id : "game13",
			  headerMsg : "Try matching the word and the Synonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["clever","new","true","unhappy","rich","old","beautiful"],
			  bottomTrayLabel : "Synonyms",
              bottomTrayOptionsArray : ["correct","ancient","wise","pretty","sad","wealthy","fresh"],
              answerMappingArray : [2,6,0,4,5,1,3]
			}																										
var game14 = { id : "game14",
			  headerMsg : "Try matching the word and the Synonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["lazy","answer","begin","close","complete","garbage","hole"],
			  bottomTrayLabel : "Synonyms",
              bottomTrayOptionsArray : ["start","total","reply","idle","rubbish","gap","shut"],
              answerMappingArray : [3,2,0,6,1,4,5]
			}	
var game15 = { id : "game15",
			  headerMsg : "Try matching the word and the Synonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["joy","last","hurry","silly","sunrise","silent","sunset"],
			  bottomTrayLabel : "Synonyms",
              bottomTrayOptionsArray : ["foolish","quiet","dawn","delight","rush","dusk","final"],
              answerMappingArray : [3,6,4,0,2,1,5]
			}	
var game16 = { id : "game16",
			  headerMsg : "Try matching the word and the Synonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["buy","yummy","warn","worry","courage","bounce","arrive"],
			  bottomTrayLabel : "Synonyms",
              bottomTrayOptionsArray : ["bravery","come","purchase","tasty","jump","alert","anxious"],
              answerMappingArray : [2,3,5,6,0,4,1]
			}																													

var game17 = { id : "game17",
			  headerMsg : "Try matching the word and the Synonym",
			  topTrayLabel : "Words",
              topTrayOptionsArray : ["quick","big","ask","end","bad","enjoy","fall"],
			  bottomTrayLabel : "Synonyms",
              bottomTrayOptionsArray : ["finish","delight","evil","large","drop","question","speed"],
              answerMappingArray : [6,3,5,0,2,1,4]
			}																													
var game18 = { id : "game18",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Indian DanceForms - States', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the Indian Dance form and the State it belongs to",
			  topTrayLabel : "Dance Forms",
              topTrayOptionsArray : ["Kathak","Odissi","Manipuri","Bharatnatyam","Kathakali","Kuchipudi","Bhangra","Saang","Dandiya","Kalbelia"],
			  bottomTrayLabel : "States",
              bottomTrayOptionsArray : ["Gujarat","Punjab","Tamil Nadu","Manipur","Rajasthan","Andhra Pradesh","Haryana","Odisha","Kerala","Uttar Pradesh"],
              answerMappingArray : [9,7,3,2,8,5,1,6,0,4]
			}
var game19 = { id : "game19",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Anniversary-Jubilee', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the Anniversary and the name it is referred to",
			  topTrayLabel : "Anniversary",
              topTrayOptionsArray : ["25th anniversary","40th anniversary","50th anniversary","60th anniversary","65th anniversary","70th anniversary"],
			  bottomTrayLabel : "Jubilee",
              bottomTrayOptionsArray : ["Ruby jubilee","Platinum jubilee","Diamond jubilee","Silver jubilee","Golden jubilee","Sapphire jubilee"],
              answerMappingArray : [3,0,4,2,5,1]
			}

var game20 = { id : "game20",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Units of Time', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the Anniversary and the name it is referred to",
			  topTrayLabel : "Unit of Time",
              topTrayOptionsArray : ["Minute","Hour","Day","Week","Fortnight","Month","Year","Olympiad","Lustrum","Decade","Century","Millennium"],
			  bottomTrayLabel : "Name",
              bottomTrayOptionsArray : ["4 Years","28/29/30/31 Days","5 Years","60 Seconds","10 Years","1000 Years","7 Days","100 Years","24 Hours","365/366 Days","60 Minutes","15 Days"],
              answerMappingArray : [3,10,8,6,11,1,9,0,2,4,7,5]
			}
var game21 = { id : "game21",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Sense Organs', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the Sense organ and activity it does",
			  topTrayLabel : "Sense Organ",
              topTrayOptionsArray : ["TONGUE","SKIN","EARS","NOSE","EYES"],
			  bottomTrayLabel : "Activity",
              bottomTrayOptionsArray : ["SIGHT","TOUCH","SMELL","TASTE","HEARING"],
              answerMappingArray : [3,1,4,2,0]
			}										
var game22 = { id : "game22",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Indian-State-Captials-1', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the Indian State and the Capital",
			  topTrayLabel : "Indian State",
              topTrayOptionsArray : ["Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chhattisgarh","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir"
],
			  bottomTrayLabel : "Capital",
              bottomTrayOptionsArray : ["Dispur","Patna","Panaji","Chandigarh","Amaravati","Shimla","Itanagar","Srinagar","Gandhinagar","Raipur"],
              answerMappingArray : [4,6,0,1,9,2,8,3,5,7]
			}										
var game23 = { id : "game23",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Indian-State-Captials-2', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the Indian State and the Capital",
			  topTrayLabel : "Indian State",
              topTrayOptionsArray : ["Jharkhand","Karnataka","Kerala","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland"],
			  bottomTrayLabel : "Capital",
              bottomTrayOptionsArray : ["Shillong","Imphal","Kohima","Aizawl","Ranchi","Mumbai","Thiruvananthapuram","Bhopal","Bengaluru"],
              answerMappingArray : [4,8,6,7,5,1,0,3,2]
			}										
var game24 = { id : "game24",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Indian-State-Captials-3', galleryGroup : 'MATCHING GAMES',
			  headerMsg : "Try matching the Indian State and the Capital",
			  topTrayLabel : "Indian State",
              topTrayOptionsArray : ["Odisha","Punjab","Rajasthan","Sikkim","Tamil Nadu","Telangana","Tripura","Uttar Pradesh","Uttarakhand","West Bengal"],
			  bottomTrayLabel : "Capital",
              bottomTrayOptionsArray : ["Agartala","Lucknow","Dehradun","Bhubaneswar","Hyderabad","Jaipur","Kolkata","Gangtok","Chandigarh","Chennai"],
              answerMappingArray : [3,8,5,7,9,4,0,1,2,6]
			}										
var game25 = { id : "game25",
			  headerMsg : "Try matching the Sense organ and activity it does",
			  topTrayLabel : "Sense Organ",
              topTrayOptionsArray : ["TONGUE","SKIN","EARS","NOSE","EYES"],
			  bottomTrayLabel : "Activity",
              bottomTrayOptionsArray : ["SIGHT","TOUCH","SMELL","TASTE","HEARING"],
              answerMappingArray : [3,1,4,2,0]
			}										
var game26 = { "id" : "game26",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Animals - Sounds-2', galleryGroup : 'MATCHING GAMES',
			  "headerMsg" : "Try matching the correct Animal and the sound that it produces.. Drag from Bottom Tray",
			  "topTrayLabel" : "Animals",
			  "topTrayOptionsArray" : [ "Bees", "Chimpanzee", "Crow", "Deer", "Dolphins", "Donkey", "Frog" ],
			  "bottomTrayLabel" : "Sounds",
			  "bottomTrayOptionsArray" : [ "Bellow", "Scream", "Buzz", "Croak", "Caw", "Click", "Bray" ],
			  "answerMappingArray" : [ 2, 1, 4, 0, 5, 6, 3 ]
}

var game27 = { "id" : "game27",gametype : 'VSCUBE.MATCHING-GAME', galleryGameLabel : 'Animals - Sounds-3', galleryGroup : 'MATCHING GAMES',
			  "headerMsg" : "Try matching the correct Animal and the sound that it produces.. Drag from Bottom Tray",
			  "topTrayLabel" : "Animals",
			  "topTrayOptionsArray" : [ "Giraffee", "Parrot", "Pigeons", "Rabbit", "Snake", "Turkey", "Owl" ],
			  "bottomTrayLabel" : "Sounds",
			  "bottomTrayOptionsArray" : [ "Squawk", "Gobble", "Hoot", "Hiss", "Bleat", "Squeak", "Coo" ],
			  "answerMappingArray" : [ 4, 0, 6, 5, 3, 1, 2 ]
			}