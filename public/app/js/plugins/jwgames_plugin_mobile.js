(function ( $ ) {
 
    function shuffle(arr) {
        for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);
        return arr;
    }

    $.fn.buildJumbleWordsGrid = function( options, fromSeriesGame ) {
 
        console.log("start of plug in execution");
        console.log("input options " + options);

        var answerCountArray = [0,0,0,0,0];
        var correctCount = 0;
        var myAudio = document.createElement('audio');
        myAudio.controls = true;
        myAudio.src = '../sounds/drop.wav';

        var $congratsDiv =$("<div id='congratsDiv' class='congratsDiv'></div>");    
        var $gameContainerDiv = $("<div id='gamePanelDiv' />");
        this.append($gameContainerDiv);

        $gameContainerDiv.append($congratsDiv);
        var $headerMsgP = $("<span style='margin-left:20px;font-size:20px;' class='text-success'></span>");
        $headerMsgP.html("<strong>"+options.topTrayLabel+"</strong>" + " : " + options.headerMsg);


        var $scoreIdP = $("<span class='jwscoreSpanClass'><b> Score </b></span> <span id='scoredid' style='font-size:25px; border-radius:20px;' class='highlight primary'>0</span>");

        var $outerPanelDiv = $('<div class="card" style="margin-left:5px;margin-right:5px"</div>');
        var $outerPanelHeading = $('<div class="card-header"></div>');
        var $outerPanelContent = $('<div class="card-body"></div>');


        //$outerPanelHeading.append($headerMsgP);
        $outerPanelHeading.append($scoreIdP);

        var $backButtonLink = $('<span style="float:right;"><a href="../html/gamegallery.html" class="btn btn-warning btn-medium btn-rounded">Back to Jumble Words Games</a></span>');
        if(!fromSeriesGame){
            $outerPanelHeading.append($backButtonLink);
        }
        $outerPanelDiv.append($outerPanelHeading);
        $outerPanelDiv.append($outerPanelContent);
        
        $gameContainerDiv.append($outerPanelDiv);

        var $topPanelDiv = $('<div class="gameContainerDiv card"></div>');
        var $topPanelContent = $('<div class="row"></div>');
        var $topPanelHeading = $('<h5 class="card-header">'+options.topTrayLabel + ' : ' + options.headerMsg+'</div>');

        var $leftPanelContent = $('<div class="col jwleftPanelContent"></div>');
        var $rightPanelContent = $('<div class="col jwrightPanelContent"></div>');

        $topPanelContent.append($leftPanelContent);
        $topPanelContent.append($rightPanelContent);

        shuffle(options.wordsArray);

        $.each(options.wordsArray, function( index, value ) 
        {
            console.log("index : " + index + " value : " + value);
            var $leftPanelContentRow = $('<div class="row" style="margin-left:20px;clear:both;"></div>');

            var jword = value.jumbledWord+"";
            $leftPanelContentRow.append("<span class = 'jwlabelClass'>"+ (parseInt(index)+1) + "</span>");
            for(var x = 0, c=''; c = jword.charAt(x); x++)
            { 
                console.log(c); 
                $leftPanelContentRow.append($('<div id = "draggable' + index + x +'" class ="jwdraggableClass">'+ c +'</div>'));
            }

        $leftPanelContent.append($leftPanelContentRow);

        });

        $.each(options.wordsArray, function( index, value ) 
        {
            console.log("index : " + index + " value : " + value);
            var $rightPanelContentRow = $('<div class="row" style="margin-left:20px;"></div>');

            $rightPanelContentRow.append("<span class = 'jwlabelClass'>"+ (parseInt(index)+1) + "</span>");

            var cword = value.correctWord+"";
            for(var x = 0, c=''; c = cword.charAt(x); x++)
            { 
                console.log(c); 
                $rightPanelContentRow.append($('<div id = "droppable' + index + x +'" class ="jwdroppableClass"/>'));
            }
            $rightPanelContentRow.append($('<div id = "correctDiv' + index +'" class = "correctDiv"/>'));
            $rightPanelContentRow.append($('<div id = "wrongDiv' + index +'" class = "wrongDiv"/>'));

        $rightPanelContent.append($rightPanelContentRow);

        });


        $topPanelDiv.append($topPanelHeading);  
        $topPanelDiv.append($topPanelContent);
        
        $outerPanelContent.append($topPanelDiv);

        $(".correctDiv").hide();
        $(".wrongDiv").hide();

         
        $( ".jwdraggableClass" ).draggable({
              revert: true,
              placeholder: true,    
              droptarget: '.jwdroppableClass',

            drop: function( event, droptarget ) {
            var result = 'notstarted';
            myAudio.play();

            //$("#"+ui.draggable[0].id).draggable( "option", "revert", true );


            var sourceDivId = droptarget.id;
            //console.log("Droppable Id : " + sourceDivId.substr(sourceDivId.length - 2));    

            var draggableDivId = $(this).attr('id');
            console.log(sourceDivId.charAt(sourceDivId.length-2));
            console.log(draggableDivId.charAt(draggableDivId.length-2));

            if (sourceDivId.charAt(sourceDivId.length-2) !== draggableDivId.charAt(draggableDivId.length-2))
            {
                return;
            }

            var draggableTxt = $(this).text();

            $("#"+'droppable' + sourceDivId.substr(sourceDivId.length - 2)).css('background-color','#2ecc71').text(draggableTxt);
            $(this).hide();

            var wordNumber = sourceDivId.charAt(sourceDivId.length - 2)
            console.log("word Number " + wordNumber);
            
            answerCountArray[wordNumber]++;

            console.log("word Number after increment " + answerCountArray[wordNumber]);

            if (answerCountArray[wordNumber] == options.wordsArray[wordNumber].correctWord.length)
            {
                var answerText = '';
                for (var i = 0; i < options.wordsArray[wordNumber].correctWord.length; i++)
                {
                    answerText  = answerText.concat($("#"+'droppable' + wordNumber + i).text().trim());
                }

                if (options.wordsArray[wordNumber].correctWord === answerText)
                {
                    
                    result = 'correct'+wordNumber;
                }
                else
                {
                    result = 'wrong';
                }
            }

          if (result.indexOf('correct') != -1)
          {
            correctCount++;
            $("#scoredid").html("<span style='font-size:25px; border-radius:20px;' class='highlight primary'>"+  correctCount    +"</span>");
            if  (correctCount == 5) 
            {
              correctCount = 0;  
              $("#congratsDiv").show();
            } 
            $("#"+'correctDiv'+ wordNumber).show();
            //$("#" + ui.draggable[0].id).draggable("disable");
          }
          else if (result === 'wrong')
          {
            $("#"+'wrongDiv'+ wordNumber).show();
          }

      }
      });
        console.log("end of plug in execution");
        return this;
    };
 
}( jQuery ));