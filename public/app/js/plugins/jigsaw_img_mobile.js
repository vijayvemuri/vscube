(function ( $ ) {
 
    $.fn.buidJigSawPuzzleGrid = function( options ) {
 
        console.log("start of plug in execution");
        console.log("input options " + options);

        var myAudio = document.createElement('audio');
        myAudio.controls = true;
        myAudio.src = '../sounds/drop.wav';

        var $congratsDiv =$("<div id='congratsDiv' class='congratsDiv'></div>");    
        var $gameContainerDiv = $("<div/>");
        this.append($gameContainerDiv);

        $gameContainerDiv.append($congratsDiv);
        var $headerMsgP = $("<span class='text-success'></span>");
        $headerMsgP.html(options.headerMsg);


        var $correctMsgDiv = $("<span id = 'correctDiv1' class='text-success' style='display: none;margin-left:20px;'><img style='width:40px;height:40px;' src='../img/correct.png'></img></span>");
        var $wrongMessageDiv = $("<span id = 'wrongDiv1' class='text-danger' style='display: none;margin-left:20px;'><img style='width:40px;height:40px;' src='../img/wrong.png'></img></span>");


        var $scoreIdP = $("<span class='scoreSpanClass'><b> Score </b></span> <span id='scoredid' class='highlight primary'> 0 </span>");

        var $outerPanelDiv = $('<div class="card" style="margin-left:5px;margin-right:5px"</div>');
        var $outerPanelHeading = $('<div class="card-header"></div>');
        var $outerPanelContent = $('<div class="card-body"></div>');


        $outerPanelHeading.append($headerMsgP);
        $outerPanelHeading.append($scoreIdP);

        var $backButtonLink = $('<span style="float:right;"><a href="../html/gamegallery.html" class="btn btn-warning btn-medium btn-rounded">Back to Fun Games</a></span>');

        $outerPanelHeading.append($correctMsgDiv);
        $outerPanelHeading.append($wrongMessageDiv);
        $outerPanelHeading.append($backButtonLink);

        $outerPanelDiv.append($outerPanelHeading);
        $outerPanelDiv.append($outerPanelContent);
        
        $gameContainerDiv.append($outerPanelDiv);

        var $topPanelDiv = $('<div class="card"></div>');
        var $topPanelHeading = $('<h5 class="card-header">'+options.topTrayLabel+'</div>');
        var $topPanelContent = $('<div class="row" style="margin-left:20px;"></div>');

        var $originalImageDiv = $('<div class="jigoriginalimageClass"></div>');
        var $imageBlocksDiv = $('<div class="jigblocksdivClass"></div>');
        var $topPanelContentRow1 = $('<div class="jigimgBlocksRowClass"></div>');
        var $topPanelContentRow2 = $('<div class="jigimgBlocksRowClass"></div>');
        var $topPanelContentRow3 = $('<div class="jigimgBlocksRowClass"></div>');

        $topPanelContent.append($originalImageDiv);
        $topPanelContent.append($imageBlocksDiv);        
        
        var origImgPath = "../img/"+options.id+"/" + options.originalImageName;
        $originalImageDiv.css('background','url('+origImgPath+') no-repeat center center scroll');        
        


        $imageBlocksDiv.append($topPanelContentRow1);
        $imageBlocksDiv.append($topPanelContentRow2);
        $imageBlocksDiv.append($topPanelContentRow3);


        $.each(options.topTrayOptionsArray, function( index, value ) 
        {
            console.log("index : " + index + " value : " + value);
            if (index < 3)
            {
                $topPanelContentRow1.append($('<div id = "droppable' + index + '" class ="jigimgdroppableClass"/>'));
            }
            else if (index < 6)
            {
                $topPanelContentRow2.append($('<div id = "droppable' + index + '" class ="jigimgdroppableClass"/>'));
            }
            else
            {
                $topPanelContentRow3.append($('<div id = "droppable' + index + '" class ="jigimgdroppableClass"/>'));
            }
        });
        $topPanelDiv.append($topPanelHeading);
        $topPanelDiv.append($topPanelContent);
        
        $outerPanelContent.append($topPanelDiv);

        var $bottomPanelDiv = $('<div class="card" style="margin-top: 20px;"></div>');
        var $bottomPanelHeading = $('<h5 class="card-header">'+options.bottomTrayLabel+'</div>');
        var $bottomPanelContent = $('<div class="row" style="margin-left:20px;"></div>');

        $bottomPanelDiv.append($bottomPanelHeading);
        $bottomPanelDiv.append($bottomPanelContent);
        
        $outerPanelContent.append($bottomPanelDiv);

        $.each(options.bottomTrayOptionsArray, function( index, value ) 
        {
            console.log("index : " + index + " value : " + value);
            $bottomPanelContent.append($('<div id = "draggable' + index + '" class ="jigimgdraggableClass"/>'));
            var imagePath = "../img/"+options.id+"/" + value;
            console.log("Image Path ::" + imagePath);
            console.log($("#draggable"+index));
            $("#"+'draggable'+index).css('background','url('+imagePath+') no-repeat center center scroll');
        });





        var correctCount = 0;

        $( ".jigimgdraggableClass" ).draggable(
            {
              revert: true,
              placeholder: true,
              droptarget: '.jigimgdroppableClass',

            drop: function( event, droptarget ) {
            myAudio.play();
            var sourceDivId = droptarget.id;
            console.log("Droppable Id : " + sourceDivId.substr(sourceDivId.length - 1));    

            var draggableDivId = $(this).attr('id');
            var actualAnswer =draggableDivId.substr(draggableDivId.length - 1);

            console.log("ActualAnswer Id: " + actualAnswer);   

            var expectedAnswer = options.answerMappingArray[sourceDivId.substr(sourceDivId.length - 1)]
            console.log("Expected Answer Id : " + expectedAnswer);   

            var result = "";
            //var currentdropElement = ui.draggable[0];

            if (actualAnswer == expectedAnswer)
            {
                result = true;
                //$("#"+ui.draggable[0].id).draggable( "option", "revert", false );
                var imagePath = "../img/"+options.id+"/" + options.bottomTrayOptionsArray[expectedAnswer];
                //console.log("img path of droppable " + imagePath);
                $("#"+'droppable' + sourceDivId.substr(sourceDivId.length - 1)).css('background','url('+ imagePath +') no-repeat center center scroll');
                $(this).hide();

            }
            else
            {
                result = false;
                //console.log($("#"+ui.draggable[0].id));
                //$("#"+ui.draggable[0].id).draggable( "option", "revert", true );
        }
            
            $("#correctDiv1").hide();
            $("#wrongDiv1").hide();

          if (result)
          {
            correctCount++;
            $("#scoredid").html("<span class='highlight primary'>"+  correctCount    +"</span>");
            if  (correctCount == 9) 
            {
              correctCount = 0;  
              $("#congratsDiv").show();
            }  
            $("#correctDiv1").show();
            $("#wrongDiv1").hide();
            //$("#" + ui.draggable[0].id).draggable("disable");
          }
          else
          {
              $("#correctDiv1").hide();
              $("#wrongDiv1").show();
          }
      }
      });
        console.log("end of plug in execution");
        return this;
    };
    
}( jQuery ));