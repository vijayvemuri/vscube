(function ( $ ) {
    $.fn.buildGameGallery = function( gameListConfig ) {
        console.log("insider gallery html plugin");
        console.log(gameListConfig.galleryName);

        var $gameGalleryContainerDiv = $("<div/>");
        var $gameGalleryHeadingDiv = $("<h4><strong>"+gameListConfig.galleryName +"</strong></h4>");
        this.append($gameGalleryContainerDiv);
        $gameGalleryContainerDiv.append($gameGalleryHeadingDiv);

        var $gameGalleryRowDiv = $("<div class='row'/>");
        $gameGalleryContainerDiv.append($gameGalleryRowDiv);
        console.log(gameListConfig.gallery.length);
        var $gameGalleryUL = $('<ul class="list-group"/>');
        $gameGalleryRowDiv.append($gameGalleryUL);
       
        $.each(gameListConfig.gallery, function( index, value ) 
        {
            if (value.galleryGameLabel)
            {
                
                $galleryItemLI = $('<li class="list-group-item list-group-item-primary"></li>');
                $galleryItemLink = $("<a  href='#' id = vscubegame" +index +">" + value.galleryGameLabel + "</a>");
                $galleryItemLI.append($galleryItemLink);
                $gameGalleryUL.append($galleryItemLI);
            }
       });
       $("a[id^='vscubegame']").click(function (e) 
       {
        var cgame =  gameListConfig.gallery[e.target.id.substr(10)];
            openGame(cgame,getGamePage(cgame.gametype));
       });
    }; 
    function getGamePage(gameType) {
        if (gameType === 'VSCUBE.MATCHING-GAME')
        return 'matchingames_g.html';
        if (gameType === 'VSCUBE.IMG-MATCHING-GAME')
        return 'imgmatchinggames_g.html';
        if (gameType === 'VSCUBE.MCQ-GAME')
        return 'mcqquizagames_g.html';
        if (gameType === 'VSCUBE.SPELLING-GAME')
        return 'spellinggames_g.html';
        if (gameType === 'VSCUBE.WORD-FINDER-GAME')
        return 'wordfindergames_g.html';
        if (gameType === 'VSCUBE.JUMBLE-WORDS-GAME')
        return 'jwgames_g.html';
        if (gameType === 'VSCUBE.ODD-ONE-OUT-GAME')
        return 'oddoneoutgames_g.html';        
        if (gameType === 'VSCUBE.ODD-ONE-OUT-GAME')
        return 'oddoneoutgames_g.html';   
        if (gameType === 'VSCUBE.ALPHA-ORDER-GAME')
        return 'alphaordergames_g.html'; 
        if (gameType === 'VSCUBE.MISSING-LETTERS-GAME')
        return 'missinglettergames_g.html'; 
        if (gameType === 'VSCUBE.CA-QUIZ-GAME')
        return 'chooseallgames_g.html'; 
        if (gameType === 'VSCUBE.JIG-GAME')
        return 'jigsawgames_g.html'; 
        
         
    }
}( jQuery ));