(function ( $ ) {
 
    $.fn.buildWordFinderGrid = function( options, fromSeriesGame ) {
 
        console.log("start of plug in execution");
        console.log("input options " + options);
        options.wordsArray = options.wordsArray.map( word => word.toUpperCase());

        var answerCountArray = [0,0,0,0,0];
        var correctCount = 0;
        var myAudio = document.createElement('audio');
        myAudio.controls = true;
        myAudio.src = '../sounds/drop.wav';

        var correctArray = new Array();
        var wrongArray = new Array();

        var $congratsDiv =$("<div id='congratsDiv' class='congratsDiv'></div>");    
        var $gameContainerDiv = $("<div id ='gamePanelDiv'/>");
        this.append($gameContainerDiv);

        $gameContainerDiv.append($congratsDiv);
        var $headerMsgP = $("<span style='margin-left:20px;font-size:20px;' class='text-success'></span>");
        $headerMsgP.html("<strong>"+options.topTrayLabel+"</strong>" + " : " + options.headerMsg);

        var $scoreIdP = $("<span class='scoreSpanClass'><b> Score </b></span> <span id='scoredid' style='font-size:25px;' class='highlight primary'>0</span>");

        var $outerPanelDiv = $('<div class="card" style="margin-left:5px;margin-right:5px"</div>');
        var $outerPanelHeading = $('<div class="card-header"></div>');
        var $outerPanelContent = $('<div class="card-body"></div>');


        $outerPanelHeading.append($headerMsgP);
        $outerPanelHeading.append($scoreIdP);

        var $backButtonLink = $('<span style="float:right;"><a href="../html/gamegallery.html" class="btn btn-warning btn-medium btn-rounded">Back to Word Finder Games</a></span>');

        if(!fromSeriesGame){
            $outerPanelHeading.append($backButtonLink);
        }
        $outerPanelDiv.append($outerPanelHeading);
        $outerPanelDiv.append($outerPanelContent);
        
        $gameContainerDiv.append($outerPanelDiv);

        var $topPanelDiv = $('<div class="panel panel-success"></div>');
        var $topPanelHeading = $('<div class="panel-heading" style="font-size:20px;></div>');
        $topPanelHeading.text(options.topTrayLabel);
        var $topPanelContent = $('<div class="row" style="margin-left:20px;"></div>');
        var $leftPanelContent = $('<div class="leftPanelContent"></div>');


        $topPanelContent.append($leftPanelContent);

        $.each(options.gridRowsArray, function( index, value ) 
        {
            console.log("index : " + index + " value : " + value);
            var $leftPanelContentRow = $('<div style="clear:both;"></div>');

            for(var x = 0, c=''; c = value.charAt(x); x++)
            { 
                $leftPanelContentRow.append($('<div id = "draggable' + index + x +'" class ="wfdraggableClass"/>').append('<p>'+ c +'</p>'))
            }

        $leftPanelContent.append($leftPanelContentRow);

        });

        var $formContent = $('<form class="form-search" id ="formId"  name="myform"></form>');

        var $textBoxContent = $('<input id ="inputBoxId" style="margin-left:20px;margin-top:20px; type="text" class="input-medium search-query" name="msg" placeholder="Enter word""></input>');
        var $goButton = $('<button id ="goButtonId" class="btn btn-success btn-medium btn-rounded" style="margin-left:20px;margin-top:20px;">GO</button>');

        $topPanelContent.append($formContent);

        $formContent.append($textBoxContent);
        $formContent.append($goButton);

        $topPanelDiv.append($topPanelHeading);  
        $topPanelDiv.append($topPanelContent);
        
        $outerPanelContent.append($topPanelDiv);
 
 $("#formId").on("submit", function(){
    return false;
 });

        $("#goButtonId").on("click",  function(e) {

            console.log("clicked")
            var wordEntered = $("#inputBoxId").val().toUpperCase().trim();
            var index = options.wordsArray.indexOf(wordEntered);

            if (index != -1)
            {

                if (correctArray.indexOf(wordEntered) == -1)
                {
                    myAudio.play();
                    correctCount++;
                    correctArray.push(wordEntered);
                    $("#scoredid").text(correctCount);
                    var postionsForIndex = options.positionsArray[index];

                    $.each(postionsForIndex, function( index, value ) 
                    {
                        var sindex = 'draggable'+value;
                        $("#"+sindex).css('background-color',' #2ecc71');
                    });
                    $("#inputBoxId").val("");
                }
            
            if  (correctCount == 10) 
            {
              correctCount = 0;  
              $("#congratsDiv").show();
            }

            }
            else
            {
                return;
            }


           });
 }
}( jQuery ));