(function ( $ ) {
 
    $.fn.buildTreasureHuntCanvas = function( options ) {
 
        console.log("start of plug in execution");
        console.log("input options " + options);

        var correctCount = 0;
        var clickCount = 0;
        var currentHint = 0;
        var $gameContainerDiv = $("<div class='gameContainerDivClass'/>");
        this.append($gameContainerDiv);

        var $firstSource = $("<div id ='option0' class='firstOptionClass'/>");
        $gameContainerDiv.append($firstSource);

        var $secondSource = $("<div id ='option1' class='secondOptionClass'/>");
        $gameContainerDiv.append($secondSource);

        var $thirdSource = $("<div id ='option2' class='thirdOptionClass'/>");
        $gameContainerDiv.append($thirdSource);

        var $fourthSource = $("<div id ='option3' class='fourthOptionClass'/>");
        $gameContainerDiv.append($fourthSource);

        var $fifthSource = $("<div id ='option4' class='fifthOptionClass'/>");
        $gameContainerDiv.append($fifthSource);



        var $questionOuterDialog = $("<div id ='qdailogOuter'class='questionDisplayPanel'/>");
        $gameContainerDiv.append($questionOuterDialog);

        var $welcomeMsgPanel = $("<div id ='welcomeMsgPanelId'class='welcomeMsgDisplayPanel'>Welcome to Treasure Hunt. Go to <mark>Cow</mark> to start the game..!</div>");
        $gameContainerDiv.append($welcomeMsgPanel);

        

        $("div[id^='option']").click(function (e) 
        {
            $("#welcomeMsgPanelId").hide();
            var $questionDialog = $("<div id ='qdailog'/>");
            $questionOuterDialog.append($questionDialog);
            var $answersRowDiv = $('<div></div>');

            console.log($(e.target)[0].id);
            var $srcElement = $(e.target)[0];
            console.log($srcElement);

            var clickedId = $srcElement.id;
            console.log("Clicked ID :: " + clickedId);

            var clickedIndex = clickedId.charAt(clickedId.length-1);
            console.log("Clicked Index :: " + clickedIndex);

            currentHint=clickedIndex;
            var question = options.questionsArray[clickedIndex];
            var answerOptionsArray = options.questionOptionsArray[clickedIndex];

            $questionDialog.append($("<h4>Tresure Hunt.. Answer the below Question.</h4>"));

            $questionDialog.append($('<p><h3>'+question+'</h3></p>'));

            var $answersRowDiv = $('<div></div>');

            $.each(answerOptionsArray, function (iindex, ivalue)
            {
                console.log("iindex : " + iindex + " ivalue : " + ivalue);
                $answersRowDiv.append($('<div id = "answerOption' + clickedIndex + iindex +'" class ="answerDivclass">'+ivalue+'</div>'));
            });
            $questionDialog.append($answersRowDiv);
            $questionDialog.append($('<br style="clear: both;">'));

            $questionDialog.append($('<p id="nextHint" class="text-success"></p>'));
            $questionDialog.append($('<button id="closeButton" class="button-success">Close</button>'));
            $("#qdailogOuter").show();
        });
        
        $(document).on('click','#closeButton',function (e) 
        {
            $("#qdailog").remove();
            $("#qdailogOuter").hide();
            console.log("Current Hint on close ::" + currentHint);
            $("div[id^='option']").hide();
            $("#" +'option'+ currentHint).show();
            
        });
        
        $(document).on('click','.answerDivclass',function (e) 
        {
            var $srcElement = $(e.target)[0];

            var ansClickedId = $srcElement.id;
            console.log("Clicked ID :: " + ansClickedId);
            var rowPostion = ansClickedId.charAt(ansClickedId.length-2);
            var coloumPostion = ansClickedId.charAt(ansClickedId.length-1);

            console.log("rowPostion " + ansClickedId.charAt(ansClickedId.length-2));
            console.log("coloumPostion " + ansClickedId.charAt(ansClickedId.length-1));
            if(options.answerMappingArray[rowPostion] == coloumPostion)
            {
                $("#"+ansClickedId).removeClass('answerDivclass').addClass('answerDivCorrectClass');
                console.log($("[id^=" + ansClickedId.substr(0,ansClickedId.length-1)+"]"));
                $("[id^=" + ansClickedId.substr(0,ansClickedId.length-1)+"]").off();
                console.log("Current Hint ::" + currentHint);
                var nexthintName = options.hintSeqArray[++currentHint];

                if(nexthintName != undefined)
                {
                    $("#closeButton").text("Go To " + nexthintName);
                    $("#nextHint").html("<h4>Correct. Good Job!.</h4>");

                }
                else
                {
                    $("#closeButton").text("Close to finish the Game");
                    $("#nextHint").html("Congratulation. You won the treasure..");
                    $("div[id^='option']").hide();
                    $("#" +'option'+ (currentHint-1)).hide();
                }

                $("#" +'option'+ rowPostion).show();
            }
            else
            {
                $("#"+ansClickedId).removeClass("answerDivclass").addClass("answerDivWrongClass");
                $("[id^=" + ansClickedId.substr(0,ansClickedId.length-1)+"]").off();

                $("div[id^='option']").hide();
                $("#" +'option'+ (currentHint-1)).hide();

            }            
        });
        return this;    
    };
}( jQuery ));