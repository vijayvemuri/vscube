(function ( $ ) {
 
    $.fn.buildMixMatchGrid = function( options ) {
 
        console.log("start of plug in execution");
        console.log("input options " + options);

        var myAudio = document.createElement('audio');
        myAudio.controls = true;
        myAudio.src = '../sounds/drop.wav';

        var $congratsDiv =$("<div id='congratsDiv' class='congratsDiv'></div>");    
        var $gameContainerDiv = $("<div/>");
        this.append($gameContainerDiv);

        $gameContainerDiv.append($congratsDiv);
        var $headerMsgP = $("<span style='margin-left:20px;font-size:20px;' class='text-success'></span>");
        $headerMsgP.html(options.headerMsg);



        var $correctMsgDiv = $("<span id = 'correctDiv1' class='text-success' style='display: none;margin-left:20px;'><img style='width:40px;height:40px;' src='../img/correct.png'></img></span>");
        var $wrongMessageDiv = $("<span id = 'wrongDiv1' class='text-danger' style='display: none;margin-left:20px;'><img style='width:40px;height:40px;' src='../img/wrong.png'></img></span>");


        var $scoreIdP = $("<span class='scoreSpanClass'><b> Score </b></span> <span id='scoredid' style='font-size:25px;' class='highlight primary'>0</span>");

        var $outerPanelDiv = $('<div class="card" style="margin-left:5px;margin-right:5px"</div>');
        var $outerPanelHeading = $('<div class="card-header"></div>');
        var $outerPanelContent = $('<div class="card-body"></div>');


        $outerPanelHeading.append($headerMsgP);
        $outerPanelHeading.append($scoreIdP);

        var $backButtonLink = $('<span style="float:right;"><a href="../html/gamegallery.html" class="btn btn-warning btn-medium btn-rounded">Back to Matching Games</a></span>');


        $outerPanelHeading.append($correctMsgDiv);
        $outerPanelHeading.append($wrongMessageDiv);
        $outerPanelHeading.append($backButtonLink);

        $outerPanelDiv.append($outerPanelHeading);
        $outerPanelDiv.append($outerPanelContent);
        
        $gameContainerDiv.append($outerPanelDiv);

        var $topPanelDiv = $('<div class="card"></div>');
        var $topPanelHeading = $('<h5 class="card-header"></div>');
        var $topPanelContent = $('<div class="row" style="margin-left:20px;"></div>');

        var $topTray1Div = $('<div id = "droppable0" class="imgdroppableClass"></div>');
        var $topTray2Div = $('<div id = "droppable1" class="imgdroppableClass"></div>');
        var $topTray3Div = $('<div id = "droppable2" class="imgdroppableClass"></div>');
        $topTray1Div.append('<p>'+ options.topTray1Label +'</p>');
        $topTray2Div.append('<p>'+ options.topTray2Label +'</p>')
        $topTray3Div.append('<p>'+ options.topTray3Label +'</p>')

        $topPanelContent.append($topTray1Div);
        $topPanelContent.append($topTray2Div);
        $topPanelContent.append($topTray3Div);
        

        $topPanelDiv.append($topPanelHeading);
        $topPanelDiv.append($topPanelContent);
        
        $outerPanelContent.append($topPanelDiv);

        var $bottomPanelDiv = $('<div class="card" style="margin-top: 20px;"></div>');
        var $bottomPanelHeading = $('<h5 class="card-header">'+options.bottomTrayLabel+'</div>');
        var $bottomPanelContent = $('<div class="row" style="margin-left:20px;"></div>');

        $bottomPanelDiv.append($bottomPanelHeading);
        $bottomPanelDiv.append($bottomPanelContent);
        
        $outerPanelContent.append($bottomPanelDiv);

        $.each(options.bottomTrayOptionsArray, function( index, value ) 
        {
            console.log("index : " + index + " value : " + value);
            $bottomPanelContent.append($('<div id = "draggable' + index + '" class ="miximgdraggableClass"/>'));
               
            $("#"+'draggable'+index).append($('<p>' + value + '</p>'));
        });

        var correctCount =0;
        $( ".miximgdraggableClass" ).draggable({

            revert: true,
            placeholder: true,
            droptarget: '.imgdroppableClass',


            drop: function( event, droptarget ) {

            myAudio.play();
            var sourceDivId = droptarget.id;
            //console.log("Droppable Id : " + sourceDivId.substr(sourceDivId.length - 1));    

            if(!sourceDivId.startsWith('droppable'))
            {
                return;
            }

            var draggableDivId = $(this).attr('id');
            var answerDivIdLast = draggableDivId.length - 9;
            var actualAnswer =draggableDivId.substr(draggableDivId.length - answerDivIdLast);

            //console.log("ActualAnswer Id: " + actualAnswer);   

            var expectedAnswerArray = options.answerMappingArray[sourceDivId.substr(sourceDivId.length - 1)]
            //console.log("Expected Answer Id : " + expectedAnswerArray);   

            var result = "";
            //console.log("expression :: "+ expectedAnswerArray.indexOf(parseInt(actualAnswer)));
            if (expectedAnswerArray.indexOf(parseInt(actualAnswer)) != -1)
            {
                result = true;
                //$("#"+ui.draggable[0].id).draggable( "option", "revert", false);
            }
            else
            {
                result = false;
                //console.log($("#"+ui.draggable[0].id));
                //$("#"+ui.draggable[0].id).draggable( "option", "revert", true );
        }
            
            $("#correctDiv1").hide();
            $("#wrongDiv1").hide();

          if (result)
          {
             $(this).appendTo(droptarget).draggable('destroy');
            correctCount++;
            $("#scoredid").text(correctCount);
            if  (correctCount == options.bottomTrayOptionsArray.length) 
            {
              correctCount = 0;  
              $("#congratsDiv").show();
            }  
            $("#correctDiv1").show();
            $("#wrongDiv1").hide();
            //$("#" + ui.draggable[0].id).draggable("disable");
          }
          else
          {
              $("#correctDiv1").hide();
              $("#wrongDiv1").show();
          }
      }
      });
        console.log("end of plug in execution");
        return this;
    };
 
}( jQuery ));