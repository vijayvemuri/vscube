(function ( $ ) {

     function shuffle(arr) {
        for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);
        return arr;
    }
 
    $.fn.buildTopNBottomMatchingGrid = function( options, fromSeriesGame ) {
 
        var correctCount = 0;

        var origAnwersArray = options.answerMappingArray.slice(0);
        var origTArray = options.topTrayOptionsArray.slice(0);
        var origBArray = options.bottomTrayOptionsArray.slice(0);
        shuffle(options.topTrayOptionsArray);
        shuffle(options.bottomTrayOptionsArray);

        var myAudio = document.createElement('audio');
        myAudio.controls = true;
        myAudio.src = '../sounds/drop.wav';

        var $congratsDiv =$("<div id='congratsDiv' class='congratsDiv'></div>");    
        var $gameContainerDiv = $("<div id='gamePanelDiv' />");
        this.append($gameContainerDiv);

        $gameContainerDiv.append($congratsDiv);
        var $headerMsgP = $("<span class='col-sm-6'></span>");
        $headerMsgP.html(options.headerMsg);



        var $correctMsgDiv = $("<span id = 'correctDiv1'  style='display: none;margin-left:20px;'><img style='width:30px;height:30px;' src='../img/correct.png'></img></span>");
        var $wrongMessageDiv = $("<span id = 'wrongDiv1'  style='display: none;margin-left:20px;'><img style='width:30px;height:30px;' src='../img/wrong.png'></img></span>");


        var $scoreIdP = $("<span class='col-sm-6 scoreSpanClass'><b> Score </b></span> <span id='scoredid' style='font-size:25px;' class='highlight primary'>0</span>");

        var $backButtonLink = $('<span style="float:right;"><a href="../html/gamegallery.html" class="btn btn-warning btn-medium btn-rounded">Back to Matching Games</a></span>');

        var $outerPanelDiv = $('<div class="card" style="margin-left:5px;margin-right:5px"</div>');
        var $outerPanelHeading = $('<div class="card-header"></div>');
        var $outerPanelContent = $('<div class="card-body"></div>');


        $outerPanelHeading.append($headerMsgP);
        $outerPanelHeading.append($scoreIdP);

        if(!fromSeriesGame){
            $outerPanelHeading.append($backButtonLink);
        }
        $outerPanelHeading.append($correctMsgDiv);
        $outerPanelHeading.append($wrongMessageDiv);
        $outerPanelDiv.append($outerPanelHeading);
        $outerPanelDiv.append($outerPanelContent);
        
        $gameContainerDiv.append($outerPanelDiv);

        var $topPanelDiv = $('<div class="card"></div>');
        var $topPanelHeading = $('<h5 class="card-header">'+options.topTrayLabel+'</div>');
        var $topPanelContent = $('<div class="row" style="margin-left:20px;"></div>');
1

        $.each(options.topTrayOptionsArray, function( index, value ) 
        {
            //console.log("index : " + index + " value : " + value);
            options.answerMappingArray[index] = options.bottomTrayOptionsArray.indexOf(origBArray[origAnwersArray[origTArray.indexOf(value)]]);
            //$topPanelContent.append($('<div id = "droppable' + index + '" class ="droppableClass"/>').append('<p class ="labelClass">'+ value +'</p>'))
            $topPanelContent.append($('<div id = "droppable' + index + '" class ="droppableClass"/>').append('<label>'+ value +'</label>'))
        });
        $topPanelDiv.append($topPanelHeading);  
        $topPanelDiv.append($topPanelContent);
        
        $outerPanelContent.append($topPanelDiv);

        var $bottomPanelDiv = $('<div class="card" style="margin-top: 20px;"></div>');
        var $bottomPanelHeading = $('<h5 class="card-header">'+options.bottomTrayLabel+'</div>');
        var $bottomPanelContent = $('<div class="row" style="margin-left:20px;"></div>');

        $.each(options.bottomTrayOptionsArray, function( index, value ) 
        {
            //console.log("index : " + index + " value : " + value);
              $bottomPanelContent.append($('<div id = "draggable' + index + '" class ="draggableClass"/>').append('<label>'+ value +'</label>'))
        });

        $bottomPanelDiv.append($bottomPanelHeading);
        $bottomPanelDiv.append($bottomPanelContent);
        
        $outerPanelContent.append($bottomPanelDiv);


        $(".draggableClass").draggable(
            {
              revert: true,
              placeholder: true,
              droptarget: '.droppableClass',

            drop: function( event, droptarget ) {

            //console.log(this);

            myAudio.play();
            var sourceDivId = droptarget.id;

            if(!sourceDivId.startsWith('droppable'))
            {
                return;
            }
            //console.log("Droppable Id : " + sourceDivId.substr(sourceDivId.length - (sourceDivId.length-9)));    

            var draggableDivId = $(this).attr('id');
            var answerDivIdLast = draggableDivId.length - 9;
            var actualAnswer =draggableDivId.substr(draggableDivId.length - answerDivIdLast);

            //console.log("ActualAnswer Id: " + actualAnswer);   

            var expectedAnswer = options.answerMappingArray[sourceDivId.substr(sourceDivId.length - (sourceDivId.length-9))]
            console.log("Expected Answer Id : " + expectedAnswer);   

            var result = "";
            //var currentdropElement = ui.draggable[0];

            if (actualAnswer == expectedAnswer)
            {
                result = true;
                //$(this).draggable( "option", "revert", false );
                var draggableTxt = $(this).children().text();
                $("#"+'droppable' + sourceDivId.substr(sourceDivId.length - (sourceDivId.length-9))).css('background-color',' #2ecc71').append($('<label class ="labelCorrectClass">&nbsp;&nbsp;'+ draggableTxt +' </label>'));
                $(this).hide();
            }
            else
            {
                result = false;
                //$(this).draggable( "option", "revert", true );
            }
            
            $("#correctDiv1").hide();
            $("#wrongDiv1").hide();

          if (result)
          {
            correctCount++;

            $("#scoredid").text(correctCount);
            if  (correctCount == parseInt(options.topTrayOptionsArray.length)) 
            {
              correctCount = 0;  
              $("#congratsDiv").show();
            }  
            $("#correctDiv1").show();
            $("#wrongDiv1").hide();
            $(this).draggable("disable");
          }
          else
          {
              $("#correctDiv1").hide();
              $("#wrongDiv1").show();
          }
      }
            });
 

        //console.log("end of plug in execution");
        return this;
    };
 
}( jQuery ));