(function ( $ ) {
 
    $.fn.buildWordFinderSeriesGrid = function( options ) {

        var currentGame = 0;
        var $gameSeriesContainerDiv = $("<div/>");
        this.append($gameSeriesContainerDiv);
        var $gameSeriesPanelDiv = $('<div class="card" style="margin-left:5px;margin-right:5px"</div>');
        var $gameSeriesPanelHeading = $('<div class="card-header"></div>');
        var $gameSeriesPanelContent = $('<div id =""gamePanel" class="card-body"></div>');

        var $previousGameButtonLink = $('<button id = "prevGameButton" class ="btn btn-primary btn-medium">Previous</button>');
        var $nextGameButtonLink = $('<button id = "nextGameButton" class ="btn btn-warning btn-small">Next</button>');
        var $spellingGamesGalleryButtonLink = $('<span style="float:right;"><a href="../html/gamegallery.html" class="btn btn-info btn-medium">Word Finder Games Gallery</a></span>');
        $gameSeriesPanelHeading.append($previousGameButtonLink);
        $gameSeriesPanelHeading.append($nextGameButtonLink);
        $gameSeriesPanelHeading.append($spellingGamesGalleryButtonLink);

        $gameSeriesContainerDiv.append($gameSeriesPanelDiv);
        $gameSeriesPanelDiv.append($gameSeriesPanelHeading);
        $gameSeriesPanelDiv.append($gameSeriesPanelContent);

        $gameSeriesPanelContent.buildWordFinderGrid(options[currentGame], true);

        $("#prevGameButton").click(function(){
            if (currentGame === 0) return;
            $("#gamePanelDiv").remove();
            $gameSeriesPanelContent.buildWordFinderGrid(options[--currentGame],true);
        });

        $("#nextGameButton").click(function(){
            if (currentGame === (options.length-1)) return;
            $("#gamePanelDiv").remove();
            $gameSeriesPanelContent.buildWordFinderGrid(options[++currentGame],true);
        });
    };  
}( jQuery ));