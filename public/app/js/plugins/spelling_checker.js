(function ( $ ) {
 
    $.fn.buildSpellCheckerGrid = function( options, fromSeriesGame ) {
 
        console.log("start of plug in execution");
        console.log("input options " + options);

        var correctCount = 0;
        var clickCount = 0;

        var myAudio = document.createElement('audio');
        myAudio.controls = true;
        myAudio.src = '../drop.wav';
        console.log("creating a new gamePanelDiv");
        var $gameContainerDiv = $("<div id='gamePanelDiv' />");
        this.append($gameContainerDiv);
        var $congratsDiv =$("<div id='congratsDiv' class='congratsDiv'></div>");    
        $gameContainerDiv.append($congratsDiv);
        
        var $tryagainDiv =$("<div id='tryagainDiv' class='tryagainDiv'></div>");    
        $gameContainerDiv.append($tryagainDiv);

        var $headerMsgP = $("<span style='margin-left:20px;font-size:20px;' class='text-success'></span>");
        $headerMsgP.html(options.headerMsg);

        var $scoreIdP = $("<span class='scoreSpanClass'><b> Score </b></span> <span id='scoredid' style='font-size:25px;' class='highlight primary'>0</span>");

        var $backButtonLink = $('<span style="float:right;"><a href="../html/gamegallery.html" class="btn btn-warning btn-medium btn-rounded">Back to Spelling Games</a></span>');

        var $outerPanelDiv = $('<div class="card" style="margin-left:5px;margin-right:5px"</div>');
        var $outerPanelHeading = $('<div class="card-header"></div>');
        var $outerPanelContent = $('<div class="card-body"></div>');

        $outerPanelHeading.append($headerMsgP);
        $outerPanelHeading.append($scoreIdP);
        if(!fromSeriesGame){
            $outerPanelHeading.append($backButtonLink);
        }

        $outerPanelDiv.append($outerPanelHeading);
        $outerPanelDiv.append($outerPanelContent);
        
        $gameContainerDiv.append($outerPanelDiv);
        var $headerMsgP = $("<p class='text-success'></p>");

        var $topPanelDiv = $('<div class="container card"></div>');
        var $topPanelContent = $('<div class="card-body"></div>');
1

        $.each(options.questionOptionsArray, function( index, value ) 
        {
            console.log("index : " + index + " value : " + value);
            var $spellRowDiv = $('<div class="row"></div>');

            $.each(value, function (iindex, ivalue)
            {
                console.log("iindex : " + iindex + " ivalue : " + ivalue);
                $spellRowDiv.append($('<span id = "spelling' + index + iindex +'" class ="col spellDivclass">'+ivalue+'</span>'));
            });
            $topPanelContent.append($spellRowDiv);

            $topPanelContent.append($('<br style="clear: both;">'));

        });
        $topPanelDiv.append($topPanelContent);
        
        $outerPanelContent.append($topPanelDiv);
    

        $(".spellDivclass").click(function (e) 
        {
            clickCount++;
            myAudio.play();
            console.log($(e.target)[0].id);
            var $srcElement = $(e.target)[0];
            console.log($srcElement);

            var clickedId = $srcElement.id;
            console.log("Clicked ID :: " + clickedId);
            var rowPostion = clickedId.charAt(clickedId.length-2);
            var coloumPostion = clickedId.charAt(clickedId.length-1);

            console.log("rowPostion " + clickedId.charAt(clickedId.length-2));
            console.log("coloumPostion " + clickedId.charAt(clickedId.length-1));
            if(options.answerMappingArray[rowPostion] == coloumPostion)
            {
                $("#"+clickedId).removeClass('spellDivclass').addClass('spellDivCorrectClass');
                console.log($("[id^=" + clickedId.substr(0,clickedId.length-1)+"]"));
                $("[id^=" + clickedId.substr(0,clickedId.length-1)+"]").off();
                correctCount++;
            $("#scoredid").text(correctCount);

            }
            else
            {
                $("#"+clickedId).removeClass("spellDivclass").addClass("spellDivWrongClass");
                $("[id^=" + clickedId.substr(0,clickedId.length-1)+"]").off();
            }
            if  (clickCount ==5 && correctCount == 5) 
            {
              correctCount = 0;  
              $("#congratsDiv").show();
            }
            else if (clickCount ==5 && correctCount !=5)
            {
              $("#tryagainDiv").show();
          }
        });
        $(".tryagainDiv").click(function (e) 
        {
            location.reload(true);
        });

        console.log("end of plug in execution");
        return this;
    };
}( jQuery ));