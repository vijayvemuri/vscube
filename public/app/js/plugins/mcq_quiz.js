(function ( $ ) {
 
    $.fn.buildMCQQuizGrid = function( options, fromSeriesGame ) {
 
        console.log("start of plug in execution");
        console.log("input options " + options);

        var correctCount = 0;
        var clickCount = 0;

        var myAudio = document.createElement('audio');
        myAudio.controls = true;
        myAudio.src = '../drop.wav';

        var $gameContainerDiv = $("<div class='container' id='gamePanelDiv'/>");
        this.append($gameContainerDiv);
        var $congratsDiv =$("<div id='congratsDiv' class='congratsDiv'></div>");    
        $gameContainerDiv.append($congratsDiv);
        
        var $tryagainDiv =$("<div id='tryagainDiv' class='tryagainDiv'></div>");    
        $gameContainerDiv.append($tryagainDiv);

        var $outerPanelDiv = $('<div class="card" style="margin-left:5px;margin-right:5px"</div>');
        var $outerPanelHeading = $('<div class="row card-header"></div>');
        var $outerPanelContent = $('<div class="card-body"></div>');

        var $headerMsgP = $("<span class='col text-success'></span>");
        $headerMsgP.html(options.headerMsg);


        var $scoreIdP = $("<span class='col'><b> Score </b><span id='scoredid' class='highlight primary'>0</span></span>");

        $outerPanelHeading.append($headerMsgP);
        $outerPanelHeading.append($scoreIdP);

        var $backButtonLink = $('<span><a href="../html/gamegallery.html" class="col btn btn-warning btn-medium btn-rounded">Back to Quiz Games</a></span>');

        if(!fromSeriesGame){
            $outerPanelHeading.append($backButtonLink);
        }
        $outerPanelDiv.append($outerPanelHeading);
        $outerPanelDiv.append($outerPanelContent);
        
        $gameContainerDiv.append($outerPanelDiv);
        var $headerMsgP = $("<p class='text-success'></p>");

        var $topPanelDiv = $('<div class="card"></div>');
        var $topPanelContent = $('<div class="card-body"></div>');
1

        $.each(options.questionOptionsArray, function( index, value ) 
        {
            console.log("index : " + index + " value : " + value);

            var $questionSectionDiv = $('<div class="questionSectionDivClass"></div>');
            $topPanelContent.append($questionSectionDiv);

            var $questionRowDiv = $('<div class="qquestionClass"><p class="questionPClass"><b>'+options.questionsArray[index]+'</b></p></div>');
            //var $questionRowDiv = $('<div class="qquestionClass"><p class="questionPClass">'+options.questionsArray[index]+'</p></div>');
            var $spellRowDiv = $('<div class="row"></div>');

            $.each(value, function (iindex, ivalue)
            {
                console.log("iindex : " + iindex + " ivalue : " + ivalue);
                $spellRowDiv.append($('<div id = "spelling' + index + iindex +'" class ="col qanswerDivclass" style="margin-left:20px;">'+ivalue+'</div>'));
            });
            $questionSectionDiv.append($questionRowDiv);
            $questionSectionDiv.append($spellRowDiv);


            $topPanelContent.append($('<br style="clear: both;">'));

        });
        $topPanelDiv.append($topPanelContent);
        
        $outerPanelContent.append($topPanelDiv);
    



        $(".qanswerDivclass").click(function (e) 
        {
            clickCount++;
            myAudio.play();
            console.log($(e.target)[0].id);
            var $srcElement = $(e.target)[0];
            console.log($srcElement);

            var clickedId = $srcElement.id;
            console.log("Clicked ID :: " + clickedId);
            var rowPostion = clickedId.charAt(clickedId.length-2);
            var coloumPostion = clickedId.charAt(clickedId.length-1);

            console.log("rowPostion " + clickedId.charAt(clickedId.length-2));
            console.log("coloumPostion " + clickedId.charAt(clickedId.length-1));
            if(options.answerMappingArray[rowPostion] == coloumPostion)
            {
                $("#"+clickedId).removeClass('qanswerDivclass').addClass('qanswerDivCorrectClass');
                console.log($("[id^=" + clickedId.substr(0,clickedId.length-1)+"]"));
                $("[id^=" + clickedId.substr(0,clickedId.length-1)+"]").off();
                correctCount++;
            $("#scoredid").text(correctCount);

            }
            else
            {
                $("#"+clickedId).removeClass("qanswerDivclass").addClass("qanswerDivWrongClass");
                $("[id^=" + clickedId.substr(0,clickedId.length-1)+"]").off();
            }
            if  (clickCount ==5 && correctCount == 5) 
            {
              correctCount = 0;  
              $("#congratsDiv").show();
            }
            else if (clickCount ==5 && correctCount !=5)
            {
              $("#tryagainDiv").show();
          }
        });
        $(".tryagainDiv").click(function (e) 
        {
            location.reload(true);
        });

        console.log("end of plug in execution");
        return this;
    };
}( jQuery ));