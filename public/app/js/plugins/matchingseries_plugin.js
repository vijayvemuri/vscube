(function ( $ ) {
 
    $.fn.buildMatchingSeriesGrid = function( options ) {

        var currentGame = 0;
        var $gameSeriesContainerDiv = $("<div/>");
        this.append($gameSeriesContainerDiv);
        var $gameSeriesPanelDiv = $('<div class="card" style="margin-left:5px;margin-right:5px"</div>');
        var $gameSeriesPanelHeading = $('<div class="card-header"></div>');
        var $gameSeriesPanelContent = $('<div id =""gamePanel" class="card-body"></div>');

        var $previousGameButtonLink = $('<button id = "prevGameButton" class ="btn btn-success btn-medium">Previous</button>');
        var $nextGameButtonLink = $('<button id = "nextGameButton" class ="btn btn-success btn-medium">Next</button>');
        var $spellingGamesGalleryButtonLink = $('<span style="float:right;"><a href="../html/gamegallery.html" class="btn btn-success btn-medium">Matching Games Gallery</a></span>');
        $gameSeriesPanelHeading.append($previousGameButtonLink);
        $gameSeriesPanelHeading.append($nextGameButtonLink);
        $gameSeriesPanelHeading.append($spellingGamesGalleryButtonLink);

        $gameSeriesContainerDiv.append($gameSeriesPanelDiv);
        $gameSeriesPanelDiv.append($gameSeriesPanelHeading);
        $gameSeriesPanelDiv.append($gameSeriesPanelContent);

        $gameSeriesPanelContent.buildTopNBottomMatchingGrid(options[currentGame], true);

        $("#prevGameButton").click(function(){
            if (currentGame === 0) return;
            $("#gamePanelDiv").remove();
            $gameSeriesPanelContent.buildTopNBottomMatchingGrid(options[--currentGame],true);
        });

        $("#nextGameButton").click(function(){
            if (currentGame === (options.length-1)) return;
            $("#gamePanelDiv").remove();
            $gameSeriesPanelContent.buildTopNBottomMatchingGrid(options[++currentGame],true);
        });
    };  
}( jQuery ));