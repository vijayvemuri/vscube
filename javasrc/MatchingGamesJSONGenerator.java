package vscube.matchinggames;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MatchingGamesJSONGenerator {

    public static void main (String args[])
    {
        MatchingConfig config = new MatchingConfig();

        String filePath = "F:\\vscube-data\\matchingames\\gamedata.xlsx";
        List<MatchingPair> matchingPairsList = new ArrayList<>();
        try
        {
            FileInputStream fis = new FileInputStream(new File(filePath));
            Workbook workbook = new XSSFWorkbook(fis);
            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> iterator = sheet.iterator();

            while(iterator.hasNext())
            {
                Row currentRow = iterator.next();

                if(currentRow.getRowNum()==0)
                {
                    config.setId(currentRow.getCell(1).getStringCellValue());
                    continue;
                }
                if(currentRow.getRowNum()==1)
                {
                    config.setHeaderMsg(currentRow.getCell(1).getStringCellValue());
                    continue;
                }
                if(currentRow.getRowNum()==2)
                {
                    config.setTopTrayLabel(currentRow.getCell(1).getStringCellValue());
                    continue;
                }
                if(currentRow.getRowNum()==3)
                {
                    config.setBottomTrayLabel(currentRow.getCell(1).getStringCellValue());
                    continue;
                }


                MatchingPair pair = new MatchingPair(currentRow.getCell(0).getStringCellValue(),currentRow.getCell(1).getStringCellValue());
                matchingPairsList.add(pair);
            }
            ArrayList<Integer> intList = new ArrayList<>();

            for(int i=0; i<matchingPairsList.size();i++)
            {
                intList.add(i);
            }
            Collections.shuffle(intList);


            ArrayList<String> topList = new ArrayList<>();
            ArrayList<String> bottomList = new ArrayList<>();

            for (MatchingPair pair :matchingPairsList)
            {
                topList.add(pair.getTop());
                bottomList.add(pair.getBottom());
            }

            ArrayList<String> finalBottomList = new ArrayList<>(bottomList);

            for(int i =0; i < intList.size();i++)
            {
                finalBottomList.set(intList.get(i),bottomList.get(i));
            }

            config.setTopTrayOptionsArray(topList);
            config.setBottomTrayOptionsArray(finalBottomList);
            config.setAnswerMappingArray(intList);

            ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter().writeValue(System.out,config);


        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }

}
class MatchingConfig {
    String id;
    String headerMsg;
    String topTrayLabel;
    ArrayList<String> topTrayOptionsArray;
    String bottomTrayLabel;
    ArrayList<String> bottomTrayOptionsArray;
    ArrayList<Integer> answerMappingArray;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeaderMsg() {
        return headerMsg;
    }

    public void setHeaderMsg(String headerMsg) {
        this.headerMsg = headerMsg;
    }

    public String getTopTrayLabel() {
        return topTrayLabel;
    }

    public void setTopTrayLabel(String topTrayLabel) {
        this.topTrayLabel = topTrayLabel;
    }

    public ArrayList<String> getTopTrayOptionsArray() {
        return topTrayOptionsArray;
    }

    public void setTopTrayOptionsArray(ArrayList<String> topTrayOptionsArray) {
        this.topTrayOptionsArray = topTrayOptionsArray;
    }

    public String getBottomTrayLabel() {
        return bottomTrayLabel;
    }

    public void setBottomTrayLabel(String bottomTrayLabel) {
        this.bottomTrayLabel = bottomTrayLabel;
    }

    public ArrayList<String> getBottomTrayOptionsArray() {
        return bottomTrayOptionsArray;
    }

    public void setBottomTrayOptionsArray(ArrayList<String> bottomTrayOptionsArray) {
        this.bottomTrayOptionsArray = bottomTrayOptionsArray;
    }

    public ArrayList<Integer> getAnswerMappingArray() {
        return answerMappingArray;
    }

    public void setAnswerMappingArray(ArrayList<Integer> answerMappingArray) {
        this.answerMappingArray = answerMappingArray;
    }

    @Override
    public String toString() {
        return "MatchingConfig{" +
                "id='" + id + '\'' +
                ", headerMsg='" + headerMsg + '\'' +
                ", topTrayLabel='" + topTrayLabel + '\'' +
                ", topTrayOptionsArray=" + topTrayOptionsArray +
                ", bottomTrayLabel='" + bottomTrayLabel + '\'' +
                ", bottomTrayOptionsArray=" + bottomTrayOptionsArray +
                ", answerMappingArray=" + answerMappingArray +
                '}';
    }
}
    class MatchingPair
    {
        String top;
        String bottom;
        int index;

        public MatchingPair(String top, String bottom, int index) {
            this.top = top;
            this.bottom = bottom;
            this.index = index;
        }
        public MatchingPair(String top, String bottom) {
            this.top = top;
            this.bottom = bottom;
        }

        public String getTop() {
            return top;
        }

        public void setTop(String top) {
            this.top = top;
        }

        public String getBottom() {
            return bottom;
        }

        public void setBottom(String bottom) {
            this.bottom = bottom;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        @Override
        public String toString() {
            return "MatchingPair{" +
                    "top='" + top + '\'' +
                    ", bottom='" + bottom + '\'' +
                    ", index=" + index +
                    '}';
        }
    }
