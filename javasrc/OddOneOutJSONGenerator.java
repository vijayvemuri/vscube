package vscube.oddoneoutgames;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class OddOneOutJSONGenerator {

    public static void main(String args[])
    {
        OddOneOutConfig config = new OddOneOutConfig();
        String filePath = "F:\\vscube-data\\oddoneoutgames\\gamedata.xlsx";
        ArrayList<OddOneOutDetails> questionsList = new ArrayList<>();
        try
        {
            FileInputStream fis = new FileInputStream(new File(filePath));
            Workbook workbook = new XSSFWorkbook(fis);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = sheet.iterator();
            while(iterator.hasNext())
            {
                Row currentRow = iterator.next();
                if(currentRow.getRowNum()==0)
                {
                    config.setId(currentRow.getCell(1).getStringCellValue());
                    continue;
                }
                if(currentRow.getRowNum()==1)
                {
                    config.setHeaderMsg(currentRow.getCell(1).getStringCellValue());
                    continue;
                }

                OddOneOutDetails oddOneOutDetails = new OddOneOutDetails();
                oddOneOutDetails.setOption1(currentRow.getCell(0).getStringCellValue());
                oddOneOutDetails.setOption2(currentRow.getCell(1).getStringCellValue());
                oddOneOutDetails.setOption3(currentRow.getCell(2).getStringCellValue());
                oddOneOutDetails.setOption4(currentRow.getCell(3).getStringCellValue());
                oddOneOutDetails.setCorrectAnswerIndex(Double.valueOf(currentRow.getCell(4).getNumericCellValue()).intValue());
                oddOneOutDetails.setReason(currentRow.getCell(5).getStringCellValue());

                questionsList.add(oddOneOutDetails);
            }

            ArrayList<ArrayList<String>> qArray = new ArrayList<>();
            ArrayList<String> reasons = new ArrayList<>();
            ArrayList<Integer> answers = new ArrayList<>();
            for (OddOneOutDetails details : questionsList)
            {
                ArrayList<String> qOptions = new ArrayList<>();
                qOptions.add(details.getOption1());
                qOptions.add(details.getOption2());
                qOptions.add(details.getOption3());
                qOptions.add(details.getOption4());

                qArray.add(qOptions);
                reasons.add(details.getReason());
                answers.add(details.getCorrectAnswerIndex()-1);
            }
            config.setAnswerMappingArray(answers);;
            config.setQuestionOptionsArray(qArray);
            config.setReasonsArray(reasons);

            ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter().writeValue(System.out,config);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}


class OddOneOutDetails
{
    String option1;
    String option2;
    String option3;
    String option4;
    int correctAnswerIndex;
    String reason;

    public OddOneOutDetails() {
    }

    public OddOneOutDetails(String option1, String option2, String option3, String option4, int correctAnswerIndex, String reason) {
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.correctAnswerIndex = correctAnswerIndex;
        this.reason = reason;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public int getCorrectAnswerIndex() {
        return correctAnswerIndex;
    }

    public void setCorrectAnswerIndex(int correctAnswerIndex) {
        this.correctAnswerIndex = correctAnswerIndex;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}

class OddOneOutConfig
{
    String id;
    String headerMsg;
    ArrayList<ArrayList<String>> questionOptionsArray;
    ArrayList<String> reasonsArray;
    ArrayList<Integer> answerMappingArray;

    @Override
    public String toString() {
        return "OddOneOutConfig{" +
                "id='" + id + '\'' +
                ", headerMsg='" + headerMsg + '\'' +
                ", questionOptionsArray=" + questionOptionsArray +
                ", reasonsArray=" + reasonsArray +
                ", answerMappingArray=" + answerMappingArray +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeaderMsg() {
        return headerMsg;
    }

    public void setHeaderMsg(String headerMsg) {
        this.headerMsg = headerMsg;
    }

    public ArrayList<ArrayList<String>> getQuestionOptionsArray() {
        return questionOptionsArray;
    }

    public void setQuestionOptionsArray(ArrayList<ArrayList<String>> questionOptionsArray) {
        this.questionOptionsArray = questionOptionsArray;
    }

    public ArrayList<String> getReasonsArray() {
        return reasonsArray;
    }

    public void setReasonsArray(ArrayList<String> reasonsArray) {
        this.reasonsArray = reasonsArray;
    }

    public ArrayList<Integer> getAnswerMappingArray() {
        return answerMappingArray;
    }

    public void setAnswerMappingArray(ArrayList<Integer> answerMappingArray) {
        this.answerMappingArray = answerMappingArray;
    }
}