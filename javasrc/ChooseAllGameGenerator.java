package vscube.chooseallgames;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class ChooseAllGameGenerator {

    public static void main(String args[])
    {
        ChooseAllGameConfig config = new ChooseAllGameConfig();
        String filePath = "F:\\vscube-data\\chooseallgames\\gamedata.xlsx";
        ArrayList<ChooseAllGameDetails> qoptionsList = new ArrayList<>();
        try
        {
            FileInputStream fis = new FileInputStream(new File(filePath));
            Workbook workbook = new XSSFWorkbook(fis);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = sheet.iterator();
            while(iterator.hasNext())
            {
                Row currentRow = iterator.next();
                System.out.println(currentRow.getRowNum());
                if(currentRow.getRowNum()==0)
                {
                    config.setId(currentRow.getCell(1).getStringCellValue());
                    continue;
                }
                if(currentRow.getRowNum()==1)
                {
                    ArrayList<String> qlist = new ArrayList<>();
                    qlist.add(currentRow.getCell(1).getStringCellValue());
                    config.setQuestionsArray(qlist);
                    continue;
                }
                ChooseAllGameDetails chooseAllGameDetails = new ChooseAllGameDetails(currentRow.getCell(0).getStringCellValue(), (currentRow.getCell(1))==null?"incorrect":currentRow.getCell(1).getStringCellValue());
                qoptionsList.add(chooseAllGameDetails);
              }

            ArrayList<String> qoptions = new ArrayList<>();
            ArrayList<Integer> answers = new ArrayList<>();

            for (int i = 0; i < qoptionsList.size();i++)
            {
                qoptions.add(qoptionsList.get(i).getOption());
                if (qoptionsList.get(i).getCorrectFlag().equalsIgnoreCase("correct"))
                {
                    answers.add(i);
                }
            }

            config.setAnswerMappingArray(answers);
            config.setQuestionOptionsArray(qoptions);
            ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter().writeValue(System.out,config);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}


class ChooseAllGameDetails
{
    String option;
    String correctFlag;

    public ChooseAllGameDetails() {
    }

    public ChooseAllGameDetails(String option, String correctFlag) {
        this.option = option;
        this.correctFlag = correctFlag;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getCorrectFlag() {
        return correctFlag;
    }

    public void setCorrectFlag(String correctFlag) {
        this.correctFlag = correctFlag;
    }
}

class ChooseAllGameConfig
{
    String id;

    public ArrayList<String> getQuestionOptionsArray() {
        return questionOptionsArray;
    }

    public void setQuestionOptionsArray(ArrayList<String> questionOptionsArray) {
        this.questionOptionsArray = questionOptionsArray;
    }

    public ArrayList<Integer> getAnswerMappingArray() {
        return answerMappingArray;
    }

    public void setAnswerMappingArray(ArrayList<Integer> answerMappingArray) {
        this.answerMappingArray = answerMappingArray;
    }

    public ArrayList<String> getQuestionsArray() {
        return questionsArray;
    }

    public void setQuestionsArray(ArrayList<String> questionsArray) {
        this.questionsArray = questionsArray;
    }

    ArrayList<String> questionsArray;
    ArrayList<String> questionOptionsArray;
    ArrayList<Integer> answerMappingArray;

    @Override
    public String toString() {
        return "ChooseAllGameConfig{" +
                "id='" + id + '\'' +
                ", questionOptionsArray=" + questionOptionsArray +
                ", answerMappingArray=" + answerMappingArray +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}