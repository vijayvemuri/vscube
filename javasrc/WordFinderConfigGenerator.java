package vscube.wordfinder;

import java.util.*;

public class WordFinderConfigGenerator
{
    static List<Integer> chosenRows = new ArrayList<>();
    static List<Integer> chosenCols = new ArrayList<>();
    static Character[][] puzzleMatrix = new Character[10][10];
    //static String[] words = new String[]{"RED", "BLUE", "WHITE", "ORANGE", "PURPLE", "GREEN", "VIOLET", "YELLOW", "BLACK", "PINK"};
    //static String[] words = new String[]{"ELEPHANT", "HORSE", "GIRAFFE", "COW", "DONKEY", "CAMEL", "TIGER", "LION", "GORILLA", "MONKEY"};
    //static String[] words = new String[]{"SQUIRREL", "TURKEY", "PARROT", "OWL", "BULBUL", "OSTRICH", "STORK", "CROW", "CRANE", "SPARROW"};
    //static String[] words = new String[]{"SINGAPORE", "NEPAL", "INDIA", "SRILANKA", "PAKISTAN", "AUSTRALIA", "CANADA", "GERMANY", "JAPAN", "CHINA"};
    static String[] words = new String[]{"AEROPLANE", "TRUCK", "CAR", "BOAT", "SHIP", "BUS", "BICYCLE", "GLIDER", "TRAIN", "CHARIOT"};
    static int count =0;
    static String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static List<String> positions = new ArrayList<>();

    public static void main(String args[]) {

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                puzzleMatrix[i][j] = '~';
            }
        }
        for (String word : words)
        {
            fillWord(word);
        }
        //System.out.println("Total words placed :: " + count);

        //System.out.println("Chosen Rows :: " + chosenRows);
       // System.out.println("Chosen Cols :: " + chosenCols);

        if (words.length == count)
        {
            System.out.println("Final List :: " );
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    if (puzzleMatrix[i][j]=='~')
                    {
                        puzzleMatrix[i][j]= getRandomAlphabet();
                    }
                   // System.out.print(puzzleMatrix[i][j] + "  ");
                }
                //System.out.println();
            }
            //System.out.println("Words Array" );

            StringBuilder sb1 = new StringBuilder();
            sb1.append("[");
            for (String word : words)
            {
                sb1.append('\"');
                sb1.append(word);
                sb1.append('\"');
                sb1.append(',');
            }
            sb1.delete(sb1.length()-1,sb1.length());
            sb1.append("]");
            System.out.println("words Array"  + sb1.toString());
            //System.out.println("Row Array"  + buildStringArray());

            StringBuilder sb2 = new StringBuilder();
            sb2.append("[");
            for (String word : buildStringArray())
            {
                sb2.append('\"');
                sb2.append(word);
                sb2.append('\"');
                sb2.append(',');
            }
            sb2.delete(sb2.length()-1,sb2.length());
            sb2.append("]");
            System.out.println("words Array"  + sb2.toString());

            StringBuilder sb3 = new StringBuilder();
            sb3.append("[");
            for (String pos : positions)
            {
                sb3.append(pos);
                sb3.append(',');
            }
            sb3.delete(sb3.length()-1,sb3.length());
            sb3.append("]");
            System.out.println("Postions Array"  + sb3.toString());

            System.exit(1);
        }
        else
        {
             chosenRows = new ArrayList<>();
             chosenCols = new ArrayList<>();
             puzzleMatrix = new Character[10][10];
             positions = new ArrayList<>();
             count=0;
             main(new String[]{});
        }
    }

    public static void fillWord(String word) {

        System.out.println("START PROCESSING WORD --> " + word);

        String rowColIndicator = getRandomRowOrColIndicator();

        int rowColNumber =0;
        if ("ROW".equals(rowColIndicator)) {

            rowColNumber = generateAndReturnRandonNumber();

            //System.out.println("Initial Row number :: " + rowColNumber);

            int retrycount =0;
            while (chosenRows.contains(Integer.valueOf(rowColNumber))&& retrycount < 10)
            {
                //System.out.println("Trying row number again!!");

                rowColNumber = generateAndReturnRandonNumber();
                retrycount++;
            }
            if (retrycount >=10)
            {
                fillWord(word);
            }
            else
            {
                //System.out.println("Final Row number :: " + rowColNumber);
                //System.out.println("Chosen Rows :: " + chosenRows);
                chosenRows.add(rowColNumber);
            }
           }
        else if ("COL".equals(getRandomRowOrColIndicator())) {

            rowColNumber = generateAndReturnRandonNumber();

            //System.out.println("Initial Col number :: " + rowColNumber);

            int retrycount =0;
            while (chosenCols.contains(Integer.valueOf(rowColNumber))&& retrycount < 10) {
                rowColNumber = generateAndReturnRandonNumber();
            }
            if (retrycount >=10) fillWord(word);

            chosenCols.add(rowColNumber);
        }

        int startIndex = generateAndReturnRandonNumber();

        while (word.length() > 9 - startIndex) {
            startIndex = generateAndReturnRandonNumber();
        }

        /*System.out.println("---------------------------------------------------");
        System.out.println("Word :: " + word);
        System.out.println("rowColIndicator :: " + rowColIndicator);
        System.out.println("rowColNumber :: " + rowColNumber);
        System.out.println("startIndex :: " + startIndex);
        System.out.println("EndIndex :: " + (startIndex + word.length() - 1));
        System.out.println("---------------------------------------------------");*/



        if ("ROW".equals(rowColIndicator)) {

            boolean dataExists = false;
            for (int index = 0; index < word.length(); index++) {
                if (!(puzzleMatrix[rowColNumber][startIndex + index] == '~')) {
                    dataExists = true;
                }
            }
                if (dataExists)
                {
                   // System.out.println("Retrying word ROW:: " + word);

                    fillWord(word);
                }
                else
                {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("[");
                    for (int index = 0; index < word.length(); index++) {
                        Character c = word.charAt(index);
                        puzzleMatrix[rowColNumber][startIndex + index] = c;
                        sb2.append('\"');
                        sb2.append(rowColNumber + ""+(startIndex + index));
                        sb2.append('\"');
                        sb2.append(',');
                    }
                    sb2.delete(sb2.length()-1,sb2.length());
                    sb2.append("]");
                    positions.add(sb2.toString());
                    count++;
                }

        } else if ("COL".equals(rowColIndicator)) {

            boolean dataExists = false;
            for (int index = 0; index < word.length(); index++) {
                if (!(puzzleMatrix[startIndex + index][rowColNumber] == '~')) {
                    dataExists = true;
                }
            }
                if (dataExists)
                {
                   // System.out.println("Retrying word  COL:: " + word);
                    fillWord(word);
                }
                else
                {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("[");
                    for (int index = 0; index < word.length(); index++) {
                        Character c = word.charAt(index);
                        puzzleMatrix[startIndex + index][rowColNumber] = c;
                        sb2.append('\"');
                        sb2.append((startIndex + index) + ""+ rowColNumber);
                        sb2.append('\"');
                        sb2.append(',');

                    }
                    sb2.delete(sb2.length()-1,sb2.length());
                    sb2.append("]");
                    positions.add(sb2.toString());

                    count++;
                }
            }

        System.out.println("END PROCESSING WORD --> " + word);
        }

    public static int generateAndReturnRandonNumber()
    {
        Random rand = new Random();
        int val = rand.nextInt(10);
        return val;
    }

    public static String getRandomRowOrColIndicator()
    {
        Random rand = new Random();
        int val = rand.nextInt(3);
        if (val == 1 || val == 2)
        {
            return "ROW";
        }
        return "COL";
    }

    public static Character getRandomAlphabet()
    {
        Random rand = new Random();
        return (Character.valueOf(abc.charAt(rand.nextInt(abc.length()))));
    }

    public static List<String> buildStringArray()
    {
        List<String> list = new ArrayList<>();

        for (int i =0; i < 10;i++)
        {
            StringBuilder sb = new StringBuilder();
            for (int j =0; j < 10;j++)
            {
                sb.append(puzzleMatrix[i][j]);
            }
            list.add(sb.toString());
        }
        return list;
    }

}
